//
//  PermissionsViewController.swift
//  Kdipa
//
//  Created by Safoine Moncef Amine on 3/1/20.
//  Copyright © 2020 Safoine Moncef Amine. All rights reserved.
//

import UIKit
import Charts
import MSPeekCollectionViewDelegateImplementation
import DropDown

class PermissionsViewController: BaseViewController {
    
    // MARK: - Outlets
    
    @IBOutlet weak var currentName: KdipaLabel!
    @IBOutlet weak var pieChart: PieChartView!
    @IBOutlet weak var permissionTakenItem: PermissionItem!
    @IBOutlet weak var permissionRemainingItem: PermissionItem!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var lblTotal: UILabel!
    @IBOutlet weak var employeeDropDownContainer: RoundedView!
    @IBOutlet weak var lblPercentage: UILabel!
    @IBOutlet weak var llblTitle: KdipaLabel!
    @IBOutlet weak var lblTotalPermission: UILabel!
    @IBOutlet weak var btnCreatePermission: UIButton!
    
    // MARK: - Variables
    
    var viewModel = PermissionViewModel()
    var delegate: MSPeekCollectionViewDelegateImplementation!
    let employeesDropDown = DropDown()
    var coordinator: MenuCoordinator?
    
    // MARK: - Life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupNavigationBarItems()
        self.configure()
        self.bind()
        self.localize()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.viewModel.loadPermissions()
    }
    
    // MARK: - Configuation
    
    func configure() {
        self.configurePermissionItems()
        self.configurePieChart()
        self.configureCollectionView()
        self.configureDropBox()
        self.configureGestures()
        self.highlightTakenItem()
    }
    
    func localize() {
        self.llblTitle.text = Localizable.Permissions.title
        self.lblTotalPermission.text = Localizable.Permissions.totalPermissions
        self.btnCreatePermission.setTitle(Localizable.Permissions.createPermission, for: .normal)
    }
    
    func configureGestures() {
        let hightlightRemainingItem = UITapGestureRecognizer(target: self, action: #selector(self.highlightRemainingItem))
        self.permissionRemainingItem.addGestureRecognizer(hightlightRemainingItem)
        
        let hightlightTakenItem = UITapGestureRecognizer(target: self, action: #selector(self.highlightTakenItem))
        self.permissionTakenItem.addGestureRecognizer(hightlightTakenItem)
    }
    
    @objc func highlightRemainingItem() {
        let remainingValue = self.viewModel.loadedPermissionHistory.value?.permissionsRemained ?? 0
        self.lblPercentage.text = "\(self.viewModel.permissionsRemainingPercentage)%"
        
        let chartHightLight = Highlight(x: 1.0, y: Double(remainingValue), dataSetIndex: 0, dataIndex: -1)
        self.pieChart.highlightValue(chartHightLight, callDelegate: false)
        self.permissionTakenItem.isSelected = false
        self.permissionRemainingItem.isSelected = true
    }
    
    @objc func highlightTakenItem() {
        let taken = self.viewModel.loadedPermissionHistory.value?.permissionsTaken ?? 0
        self.lblPercentage.text = self.viewModel.permissionsTakenPercentage + "%"
        let chartHightLight = Highlight(x: 0.0, y: Double(taken), dataSetIndex: 0, dataIndex: -1)
        self.pieChart.highlightValue(chartHightLight, callDelegate: false)
        self.permissionTakenItem.isSelected = true
        self.permissionRemainingItem.isSelected = false
        
    }
    
    func configureCollectionView() {
        self.collectionView.dataSource = self
        self.collectionView.delegate = self
        collectionView.register(UINib.nib(named: PermissionCollectionViewCell.reuseIdentifier),
                                forCellWithReuseIdentifier: PermissionCollectionViewCell.reuseIdentifier)
        collectionView.configureForPeekingDelegate()
        delegate = MSPeekCollectionViewDelegateImplementation()
        delegate = MSPeekCollectionViewDelegateImplementation(cellSpacing: 5)
        delegate = MSPeekCollectionViewDelegateImplementation(cellPeekWidth: 5)
        delegate = MSPeekCollectionViewDelegateImplementation(minimumItemsToScroll: 1)
        delegate = MSPeekCollectionViewDelegateImplementation(maximumItemsToScroll: 3)
        delegate = MSPeekCollectionViewDelegateImplementation(numberOfItemsToShow: 1)
        collectionView.delegate = delegate
    }
    
    func configurePieChart() {
        var pieEntries: [PieChartDataEntry] = []
        pieChart.legend.enabled = false
        pieChart.noDataText = "no data available"
        pieEntries.append(PieChartDataEntry(value: Double(self.viewModel.loadedPermissionHistory.value?.permissionsTaken ?? 0), label: ""))
        pieEntries.append(PieChartDataEntry(value: Double(self.viewModel.loadedPermissionHistory.value?.permissionsRemained ?? 0), label: ""))
        let set: PieChartDataSet = PieChartDataSet(entries: pieEntries)
        set.colors = [UIColor.kdipaDarkBlue, UIColor.kdipaOrange]
        set.selectionShift = 7.0
        let data: PieChartData = PieChartData(dataSet: set)
        data.setDrawValues(false)
        self.pieChart.data = data
        self.pieChart.delegate = self
        guard let permissionRemaining = self.viewModel.loadedPermissionHistory.value?.permissionsRemained else {
            return
        }
        guard let permissionTaken = self.viewModel.loadedPermissionHistory.value?.permissionsTaken else {
            return
        }
        self.lblTotal.text = "\(permissionRemaining + permissionTaken)"
        self.currentName.text = self.viewModel.selectedEmployee?.employeeName
    }
    
    func configurePermissionItems() {
        self.permissionTakenItem.configure(value: self.viewModel.loadedPermissionHistory.value?.permissionsTaken ?? 0,
                                           type: .taken,
                                           isSelected: true)
        self.permissionRemainingItem.configure(value: self.viewModel.loadedPermissionHistory.value?.permissionsRemained ?? 0,
                                               type: .remaining,
                                               isSelected: false)
    }
    
    func configureDropBox() {
        guard let employees = SessionHandler.shared.connectedEmployee?.employees else {
            return
        }
        let array = employees.map { $0.employeeName }
        employeesDropDown.dataSource = array
        employeesDropDown.anchorView = self.employeeDropDownContainer
        employeesDropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            self.viewModel.selectedEmployee = SessionHandler.shared.connectedEmployee?.employees?.first(where: { (employee) -> Bool in
                employee.employeeName == item
            })
            self.viewModel.loadPermissions()
        }
        let showDropDownTapGesture = UITapGestureRecognizer(target: self, action: #selector(self.showEmployeeDropDown))
        self.employeeDropDownContainer.addGestureRecognizer(showDropDownTapGesture)
    }
    
    @objc func showEmployeeDropDown() {
        self.employeesDropDown.show()
    }
    
    func bind() {
        self.viewModel.loadedPermissionHistory.bind(listener: { _ in
            self.configure()
        })
        self.viewModel.loadpermissionsAPICallState.bind { (state) in
            switch state {
            case .running:
                self.showSpinner(uiView: self.coordinator?.navigationController.view ?? self.view)
            case .failed:
                self.hideSpinner()
                self.showConfirmationMessage(view: self.view, message: "Server Error")
            case .success:
                self.hideSpinner()
            default:
                self.hideSpinner()
                
            }
        }
        
        self.viewModel.loadedPermissionHistory.bind(listener: { _ in
            self.configure()
            self.collectionView.reloadData()
        })
    }
    // MARK: - UI Actions
    
    @IBAction func btnNewPermissionClicked(_ sender: Any) {
        self.coordinator?.newPermission()
    }
}

// MARK: - ChartViewDelegate

extension PermissionsViewController: ChartViewDelegate {
    func chartValueSelected(_ chartView: ChartViewBase, entry: ChartDataEntry, highlight: Highlight) {
        if entry.y == Double(self.permissionTakenItem.lblValue.text ?? "") {
            highlightTakenItem()
        } else {
            highlightRemainingItem()
        }
    }
}

// MARK: - CollectionView Datasurce

extension PermissionsViewController: UICollectionViewDataSource, UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return (self.viewModel.loadedPermissionHistory.value?.permissionsHistory.count ?? 0) + 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: PermissionCollectionViewCell.reuseIdentifier,
                                                            for: indexPath) as? PermissionCollectionViewCell else {
                                                                return PermissionCollectionViewCell()
        }
        
        if let maxIndex = self.viewModel.loadedPermissionHistory.value?.permissionsHistory.count, maxIndex == indexPath.row {
            cell.contentView.alpha = 0.0
            return cell
        }
        cell.contentView.alpha = 1.0
        if let permission = self.viewModel.loadedPermissionHistory.value?.permissionsHistory[indexPath.row] {
            cell.configure(permission: permission)
        }
        
        return cell
    }
}

// MARK: - UICollectionViewDelegateFlowLayout

extension PermissionsViewController: UICollectionViewDelegateFlowLayout {

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width, height: collectionView.frame.height)
    }
}
