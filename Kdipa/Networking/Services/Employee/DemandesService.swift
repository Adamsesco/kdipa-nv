//
//  DemandesService.swift
//  Kdipa
//
//  Created by Safoine Moncef Amine on 2/29/20.
//  Copyright © 2020 Safoine Moncef Amine. All rights reserved.
//

import Foundation
import Alamofire

class DemandesService {
    
    func loadVacations(email: String, completion: @escaping (VacationResponse?, Error?) -> Void) {
        let url = URL(string: Constants.kdipaEmployeeBaseURL + KdipaAPI.vacations.path)!
        let params = ["email": email]
        let headers = ["Content-Type": "application/json", "Accept": "application/json"]
        Alamofire.request(url, method: .post, parameters: params, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
            switch response.result {
            case .success(let value):
                let dictValue = value as? [String: Any]
                guard let data = dictValue?.jsonStringRepresentation?.data(using: .utf8) else {
                    return
                }
                do {
                    let vacationResponse = try JSONDecoder().decode(VacationResponse.self, from: data)
                    completion(vacationResponse, nil)
                } catch {
                    completion(nil, error)
                }
            case .failure(let error):
                completion(nil, error)
            }
        }
    }
    
    func loadAbsences(email: String, completion: @escaping (AbsenceResponse?, Error?) -> Void) {
        let url = URL(string: Constants.kdipaEmployeeBaseURL + KdipaAPI.absence.path)!
        let params = ["email": email]
        let headers = ["Content-Type": "application/json", "Accept": "application/json"]
        Alamofire.request(url, method: .post, parameters: params, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
            switch response.result {
            case .success(let value):
                let dictValue = value as? [String: Any]
                guard let data = dictValue?.jsonStringRepresentation?.data(using: .utf8) else {
                    return
                }
                let absenceResponse = try? JSONDecoder().decode(AbsenceResponse.self, from: data)
                completion(absenceResponse, nil)
            case .failure(let error):
                completion(nil, error)
            }
        }
    }
    
    func loadPermissions(email: String, completion: @escaping (PermissionHistoryResponse?, Error?) -> Void) {
        let url = URL(string: Constants.kdipaEmployeeBaseURL + KdipaAPI.permisions.path)!
        let params = ["email": email]
        let headers = ["Content-Type": "application/json", "Accept": "application/json"]
        Alamofire.request(url, method: .post, parameters: params, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
            switch response.result {
            case .success(let value):
                let dictValue = value as? [String: Any]
                guard let data = dictValue?.jsonStringRepresentation?.data(using: .utf8) else {
                    return
                }
                do {
                    let absenceResponse = try JSONDecoder().decode(PermissionHistoryResponse.self, from: data)
                    completion(absenceResponse, nil)
                } catch {
                    completion(nil, error)
                }
            case .failure(let error):
                completion(nil, error)
            }
        }
    }
    
    func loadAttendancyDays(email: String, completion: @escaping ([DaysOfAttendance]?, Error?) -> Void) {
        let url = URL(string: Constants.kdipaEmployeeBaseURL + KdipaAPI.attendancyDays.path)!
        let params = ["email": email]
        let headers = ["Content-Type": "application/json", "Accept": "application/json"]
        Alamofire.request(url, method: .post, parameters: params, encoding: JSONEncoding.default, headers: headers).responseData { (response) in
            switch response.result {
            case .success(let value):
                do {
                    let attendanceDays = try JSONDecoder().decode([DaysOfAttendance].self, from: value)
                    completion(attendanceDays, nil)
                } catch {
                    completion(nil, error)
                }
            case .failure(let error):
                completion(nil, error)
            }
        }
    }
}
