//
//  AbsenceResponse.swift
//  Kdipa
//
//  Created by Safoine Moncef Amine on 2/29/20.
//  Copyright © 2020 Safoine Moncef Amine. All rights reserved.
//

import Foundation

// MARK: - Welcome
struct AbsenceResponse: Codable {
    let name, nameAr: String
    let year: [String: [String]]
}
