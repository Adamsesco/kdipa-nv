//
//  AbsenceViewModel.swift
//  Kdipa
//
//  Created by Safoine Moncef Amine on 3/1/20.
//  Copyright © 2020 Safoine Moncef Amine. All rights reserved.
//

import Foundation

class AbsenceViewModel {
    
    let service = DemandesService()
    var absence: AbsenceResponse?
    
    var loadAbsencesAPICallState: Box<ApiCallState> = Box(.inactive)
    
    var absences: [String] {
        return (self.absence?.year[self.selectedYear] ?? []).map { (absence) -> String in
            return self.formatToVacationDate(date: absence)
        }
    }
    
    var years: [String] = []
    
    var selectedEmployee: Employee? = SessionHandler.shared.connectedEmployee?.employees?[exist: 0] ??
    Employee(
        dict: ["Email": UserDefaults.standard.string(forKey: Constants.UserDefaultsKeys.email) ?? "",
           "EmployeeName": SessionHandler.shared.connectedEmployee?.firstName ?? ""])
    
    var selectedYear: String = ""
    
    func loadAbsences() {
        self.loadAbsencesAPICallState.value = .running
        service.loadAbsences(email: self.selectedEmployee?.email ?? "") { (vacationResponse, error) in
            if error == nil {
                self.selectedYear = vacationResponse?.year.keys.first ?? ""
                self.years = vacationResponse?.year.keys.map({$0}) ?? []
                self.absence = vacationResponse
                self.loadAbsencesAPICallState.value = .success
            } else {
                self.loadAbsencesAPICallState.value = .failed
            }
        }
    }
    
    func formatDateForDateField(date: Date) -> String {
        let dateFormatterPrint = DateFormatter()
        dateFormatterPrint.dateFormat = "EEEE dd MMMM yyyy "
        return dateFormatterPrint.string(from: date)
    }
    
    func dateFromString(dateDescription: String) -> Date {
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        
        let dateFormatterPrint = DateFormatter()
        dateFormatterPrint.dateFormat = "EEE dd MMMMM hh:mm aaa"
        
        let date = dateFormatterGet.date(from: dateDescription)!
        return date
    }
    
    func formatToVacationDate(date: String) -> String {
        return formatDateForDateField(date: dateFromString(dateDescription: date))
    }
}
