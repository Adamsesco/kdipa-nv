//
//  Box.swift
//  Kdipa
//
//  Created by Safoine Moncef Amine on 2/7/20.
//  Copyright © 2020 Safoine Moncef Amine. All rights reserved.
//

import Foundation

/// Binding class
class Box<T> {
    
    // MARK: - Properties
    
    typealias Listener = (T) -> Void
    var listener: Listener?
    
    var value: T {
        didSet {
            listener?(value)
        }
    }
    
    // MARK: - Init
    
    init(_ value: T) {
        self.value = value
    }
    
    // MARK: - Methods
    
    func bind(listener: Listener?) {
        self.listener = listener
        listener?(value)
    }
}
