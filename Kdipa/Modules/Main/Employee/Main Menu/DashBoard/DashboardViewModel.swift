//
//  DashboardViewModel.swift
//  Kdipa
//
//  Created by Safoine Moncef Amine on 2/12/20.
//  Copyright © 2020 Safoine Moncef Amine. All rights reserved.
//

import Foundation

class DashboardViewModel {
    
    var apiCallState: Box<ApiCallState> = Box(.inactive)
    var selectedEmployee: Employee? = SessionHandler.shared.connectedEmployee?.employees?[exist: 0] ??
        Employee(
            dict: ["Email": UserDefaults.standard.string(forKey: Constants.UserDefaultsKeys.email) ?? "",
               "EmployeeName": SessionHandler.shared.connectedEmployee?.firstName ?? ""])
    let dashboard: Box<DashboardResponse?> = Box(nil)
    func getDashboard() {
        self.apiCallState.value = .running
        SessionHandler.shared.getDashBoard(for: self.selectedEmployee?.email ?? "") { (dashboardDict, error) in
            if error == nil {
                self.dashboard.value = DashboardResponse(dict: dashboardDict ?? [:])
                self.apiCallState.value = .success
            } else {
                self.apiCallState.value = .failed
            }
        }
    }
}
