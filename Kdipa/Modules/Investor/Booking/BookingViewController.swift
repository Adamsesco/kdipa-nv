//
//  BookingViewController.swift
//  Kdipa
//
//  Created by Safoine Moncef Amine on 3/26/20.
//  Copyright © 2020 Safoine Moncef Amine. All rights reserved.
//

import UIKit
import FSCalendar

class BookingViewController: BaseViewController {
    
    var coordinator: MenuCoordinator?
    var viewModel = BookingViewModel()
    
    @IBOutlet weak var slotsStack: UIStackView!
    @IBOutlet weak var calander: FSCalendar!
    @IBOutlet weak var vwConfirmAppointment: RoundedView!
    override func viewDidLoad() {
        super.viewDidLoad()
        super.setupNavigationBarItems()
        self.configureSlots()
        self.bind()
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.bookAppointment))
        self.vwConfirmAppointment.addGestureRecognizer(tapGesture)
        self.calander.delegate = self
    }
    
    @objc func bookAppointment() {
        self.viewModel
            .bookNewBooking()
    }
    
    func bind() {
        self.viewModel.bookingCallState.bind { (state) in
            switch state {
            case .running:
                self.showSpinner(uiView: self.coordinator?.navigationController.view ?? self.view)
            case .success:
                self.showConfirmationMessage(view: self.view, message: self.viewModel.messageToShow)
                self.hideSpinner()
            case .failed:
                self.showConfirmationMessage(view: self.view, message: self.viewModel.messageToShow)
                self.hideSpinner()
            default:
                self.hideSpinner()
            }
        }
    }
    func configureSlots() {
        for stack in self.slotsStack.subviews {
            for slot in stack.subviews {
                if let timeSlot = slot as? TimeSlot {
                    let tap = UITapGestureRecognizer(target: self, action: #selector(self.slotDidSelected(_:)))
                    timeSlot.addGestureRecognizer(tap)
                    timeSlot.configure()
                }
            }
        }
    }
    
    @objc func slotDidSelected(_ gestureRecognizer: UITapGestureRecognizer) {
        self.deselectAllSlots()
        (gestureRecognizer.view as? TimeSlot)?.updateState(newState: .selected)
        self.viewModel.selectedSlotTime = viewModel.getTimeByIndex(gestureRecognizer.view?.tag ?? 0)
    }
    
    func deselectAllSlots() {
        for stack in self.slotsStack.subviews {
            for slot in stack.subviews {
                if let timeSlot = slot as? TimeSlot {
                    timeSlot.updateState(newState: .unselected)
                }
            }
        }
    }
    
    
}

extension BookingViewController: FSCalendarDelegate {
    func calendar(_ calendar: FSCalendar, didSelect date: Date, at monthPosition: FSCalendarMonthPosition) {
        self.viewModel.selectedDate = date
    }
}
