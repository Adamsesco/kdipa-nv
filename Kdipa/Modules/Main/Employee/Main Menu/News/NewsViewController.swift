//
//  NewsViewController.swift
//  Kdipa
//
//  Created by Safoine Moncef Amine on 3/8/20.
//  Copyright © 2020 Safoine Moncef Amine. All rights reserved.
//

import UIKit

class NewsViewController: BaseViewController {
    
    var coordinator: MenuCoordinator?
    var viewModel: NewsViewModel = NewsViewModel()
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var lblScreenTitle: KdipaLabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupNavigationBarItems()
        self.bind()
        collectionView.register(UINib.nib(named: "NewsCollectionViewCell"), forCellWithReuseIdentifier: "NewsCollectionViewCell")
        if let flowLayout = collectionView?.collectionViewLayout as? UICollectionViewFlowLayout {
           flowLayout.estimatedItemSize = UICollectionViewFlowLayout.automaticSize
        }
        self.localize()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.viewModel.loadNews()
    }
    
    func localize() {
        self.lblScreenTitle.text = Localizable.News.title
    }
    func bind() {
        self.viewModel.loadNewsCallState.bind { (state) in
            switch state {
            case .running:
                self.showSpinner(uiView: self.coordinator?.navigationController.view ?? self.view)
            case .success:
                self.hideSpinner()
                self.collectionView.reloadData()
            case .failed:
                self.hideSpinner()
                self.showConfirmationMessage(view: self.view, message: "something went wrong")
            default:
                self.hideSpinner()
            }
        }
    }
}

extension NewsViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        self.viewModel.news.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "NewsCollectionViewCell",
                                                            for: indexPath) as? NewsCollectionViewCell else { return NewsCollectionViewCell() }
        cell.configure(news: self.viewModel.news[indexPath.row])
        return cell
    }
}

// MARK: - UICollectionViewDelegateFlowLayout

extension NewsViewController: UICollectionViewDelegateFlowLayout {

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width, height: 200)
    }
}
