//
//  VacationReturnViewController.swift
//  Kdipa
//
//  Created by Said Elmansour on 2020-03-01.
//  Copyright © 2020 Safoine Moncef Amine. All rights reserved.
//

import UIKit
import FSCalendar
import DropDown

class VacationReturnViewController: BaseViewController {
    
    // MARK: - Outlets
    
    @IBOutlet weak var lblReturnDate: UILabel!
    @IBOutlet weak var lblEndDate: UILabel!
    
    @IBOutlet weak var lblReturnDateDesc: UILabel!
    @IBOutlet weak var vacationsDropDown: KdipaDropDown!
    
    @IBOutlet weak var constraintCalenderLeading: NSLayoutConstraint!
    @IBOutlet weak var constraintCalenderTrailing: NSLayoutConstraint!
    
    @IBOutlet weak var notDelayedField: KdipaSelectionField!
    @IBOutlet weak var delayedField: KdipaSelectionField!
    @IBOutlet weak var txtViewDelayReason: UITextView!
    @IBOutlet weak var delayReasonFieldsContainer: UIStackView!
    
    @IBOutlet weak var constraintBodyBottom: NSLayoutConstraint!
    
    @IBOutlet weak var lblTitle: KdipaLabel!
    
    @IBOutlet weak var lblForFollowingReason: UILabel!
    @IBOutlet weak var btnSubmit: UIButton!
    
    // MARK: - Variables
    
    var viewModel: VacationReturnViewModel = VacationReturnViewModel()
    var coordinator: MenuCoordinator?
    var btnType: BtnDatesType?
    
    // MARK: - VC life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupNavigationBarItems()
        self.bind()
        self.localize()
        self.configure()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.viewModel.loadVacationReturn()
    }
    
    // MARK: - configure
    
    func configure() {
        self.txtViewDelayReason.delegate = self
        self.vacationsDropDown.delegate = self
        self.configureGestures()
        self.configureNotification()
        self.configureDropDwon()
    }
    
    func configureDropDwon() {
        self.vacationsDropDown.reLoad(array: self.viewModel.vacationsDescription)
    }
    
    private func configureGestures() {
        self.delayReasonFieldsContainer.isHidden = true
        let notDeleyedTapGesture = UITapGestureRecognizer(target: self, action: #selector(self.notDelayedFieldClicked))
        self.notDelayedField.addGestureRecognizer(notDeleyedTapGesture)
        
        let deleyedTapgesture = UITapGestureRecognizer(target: self, action: #selector(self.delayedFieldClicked))
        self.delayedField.addGestureRecognizer(deleyedTapgesture)
    }
    
    private func configureNotification() {
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(keyboardWillDisappear), name: UIResponder.keyboardWillHideNotification,
                                               object: nil)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(keyboardWillAppear), name: UIResponder.keyboardWillShowNotification,
                                               object: nil)
    }
    
    private func bind() {
        guard let spinnerContainer = self.navigationController?.view ?? self.view else {
            return
        }
        self.viewModel.retrieveVacationsAPICallState.bind { (state) in
            switch state {
            case .running:
                self.showSpinner(uiView: spinnerContainer)
            case .success:
                self.hideSpinner()
                self.configureDropDwon()
            case .failed:
                self.hideSpinner()
            default:
                self.hideSpinner()
            }
        }
        
        self.viewModel.dates.departureDate.bind { date in
            self.lblReturnDate.text = date?.formatTo(format: "EEE, MMM dd")
        }
        
        self.viewModel.dates.returnDate.bind { date in
            self.lblReturnDate.text = date?.formatTo(format: "EEE, MMM dd")
        }
        
        self.viewModel.submitVacationReturnAPICallState.bind { (state) in
            switch state {
            case .running:
                self.showSpinner(uiView: spinnerContainer)
            case .success:
                self.hideSpinner()
                self.coordinator?.dashboard()
            case .failed:
                self.hideSpinner()
                self.showConfirmationMessage(view: self.view, message: "server error")
            default:
                self.hideSpinner()
            }
        }
    }
    
    private func localize() {
        self.delayedField.configure(description: Localizable.VacationReturn.wasLate, selected: false)
        self.notDelayedField.configure(description: Localizable.VacationReturn.didNotDelay, selected: true)
        self.lblTitle.text = Localizable.VacationReturn.title
        self.lblForFollowingReason.text = Localizable.VacationReturn.forFollowingReason
        self.btnSubmit.setTitle(Localizable.VacationReturn.submit, for: .normal)
        self.lblReturnDateDesc.text = Localizable.VacationReturn.returnDate
    }
    
    // MARK: - UI Actions
    
    @IBAction func onReturnDate(_ sender: Any) {
        showCalendar()
        self.btnType = .endDate
    }
    
    @IBAction func onEnd(_ sender: Any) {
        showCalendar()
        self.btnType = .endDate
    }
    
    @IBAction func onSubmit(_ sender: Any) {
        self.viewModel.submitVacationReturn()
    }
    
    // MARK: - Handlers
    
    func showCalendar() {
        self.constraintCalenderTrailing.isActive = false
        self.constraintCalenderLeading.isActive = true
        UIView.animate(withDuration: 0.3) {
            self.view.layoutIfNeeded()
        }
    }
    
    func hideCalendar() {
        self.constraintCalenderLeading.isActive = false
        self.constraintCalenderTrailing.isActive = true
        UIView.animate(withDuration: 0.3) {
            self.view.layoutIfNeeded()
        }
    }
    
    // MARK: - Selectors
    
    @objc func keyboardWillAppear(_ notification: Notification) {
        guard let keyboardFrame: NSValue = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue else {
            return
        }
        let keyboardRectangle = keyboardFrame.cgRectValue
        let keyboardHeight = keyboardRectangle.height
        self.constraintBodyBottom.constant = keyboardHeight
    }
    
    @objc func keyboardWillDisappear(_ notification: Notification) {
        self.constraintBodyBottom.constant = 0
    }
    
    @objc func notDelayedFieldClicked() {
        self.delayReasonFieldsContainer.isHidden = true
        self.delayedField.unselect()
        self.notDelayedField.select()
    }
    
    @objc func delayedFieldClicked() {
        self.delayReasonFieldsContainer.isHidden = false
        self.delayedField.select()
        self.notDelayedField.unselect()
    }
}

// MARK: - FSCalendarDelegate

extension VacationReturnViewController: FSCalendarDelegate {
    
    func calendar(_ calendar: FSCalendar, didSelect date: Date, at monthPosition: FSCalendarMonthPosition) {
        self.hideCalendar()
        switch self.btnType {
        case .endDate:
            self.viewModel.dates.returnDate.value = date
        case .startDate:
            self.viewModel.dates.departureDate.value = date
        default:
            break
        }
    }
}

// MARK: - KdipaDropDownDelegate

extension VacationReturnViewController: KdipaDropDownDelegate {
    func didSelectItem(sender: KdipaDropDown, index: Int, newValue: String, oldValue: String) {
        self.viewModel.selectVacationAtIndex(index: index)
    }
}

// MARK: - UITextViewDelegate

extension VacationReturnViewController: UITextViewDelegate {
    func textViewDidChange(_ textView: UITextView) {
        self.viewModel.lateReturnReason = textView.text
    }
}
