//
//  RequestStatusService.swift
//  Kdipa
//
//  Created by Safoine Moncef Amine on 5/24/20.
//  Copyright © 2020 Safoine Moncef Amine. All rights reserved.
//

import Foundation
import Alamofire

struct RequestStatusService {
    func loadRequestsStatus(email: String, completion: @escaping (RequestStatusResponse?, Error?) -> Void) {
        let url = URL(string: Constants.kdipaEmployeeBaseURL + KdipaAPI.requestStatus.path)!
        let params = ["email": email]
        let headers = ["Content-Type": "application/json", "Accept": "application/json"]
        Alamofire.request(url, method: .post, parameters: params, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
            switch response.result {
            case .success(let value):
                let dictValue = value as? [String: Any]
                guard let data = dictValue?.jsonStringRepresentation?.data(using: .utf8) else {
                    return
                }
                do {
                    let vacationResponse = try JSONDecoder().decode(RequestStatusResponse.self, from: data)
                    completion(vacationResponse, nil)
                } catch {
                    completion(nil, error)
                }
            case .failure(let error):
                completion(nil, error)
            }
        }
    }
    
    func cancel(email: String, vacationID: String, completion: @escaping (Error?) -> Void) {
            let url = URL(string: Constants.kdipaEmployeeBaseURL + KdipaAPI.cancelRequest.path)!
        let params = ["email": email,"password": "","vacationId": vacationID,"cancelDate": Date().formatTo(format: "yyyy/MM/dd")]
            let headers = ["Content-Type": "application/json", "Accept": "application/json"]
            Alamofire.request(url, method: .post, parameters: params, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
                switch response.result {
                case .success(let _):
                    completion(nil)
                case .failure(let error):
                    completion(error)
                }
            }
        }
}
