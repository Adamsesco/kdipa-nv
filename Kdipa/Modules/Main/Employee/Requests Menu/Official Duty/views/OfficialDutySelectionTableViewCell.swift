//
//  OfficialDutySelectionTableViewCell.swift
//  Kdipa
//
//  Created by Safoine Moncef Amine on 3/11/20.
//  Copyright © 2020 Safoine Moncef Amine. All rights reserved.
//

import UIKit

class OfficialDutySelectionTableViewCell: UITableViewCell {
    
    @IBOutlet weak var vwSelection: KdipaSelectionField!
    
    static let reuseIdentifier = "OfficialDutySelectionTableViewCell"
    
    var content: String {
        return self.vwSelection.lblFieldDescription.text ?? ""
    }
    func configure(description: String, selected: Bool) {
        self.vwSelection.checkBox.isUserInteractionEnabled = false
        self.vwSelection.configure(description: description, selected: selected)
    }
    
    func setAsSelected() {
        self.vwSelection.select()
    }
    
    func setAsUnselected() {
        self.vwSelection.unselect()
    }
}
