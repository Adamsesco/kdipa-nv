//
//  EmergencyLeaveViewModel.swift
//  Kdipa
//
//  Created by Said Elmansour on 2020-02-23.
//  Copyright © 2020 Safoine Moncef Amine. All rights reserved.
//

import Foundation
import UIKit
class EmergencyLeaveViewModel {
    
    var emergencyLeaveDocuments = [UIImage]()
    var emergencyLeave: EmergencyLeave = EmergencyLeave()
    var services = EmergencyLeaveService()
    var submittingPermissionCallState: Box<ApiCallState> = Box(.inactive)

    init() {}
    
    func submitEmergencyLeave() {
        
        var submittedPermission = self.emergencyLeave
        
        submittedPermission.trxDate = self.emergencyLeave.trxDate.split(separator: " ")[0].replacingOccurrences(of: "-", with: "/")
      
        self.submittingPermissionCallState.value = .running

        services.postEmergencyLeave(emergencyLeave: submittedPermission) { error in
            if error == nil {
                self.submittingPermissionCallState.value = .success
            } else {
                self.submittingPermissionCallState.value = .failed
            }
        }
    }
}
