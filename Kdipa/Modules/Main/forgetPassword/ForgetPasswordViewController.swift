//
//  ForgetPasswordViewController.swift
//  Kdipa
//
//  Created by Safoine Moncef Amine on 6/19/20.
//  Copyright © 2020 Safoine Moncef Amine. All rights reserved.
//

import UIKit

class ForgetPasswordViewController: BaseViewController {

    @IBOutlet weak var txtFieldEmail: KdipaTextField!
    @IBOutlet weak var btnSubmit: UIButton!
    @IBOutlet weak var lblTitle: KdipaLabel!
    @IBOutlet weak var lblSubTitle: KdipaLabel!
    
    let viewModel = ForgetPasswordViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupUI()
        self.localize()
        self.bind()
    }
    
    func setupUI() {
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        self.txtFieldEmail.type = .email
        self.navigationController?.navigationBar.tintColor = UIColor.white
    }
    
    func localize() {
        self.btnSubmit.setTitle(Localizable.ForgetPassword.submit, for: .normal)
        self.lblTitle.text = Localizable.ForgetPassword.title
        self.lblSubTitle.text = Localizable.ForgetPassword.subTitle
    }
    
    func bind() {
        self.viewModel.forgetPassowrdRequestCallState.bind { (status) in
            switch status {
            case .running:
                self.showSpinner(uiView: self.view)
            case .failed:
                self.hideSpinner()
                self.showConfirmationMessage(view: self.view, message: Localizable.ForgetPassword.error)
            case .success:
                self.hideSpinner()
                self.showConfirmationMessage(view: self.view, message: Localizable.ForgetPassword.updateSuccess)
            default:
                break
            }
        }
    }
    
    @IBAction func btnForgetPasswordTapped(_ sender: Any) {
        self.txtFieldEmail.resignFirstResponder()
        if let email = self.txtFieldEmail.text {
            self.viewModel.resetPassword(email: email)
        }
    }
}
