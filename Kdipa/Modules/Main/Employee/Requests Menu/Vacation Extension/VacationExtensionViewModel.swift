//
//  VacationExtensionViewModel.swift
//  Kdipa
//
//  Created by Said Elmansour on 2020-03-01.
//  Copyright © 2020 Safoine Moncef Amine. All rights reserved.
//

import Foundation

struct VacationExtensionDates {
    var oldFromDate: Box<Date?> = Box(nil)
    var oldToDate: Box<Date?> = Box(nil)
    var newToDate: Box<Date?> = Box(nil)
}

class VacationExtensionViewModel {
    
    var submittingVacationExtensionCallState: Box<ApiCallState> = Box(.inactive)
    var gettingVExtensionCallState: Box<ApiCallState> = Box(.inactive)
    var vacationExtensionDates: VacationExtensionDates = VacationExtensionDates()
    
    var vacationExtension: VacationExtention? {
        didSet {
            self.vacationExtensionDates.oldFromDate.value = self.vacationExtension?.oldFromDate.getDate()
            self.vacationExtensionDates.oldToDate.value = self.vacationExtension?.oldToDate.getDate()
            self.vacationExtensionDates.newToDate.value = self.vacationExtension?.newToDate.getDate()
        }
    }
    var selectedDelegate: String = ""
    var selectedtype: String = ""
    var vacationType: [VacationType] {
        return self.vacationExtension?.vacationTypes ?? []
    }
    
    var delegatList: [String] {
        return SessionHandler
            .shared.connectedEmployee?.employees?.map({ (employee) -> String in
                return employee.employeeName
            }) ?? []
    }
    
    private var services = VacationExtentionService()
    
    func getvacationExtension() {
        self.gettingVExtensionCallState.value = .running
        services.loadVacationsExtension(email: "") { (vacationExtension, error) in
            if vacationExtension != nil {
                self.vacationExtension = vacationExtension
                self.gettingVExtensionCallState.value = .success
            }
            if error != nil {
                self.gettingVExtensionCallState.value = .failed
            }
        }
    }
    
    func submitVacationAmendment() {
        self.submittingVacationExtensionCallState.value = .running
        let payload = VacationExtentionPayload(
            email: UserDefaults.standard.string(forKey: Constants.UserDefaultsKeys.email) ?? "",
            vacationType: self.selectedtype,
            oldFromDate: self.vacationExtensionDates.oldFromDate.value?.formatTo(format: "yyyy/mm/dd") ?? "",
            oldToDate: self.vacationExtensionDates.oldToDate.value?.formatTo(format: "yyyy/mm/dd") ?? "",
            newToDate: self.vacationExtensionDates.newToDate.value?.formatTo(format: "yyyy/mm/dd") ?? "",
            delegatedEmployee: self.selectedDelegate)
        
        services.submitNewVacationsExtention(payload: payload) { (error) in
            if error == nil {
                self.submittingVacationExtensionCallState.value = .success
            } else {
                self.submittingVacationExtensionCallState.value = .failed
            }
        }
    }
}
