//
//  BookingResult.swift
//  Kdipa
//
//  Created by Safoine Moncef Amine on 3/30/20.
//  Copyright © 2020 Safoine Moncef Amine. All rights reserved.
//

import Foundation

// MARK: - BookingResult
struct BookingResult: Codable {
    let id, version, bookingServiceID: Int
    let service: String
    let accountManagerID: Int
    let accManager: String
    let meetingDate: Int
    let meetingTime: String
    let meetingTimeFormated: String

    enum CodingKeys: String, CodingKey {
        case id, version
        case bookingServiceID = "bookingServiceId"
        case service
        case accountManagerID = "accountManagerId"
        case accManager, meetingDate, meetingTime, meetingTimeFormated
    }
}
