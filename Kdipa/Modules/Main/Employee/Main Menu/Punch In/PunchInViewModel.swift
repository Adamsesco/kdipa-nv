//
//  PunchInViewModel.swift
//  Kdipa
//
//  Created by Said Elmansour on 2020-02-28.
//  Copyright © 2020 Safoine Moncef Amine. All rights reserved.
//

import Foundation

class PunchInViewModel {
    
    var submittingPunchInCallState: Box<ApiCallState> = Box(.inactive)
    var checkPunchInCallState: Box<ApiCallState> = Box(.inactive)
    var punchIn: PunchIn = PunchIn()
    var services = PunchInService()
    var punchInResponse: [String:String]?
    init() {}
    
    func submitAttendancePunch() {
        self.submittingPunchInCallState.value = .running

        services.postPunchIn(punchIn: self.punchIn) { value, error  in
            if error == nil && value != nil {
                self.punchInResponse = value
                self.submittingPunchInCallState.value = .success
            } else {
                self.submittingPunchInCallState.value = .failed
            }
        }
    }
    
    func checkAttendancePunch() {
        self.submittingPunchInCallState.value = .running
        services.geoAttendCheck(punchIn: self.punchIn) { value, error  in
            if error == nil && value != nil {
                if (value["Location"] as? String) == "1" {
                    self.checkPunchInCallState.value = .success
                } else {
                    self.checkPunchInCallState.value = .failed
                }
                
            } else {
                self.checkPunchInCallState.value = .failed
            }
        }
    }
}
