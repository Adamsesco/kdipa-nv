//
//  OfficialDuty.swift
//  Kdipa
//
//  Created by Safoine Moncef Amine on 3/11/20.
//  Copyright © 2020 Safoine Moncef Amine. All rights reserved.
//

import Foundation

// MARK: - OfficialDuty

struct OfficialDuty: Codable {
    let dutyTypes, dutyTypesAr: String
    let employees, employeesAr: [Employee]
    let attendanceExemption, attendanceExemptionAr: [String]
    let delegatedEmployees, delegatedEmployeesAr: [Employee]
    let officialDutyDutyTypes, officialDutyDutyTypesAr: [DutyType]
    let fromDate, toDate: String

    enum CodingKeys: String, CodingKey {
        case dutyTypes = "DutyTypes"
        case dutyTypesAr = "DutyTypesAr"
        case employees = "Employees"
        case employeesAr = "EmployeesAr"
        case attendanceExemption, attendanceExemptionAr, delegatedEmployees, delegatedEmployeesAr
        case officialDutyDutyTypes = "dutyTypes"
        case officialDutyDutyTypesAr = "dutyTypesAr"
        case fromDate, toDate
    }
}

// MARK: - DutyType

struct DutyType: Codable {
    let dutyID: Int
    let dutyName: String

    enum CodingKeys: String, CodingKey {
        case dutyID = "DutyID"
        case dutyName = "DutyName"
    }
}

// MARK: - OfficialDutyPayload

struct OfficialDutyPayload: Codable {
    let email, dutyType, fromDate: String
    let toDate, attendanceExemption, employees, delegatedEmployees: String
}
