//
//  SettingViewController.swift
//  Kdipa
//
//  Created by Safoine Moncef Amine on 3/31/20.
//  Copyright © 2020 Safoine Moncef Amine. All rights reserved.
//

import UIKit

class SettingViewController: BaseViewController {

    @IBOutlet weak var txtFirstName: KdipaTextField!
    @IBOutlet weak var txtLastName: KdipaTextField!
    @IBOutlet weak var txtEmail: KdipaTextField!
    
    @IBOutlet weak var lblScreenTitle: KdipaLabel!
    @IBOutlet weak var lblDropDesc: UILabel!
    
    @IBOutlet weak var languagesDropDown: KdipaDropDown!
    @IBOutlet weak var vwSaveChanges: RoundedView!
    @IBOutlet weak var btnSaveChanges: UIButton!
    
    var coordinator: MenuCoordinator?
    var selectedLanguage: Language = Language.language
    
    override func viewDidLoad() {
        super.viewDidLoad()
        super.setupNavigationBarItems()
        
        self.configure()
        self.loadData()
        localize()
    }

    func configure() {
        self.txtFirstName.type = .standard
        self.txtLastName.type = .standard
        self.txtEmail.type = .standard
        self.languagesDropDown.reLoad(array: [Localizable.english, Localizable.arabic])
        self.languagesDropDown.fill(description: Localizable.lang)
        self.languagesDropDown.delegate = self
    }
    
    func localize() {
        self.lblScreenTitle.text = Localizable.EmployeeSetting.title
        self.lblDropDesc.text = Localizable.EmployeeSetting.dropDownDesc
        self.btnSaveChanges.setTitle(Localizable.EmployeeSetting.btnSave
            , for: .normal)
    }
    
    func loadData() {
        self.txtFirstName.placeholder = SessionHandler.shared.connectedInvestor?.firstName
        self.txtLastName.placeholder = SessionHandler.shared.connectedInvestor?.lastName
        self.txtEmail.placeholder = UserDefaults.standard.string(forKey: Constants.UserDefaultsKeys.email)
    }
    
    @IBAction func updateLanguage(_ sender: Any) {
        let alert = UIAlertController(
            title: "are you sure ?".localized,
            message: "this will close the app",
            preferredStyle: .alert
        )
        
        alert.addAction(
            UIAlertAction(title: "Yes", style: .default, handler: { (_) in
                Language.language = self.selectedLanguage
                exit(0)
            })
        )
        
        alert.addAction(
            UIAlertAction(
                title: "Cancel".localized,
                style: UIAlertAction.Style.cancel,
                handler: nil
            )
        )
        present(alert, animated: true, completion: nil)
    }
}

extension SettingViewController: KdipaDropDownDelegate {
    func didSelectItem(sender: KdipaDropDown, index: Int, newValue: String, oldValue: String) {
        if newValue == Localizable.english {
            self.selectedLanguage = .english
        } else {
            self.selectedLanguage = .arabic
        }
    }
}
