//
//  OfficialDuty.swift
//  Kdipa
//
//  Created by Safoine Moncef Amine on 3/11/20.
//  Copyright © 2020 Safoine Moncef Amine. All rights reserved.
//

import Foundation

class OfficialDutyViewModel {
    
    // MARK: Structs
    
    struct OfficialDutyDates {
        var fromDate: Box<Date?> = Box(nil)
        var toDate: Box<Date?> = Box(nil)
    }
    
    // MARK: Variables
    
    private let service = OfficalDutyService()
    var loadOfficialDutyAPICallState: Box<ApiCallState> = Box(.inactive)
    var submitOfficialDutyAPICallState: Box<ApiCallState> = Box(.inactive)
    var dates: OfficialDutyDates = OfficialDutyDates()
    var selectedAttendanceExemption: String = ""
    var officialDuty: OfficialDuty? {
        didSet {
            self.dates.fromDate.value = self.officialDuty?.fromDate.getDate()
            self.dates.toDate.value = self.officialDuty?.toDate.getDate()
            self.selectedtype = self.dutyTypes[exist: 0] ?? ""
            self.selectedEmployee = self.employees[exist: 0] ?? ""
            self.selectedDelegates.append(self.delegatedEmployees[exist: 0] ?? "")
        }
    }
    
    var attendanceExemption: [String] {
        return ((Localizable.lang == Language.english.rawValue) ?
            self.officialDuty?.attendanceExemption :
            self.officialDuty?.attendanceExemptionAr) ?? []
    }
    
    var employees: [String] {
        return ((Localizable.lang == Language.english.rawValue) ?
            self.officialDuty?.employees :
            self.officialDuty?.employeesAr)?.map({$0.employeeName}) ?? []
    }
    
    var delegatedEmployees: [String] {
        return ((Localizable.lang == Language.english.rawValue) ?
            self.officialDuty?.delegatedEmployees :
            self.officialDuty?.delegatedEmployeesAr)?.map({$0.employeeName}) ?? []
    }
    
    var dutyTypes: [String] {
        return ((Localizable.lang == Language.english.rawValue) ?
            self.officialDuty?.officialDutyDutyTypes :
            self.officialDuty?.officialDutyDutyTypesAr)?.map({$0.dutyName}) ?? []
    }
    
    var selectedtype: String = ""
    var selectedEmployee: String = ""
    var selectedDelegates: [String] = []
    
    func loadOfficialDuty() {
        self.loadOfficialDutyAPICallState.value = .running
        let email = UserDefaults.standard.string(forKey: Constants.UserDefaultsKeys.email) ?? ""
        self.service.loadOfficialDuty(email: email) { (response, error) in
            if response != nil && error == nil {
                self.officialDuty = response
                self.loadOfficialDutyAPICallState.value = .success
            } else {
                self.loadOfficialDutyAPICallState.value = .failed
            }
        }
    }
    
    func submitNewOfficialDuty() {
        self.submitOfficialDutyAPICallState.value = .running
        let email = UserDefaults.standard.string(forKey: Constants.UserDefaultsKeys.email) ?? ""
        let selectedDelegatesDescription = self.selectedDelegates.reduce("") { (result, nextValue) -> String in
            if result.isEmpty {return nextValue}
            return result + "," + nextValue
        }
        let officialDuty = OfficialDutyPayload(email: email,
                                               dutyType: self.selectedtype,
                                               fromDate: self.dates.fromDate.value?.formatTo(format: "yyyy/mm/dd") ?? "",
                                               toDate: self.dates.fromDate.value?.formatTo(format: "yyyy/mm/dd") ?? "",
                                               attendanceExemption: self.selectedAttendanceExemption,
                                               employees: self.selectedEmployee,
                                               delegatedEmployees: selectedDelegatesDescription)
        
        self.service.submitNewVacationsReturnn(payload: officialDuty) { (error) in
            if error == nil {
                self.submitOfficialDutyAPICallState.value = .success
            } else {
                self.submitOfficialDutyAPICallState.value = .failed
            }
        }
    }
}
