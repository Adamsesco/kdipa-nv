//
//  VacationReturnService.swift
//  Kdipa
//
//  Created by Safoine Moncef Amine on 3/10/20.
//  Copyright © 2020 Safoine Moncef Amine. All rights reserved.
//

import Foundation
import Alamofire

struct VacationReturnService {
    
    func loadVacations(email: String, completion: @escaping (VacationResponse?, Error?) -> Void) {
        let url = URL(string: Constants.kdipaEmployeeBaseURL + KdipaAPI.vacations.path)!
        let params = ["email": email]
        let headers = ["Content-Type": "application/json", "Accept": "application/json"]
        Alamofire.request(url, method: .post, parameters: params, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
            switch response.result {
            case .success(let value):
                let dictValue = value as? [String: Any]
                guard let data = dictValue?.jsonStringRepresentation?.data(using: .utf8) else {
                    return
                }
                do {
                    let vacationResponse = try JSONDecoder().decode(VacationResponse.self, from: data)
                    completion(vacationResponse, nil)
                } catch {
                    completion(nil, error)
                }
            case .failure(let error):
                completion(nil, error)
            }
        }
    }
    
    func submitNewVacationsReturnn(payload: VacationReturnPayload,
                                   completion: @escaping (Error?) -> Void) {
        let url = URL(string: Constants.kdipaEmployeeBaseURL + KdipaAPI.postVacationReturn.path)!
        let params = payload.dictionary
        let headers = ["Content-Type": "application/json", "Accept": "application/json"]
        Alamofire.request(url, method: .post, parameters: params, encoding: JSONEncoding.default, headers: headers).responseData { (response) in
            switch response.result {
            case .success:
                completion(nil)
            case .failure(let error):
                completion(error)
            }
        }
    }
}
