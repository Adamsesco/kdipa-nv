//
//  Dictionary + extension.swift
//  Kdipa
//
//  Created by Safoine Moncef Amine on 2/29/20.
//  Copyright © 2020 Safoine Moncef Amine. All rights reserved.
//

import Foundation

extension Dictionary {
    var jsonStringRepresentation: String? {
        guard let theJSONData = try? JSONSerialization.data(withJSONObject: self,
                                                            options: [.prettyPrinted]) else {
            return nil
        }
        
        return String(data: theJSONData, encoding: .utf8)
    }
}
