//
//  NewPermissionViewController.swift
//  Kdipa
//
//  Created by Safoine Moncef Amine on 2/17/20.
//  Copyright © 2020 Safoine Moncef Amine. All rights reserved.
//

import UIKit
import FSCalendar
import MaterialComponents.MaterialRipple
import MobileCoreServices
import Alamofire

class NewPermissionViewController: BaseViewController {

    // MARK: - Outlets
    
    @IBOutlet weak var dutyStartDateField: KdipaDateField!
    @IBOutlet weak var dutyEndDateField: KdipaDateField!
    @IBOutlet weak var dutyStartStack: UIStackView!
    @IBOutlet weak var dutyEndStack: UIStackView!
    @IBOutlet weak var checkBoxDutyStart: KdipaCheckBox!
    @IBOutlet weak var checkBoxDutyEnd: KdipaCheckBox!
    @IBOutlet weak var duringDutyStartDateField: KdipaDateField!
    @IBOutlet weak var duringDutyEndDateField: KdipaDateField!
    @IBOutlet weak var checkBoxDuringDuty: KdipaCheckBox!
    @IBOutlet weak var calender: FSCalendar!
    @IBOutlet weak var btnDone: UIButton!
    @IBOutlet weak var btnSubmit: UIButton!
    @IBOutlet weak var btnAM: UIButton!
    @IBOutlet weak var btnPM: UIButton!
    @IBOutlet weak var constraintsCalenderLeading: NSLayoutConstraint!
    @IBOutlet weak var hoursPickerView: UIPickerView!
    @IBOutlet weak var minutesPicker: UIPickerView!
    
    @IBOutlet weak var ampmStack: UIStackView!
    @IBOutlet weak var ampmBackGround: UIView!
    @IBOutlet weak var selectedImage: UIImageView!
    
    @IBOutlet weak var lblScreenTitle: KdipaLabel!
    @IBOutlet weak var lblDutyStartDesc: UILabel!
    @IBOutlet weak var lblDutyEndDesc: UILabel!
    @IBOutlet weak var lblDuringDutyDesc: UILabel!
    
    // MARK: - Properties
    
    var selectedDateField: KdipaDateField?
    var viewModel: NewPermissionViewModel = NewPermissionViewModel()
    var imagePicker: ImagePicker!
    
    // MARK: - view life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupNavigationBarItems()
        self.configureGestures()
        self.configure()
        self.localize()
        self.imagePicker = ImagePicker(presentationController: self, delegate: self)
        self.selectedImage.isHidden = true
        self.showCalenderItems()
    }
    
    // MARK: - Configuration
    
    func configure() {
        self.hoursPickerView.dataSource = self
        self.hoursPickerView.delegate = self
        self.minutesPicker.dataSource = self
        self.minutesPicker.delegate = self
        self.viewModel.submittingPermissionCallState.bind { (state) in
            switch state {
            case .running:
                self.showSpinner(uiView: self.navigationController?.view ?? self.view)
            case .success:
                self.hideSpinner()
                self.showConfirmationMessage(view: self.view, message: "permission successfully sent")
            case .failed:
                self.hideSpinner()
                self.showConfirmationMessage(view: self.view, message: "something went wrong")
            default:
                self.hideSpinner()
            }
        }
    }
    
    func localize() {
        self.lblScreenTitle.text = Localizable.NewPermission.title
        self.lblDutyEndDesc.text = Localizable.NewPermission.dutyEnd
        self.lblDutyStartDesc.text = Localizable.NewPermission.dutyStart
        self.lblDuringDutyDesc.text = Localizable.NewPermission.duringDuty
        self.btnSubmit.setTitle(Localizable.NewPermission.submit, for: .normal)
    }
    // MARK: - UI Actions
    
    @IBAction func btnDoneClicked(_ sender: Any) {
        if self.viewModel.permission.permissionType.isEmpty {
            self.showConfirmationMessage(view: self.view, message: "please select a permission type")
            return
        }
        if viewModel.isPermissionDatesSelected {
            self.hideCalenderItems()
        } else {
            self.showConfirmationMessage(view: self.view, message: "please select date and time")
        }
    }
    
    @IBAction func btnSubmitClicked(_ sender: Any) {
        self.viewModel.submitNewPermission()
    }
    
    @IBAction func btnAMClicked(_ sender: Any) {
        self.btnAM.titleLabel?.textColor = UIColor.kdipaDarkBlue
        self.btnPM.titleLabel?.textColor = UIColor.kdipaLightBlue
        self.btnAM.titleLabel?.font = UIFont.frutigerBold(size: 17)
        self.btnPM.titleLabel?.font = UIFont.frutigerRegular(size: 15)
        self.viewModel.istimeInAM = true
    }
    
    @IBAction func btnPMClicked(_ sender: Any) {
        self.btnAM.titleLabel?.textColor = UIColor.kdipaLightBlue
        self.btnPM.titleLabel?.textColor = UIColor.kdipaDarkBlue
        self.btnPM.titleLabel?.font = UIFont.frutigerBold(size: 17)
        self.btnAM.titleLabel?.font = UIFont.frutigerRegular(size: 15)
        self.viewModel.istimeInAM = false
    }
    
    @IBAction func btnAddAttachementClicked(_ sender: UIButton) {
        self.imagePicker.present(from: sender)
    }
    
    // MARK: - UI handlers Methodes
    
    func showCalenderItems() {
        UIView.animate(withDuration: 2) {
            self.calender.isHidden = false
            self.hoursPickerView.isHidden = false
            self.minutesPicker.isHidden = false
            self.ampmStack.isHidden = false
            self.ampmBackGround.isHidden = false
            self.btnDone.isHidden = false
            self.btnSubmit.isHidden = true
        }
    }
    
    func hideCalenderItems() {
        UIView.animate(withDuration: 2) {
            self.calender.isHidden = true
            self.hoursPickerView.isHidden = true
            self.minutesPicker.isHidden = true
            self.ampmStack.isHidden = true
            self.ampmBackGround.isHidden = true
            self.btnDone.isHidden = true
            self.btnSubmit.isHidden = false
        }
    }
}

// MARK: - Gestures

extension NewPermissionViewController {
    
    func configureGestures() {
        let startDutyTapGesture = UITapGestureRecognizer(target: self, action: #selector(self.didSelectStartDutyField))
        self.dutyStartStack.addGestureRecognizer(startDutyTapGesture)
        
        let endDutyTapGesture = UITapGestureRecognizer(target: self, action: #selector(self.didSelectEndDutyField))
        self.dutyEndStack.addGestureRecognizer(endDutyTapGesture)
        
        let duringDutyStartTapGesture = UITapGestureRecognizer(target: self, action: #selector(self.duringDutyStartSelected))
        self.duringDutyStartDateField.addGestureRecognizer(duringDutyStartTapGesture)
        
        let duringDutyEndTapGesture = UITapGestureRecognizer(target: self, action: #selector(self.duringDutyEndSelected))
        self.duringDutyEndDateField.addGestureRecognizer(duringDutyEndTapGesture)
    }
    
    @objc func didSelectStartDutyField() {
        self.showCalenderItems()
        self.checkBoxDuringDuty.isChecked = false
        self.checkBoxDutyStart.isChecked = true
        self.checkBoxDutyEnd.isChecked = false
        self.selectedDateField = self.dutyStartDateField
        let hoursRow = self.viewModel.hours.firstIndex(of: String(self.viewModel.permission.fromTime.split(separator: ":")[0])) ?? 0
        self.hoursPickerView.selectRow(hoursRow, inComponent: 0, animated: true)
        let minutesRow = self.viewModel.minutes.firstIndex(of: String(self.viewModel.permission.fromTime.split(separator: ":")[1])) ?? 0
        self.minutesPicker.selectRow(minutesRow, inComponent: 0, animated: true)
        self.viewModel.permission.permissionType = "1"
    }
    
    @objc func didSelectEndDutyField() {
        self.showCalenderItems()
        self.checkBoxDuringDuty.isChecked = false
        self.checkBoxDutyStart.isChecked = false
        self.checkBoxDutyEnd.isChecked = true
        self.selectedDateField = self.dutyEndDateField
        let hoursRow = self.viewModel.hours.firstIndex(of: String(self.viewModel.permission.toTime.split(separator: ":")[0])) ?? 0
        self.hoursPickerView.selectRow(hoursRow, inComponent: 0, animated: true)
        let minutesRow = self.viewModel.minutes.firstIndex(of: String(self.viewModel.permission.toTime.split(separator: ":")[1])) ?? 0
        self.minutesPicker.selectRow(minutesRow, inComponent: 0, animated: true)
        self.viewModel.permission.permissionType = "2"
    }
    
    @objc func duringDutyStartSelected() {
        self.showCalenderItems()
        self.checkBoxDuringDuty.isChecked = true
        self.checkBoxDutyStart.isChecked = false
        self.checkBoxDutyEnd.isChecked = false
        self.selectedDateField = self.duringDutyStartDateField
        let hoursRow = self.viewModel.hours.firstIndex(of: String(self.viewModel.permission.fromTime.split(separator: ":")[0])) ?? 0
        self.hoursPickerView.selectRow(hoursRow, inComponent: 0, animated: true)
        let minutesRow = self.viewModel.minutes.firstIndex(of: String(self.viewModel.permission.fromTime.split(separator: ":")[1])) ?? 0
        self.minutesPicker.selectRow(minutesRow, inComponent: 0, animated: true)
        self.viewModel.permission.permissionType = "3"
    }
    
    @objc func duringDutyEndSelected() {
        self.showCalenderItems()
        self.checkBoxDuringDuty.isChecked = true
        self.checkBoxDutyStart.isChecked = false
        self.checkBoxDutyEnd.isChecked = false
        self.selectedDateField = self.duringDutyEndDateField
        let hoursRow = self.viewModel.hours.firstIndex(of: String(self.viewModel.permission.toTime.split(separator: ":")[0])) ?? 0
        self.hoursPickerView.selectRow(hoursRow, inComponent: 0, animated: true)
        let minutesRow = self.viewModel.minutes.firstIndex(of: String(self.viewModel.permission.toTime.split(separator: ":")[1])) ?? 0
        self.minutesPicker.selectRow(minutesRow, inComponent: 0, animated: true)
        self.viewModel.permission.permissionType = "3"
    }
}

// MARK: - Calender delegates

extension NewPermissionViewController: FSCalendarDelegate {
    func calendar(_ calendar: FSCalendar, didSelect date: Date, at monthPosition: FSCalendarMonthPosition) {
        self.viewModel.permission.trxDate = date.description
        guard let dateField = self.selectedDateField else {
            return
        }
        let date = self.viewModel.formatDateForDateField(date: date)
        if selectedDateField == duringDutyStartDateField || selectedDateField == duringDutyEndDateField {
            duringDutyStartDateField.fillTextField(date)
            duringDutyEndDateField.fillTextField(date)
        } else {
            dateField.fillTextField(date)
        }
    }
}

// MARK: - Collection View Data Source

extension NewPermissionViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 48
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = UICollectionViewCell()
        return cell
    }
}

// MARK: - UIPickerViewDataSource delegates and datasource

extension NewPermissionViewController: UIPickerViewDataSource, UIPickerViewDelegate {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if pickerView == self.hoursPickerView {
            return self.viewModel.hours.count
        } else {
            return self.viewModel.minutes.count
        }
        
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if pickerView == self.hoursPickerView {
            return self.viewModel.hours[row]
        } else {
            return self.viewModel.minutes[row]
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if selectedDateField == self.dutyStartDateField  || selectedDateField == self.dutyEndDateField {
            
            let hours = self.viewModel.permission.fromTime.split(separator: ":")[exist: 0] ?? " "
            let minutes = self.viewModel.permission.fromTime.split(separator: ":")[exist: 1] ?? " "
            if pickerView == self.hoursPickerView {
                self.viewModel.permission.fromTime = self.viewModel.hours[row] + ":" + minutes
                self.viewModel.permission.toTime = self.viewModel.hours[row] + ":" + minutes
            } else {
                self.viewModel.permission.fromTime = hours + ":" + self.viewModel.minutes[row]
                self.viewModel.permission.toTime = hours + ":" + self.viewModel.minutes[row]
            }
            
        } else if selectedDateField == self.duringDutyStartDateField {
            let fromHours = self.viewModel.permission.fromTime.split(separator: ":")[exist: 0] ?? " "
            let fromMinutes = self.viewModel.permission.fromTime.split(separator: ":")[exist: 1] ?? " "
            if pickerView == self.hoursPickerView {
                self.viewModel.permission.fromTime = self.viewModel.hours[row] + ":" + fromMinutes
            } else {
                self.viewModel.permission.fromTime = fromHours + ":" + self.viewModel.minutes[row]
            }
        } else {
            let toHours = self.viewModel.permission.toTime.split(separator: ":")[exist: 0] ?? " "
            let toMinutes = self.viewModel.permission.toTime.split(separator: ":")[exist: 1] ?? " "
            if pickerView == self.hoursPickerView {
                self.viewModel.permission.toTime = self.viewModel.hours[row] + ":" + toMinutes
            } else {
                self.viewModel.permission.toTime = toHours + ":" + self.viewModel.minutes[row]
            }
        }
    }
}

// MARK: - ImagePickerDelegate

extension NewPermissionViewController: ImagePickerDelegate {
    
    func didSelect(image: UIImage?) {
        self.selectedImage.isHidden = false
        self.selectedImage.image = image
        self.viewModel.permission.filename = image?.pngData()
    }
}
