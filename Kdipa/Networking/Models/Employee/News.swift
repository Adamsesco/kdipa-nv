//
//  News.swift
//  Kdipa
//
//  Created by Safoine Moncef Amine on 3/8/20.
//  Copyright © 2020 Safoine Moncef Amine. All rights reserved.
//

import Foundation

// MARK: - News
struct News: Codable {
    let date, title, titleAr, content: String
    let contentAr: String
}
