//
//  BookingPayload.swift
//  Kdipa
//
//  Created by Safoine Moncef Amine on 3/30/20.
//  Copyright © 2020 Safoine Moncef Amine. All rights reserved.
//

import Foundation

struct BookingPayload: Codable {
    let bookingServiceID, accountManagerID: Int
    let meetingDate, meetingTime: String

    enum CodingKeys: String, CodingKey {
        case bookingServiceID = "bookingServiceId"
        case accountManagerID = "accountManagerId"
        case meetingDate, meetingTime
    }
}
