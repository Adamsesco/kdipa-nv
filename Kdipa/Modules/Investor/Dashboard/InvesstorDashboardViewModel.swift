//
//  InvesstorDashboardViewModel.swift
//  Kdipa
//
//  Created by Safoine Moncef Amine on 3/29/20.
//  Copyright © 2020 Safoine Moncef Amine. All rights reserved.
//

import Foundation

class InvesstorDashboardViewModel {
    
    var service = InvestorDashboardService()
    var callAPI: Box<ApiCallState> = Box(.inactive)
    var dashboard: InvestorDashboard?
    
    func loadDashboard() {
        self.callAPI.value = .running
        service.loadInvestorDashboard { (dashboard, error) in
            if error == nil {
                self.dashboard = dashboard
                self.callAPI.value = .success
            } else {
                self.callAPI.value = .failed
            }
        }
    }
}
