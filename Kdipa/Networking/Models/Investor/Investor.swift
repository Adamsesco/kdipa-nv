//
//  Investor.swift
//  Kdipa
//
//  Created by Safoine Moncef Amine on 3/26/20.
//  Copyright © 2020 Safoine Moncef Amine. All rights reserved.
//

import Foundation

// MARK: - Investor
struct Investor: Codable {
    let token, firstName, lastName, userEmail: String
    let moci, isAccountManager: Bool
    let investorID: Int
    enum CodingKeys: String, CodingKey {
        case token, firstName, lastName, userEmail, moci, isAccountManager
        case investorID = "id"
    }
}
