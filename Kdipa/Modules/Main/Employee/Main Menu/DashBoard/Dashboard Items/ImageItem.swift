//
//  SecondItem.swift
//  Kdipa
//
//  Created by Safoine Moncef Amine on 2/12/20.
//  Copyright © 2020 Safoine Moncef Amine. All rights reserved.
//

import UIKit

enum DashboardType {
    case attendeddays
    case vacation
    case absence
}

class ImageItem: UIView {
    
    @IBOutlet var contentView: UIView!
    @IBOutlet weak var lblValue: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var imgIcon: UIImageView!
    
    // MARK: - inits
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        sharedInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        sharedInit()
    }
    
    func sharedInit() {
        let nib = UINib(nibName: "ImageItem", bundle: nil)
        nib.instantiate(withOwner: self, options: nil)
        contentView.frame = bounds
        addSubview(contentView)
    }
    
    // MARK: - configure
    
    func configure(type: DashboardType, value: String, description: String) {
        self.lblValue.text = value
        self.lblDescription.text = description
        switch type {
        case .absence:
            self.imgIcon.image = UIImage(named: "cross")
        case .attendeddays:
            self.imgIcon.image = UIImage(named: "calender")
        case .vacation:
            self.imgIcon.image = UIImage(named: "sun")
        }
    }
    func configure(type: DashboardType, dashBoard: DashboardResponse) {
        switch type {
        case .absence:
            self.imgIcon.image = UIImage(named: "cross")
            self.lblDescription.text = Localizable.Dashboard.absences
            self.lblValue.text = "\(dashBoard.absences)"
        case .attendeddays:
            self.imgIcon.image = UIImage(named: "calender")
            self.lblDescription.text = Localizable.Dashboard.attendedDays
            self.lblValue.text = "\(dashBoard.attendedDays)"
        case .vacation:
            self.imgIcon.image = UIImage(named: "sun")
            self.lblDescription.text = Localizable.Dashboard.vacations
            self.lblValue.text = "\(dashBoard.vacations)"
        }
        
    }
}
