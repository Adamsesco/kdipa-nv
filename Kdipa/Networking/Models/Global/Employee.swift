//
//  Employee.swift
//  Kdipa
//
//  Created by Safoine Moncef Amine on 3/8/20.
//  Copyright © 2020 Safoine Moncef Amine. All rights reserved.
//

import Foundation

struct Employee: Codable {
    
    let email: String
    let employeeID: Int
    let employeeName: String

    enum CodingKeys: String, CodingKey {
        case email = "Email"
        case employeeID = "EmployeeID"
        case employeeName = "EmployeeName"
    }
    
    init(dict: [String: Any]) {
        self.email = dict["Email"] as? String ?? ""
        self.employeeID = dict["EmployeeID"] as? Int ?? 0
        self.employeeName = dict["EmployeeName"] as? String ?? ""
    }
}
