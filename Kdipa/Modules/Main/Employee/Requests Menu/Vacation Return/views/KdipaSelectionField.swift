//
//  SelectionField.swift
//  Kdipa
//
//  Created by Safoine Moncef Amine on 3/10/20.
//  Copyright © 2020 Safoine Moncef Amine. All rights reserved.
//

import UIKit

class KdipaSelectionField: UIView {
    
    // MARK: - Enums
    
    enum SelectionFieldState {
        case selected
        case unselected
    }
    
    // MARK: - Outlets
    
    @IBOutlet var contentView: UIView!
    @IBOutlet weak var lblFieldDescription: UILabel!
    @IBOutlet weak var checkBox: KdipaCheckBox!
    
    // MARK: - Variables
    
    var isSelected: Bool {
        return self.state == .selected
    }
    
    private var state: SelectionFieldState = .unselected {
        didSet {
            switch self.state {
            case .selected:
                self.checkBox.isChecked = true
                self.lblFieldDescription.font = UIFont.frutigerBold(size: 16)
            case .unselected:
                self.checkBox.isChecked = false
                self.lblFieldDescription.font = UIFont.frutigerRegular(size: 16)
            }
        }
    }
    
    // MARK: - inits
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        sharedInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        sharedInit()
    }
    
    func sharedInit() {
        let nib = UINib(nibName: "KdipaSelectionField", bundle: nil)
        nib.instantiate(withOwner: self, options: nil)
        contentView.frame = bounds
        addSubview(contentView)
    }
    
    // MARK: - configure
    
    func configure(description: String, selected: Bool) {
        self.lblFieldDescription.text = description
        self.state = selected ? .selected : .unselected
    }
    
    // MARK: - Handlers
    
    func toggleFieldState() {
        if self.state == .selected {
            self.state = .unselected
        } else {
            self.state = .selected
        }
    }
    
    func select() {
        self.state = .selected
    }
    
    func unselect() {
        self.state = .unselected
    }
}
