//
//  BookingService.swift
//  Kdipa
//
//  Created by Safoine Moncef Amine on 3/30/20.
//  Copyright © 2020 Safoine Moncef Amine. All rights reserved.
//

import Foundation
import Alamofire


// MARK: - BookingError
struct BookingError: Codable {
    let code: String
}

struct BookingService {
    
    func bookNewAppointment(payload: BookingPayload,
                            completion: @escaping (BookingResult?, Error?) -> Void) {
        let url = URL(string: Constants.kdipaInvestorBaseURL + KdipaAPI.booking.path)!
        let params = payload.dictionary
        let headers = ["Content-Type": "application/json",
                       "API-KEY": "API-KEY-MOBILE-123",
                       "X-Auth-Token": SessionHandler.shared.connectedInvestor?.token ?? ""]
        Alamofire.request(url, method: .post, parameters: params, encoding: JSONEncoding.default, headers: headers).responseData { (response) in
            switch response.result {
            case .success(let value):
                do {
                    if let statusCode = response.response?.statusCode, statusCode == 201 {
                        let bookingResult = try JSONDecoder().decode(BookingResult.self, from: value)
                        completion(bookingResult, nil)
                    } else {
                        let bookingError = try JSONDecoder().decode(BookingError.self, from: value)
                        completion(nil, NSError(domain: bookingError.code, code: -1, userInfo: nil))
                    }
                } catch {
                    completion(nil, error)
                }
            case .failure(let error):
                completion(nil, error)
            }
        }
    }
}
