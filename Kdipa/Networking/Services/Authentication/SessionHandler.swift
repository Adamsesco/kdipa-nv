//
//  LoginService.swift
//  Kdipa
//
//  Created by Safoine Moncef Amine on 2/9/20.
//  Copyright © 2020 Safoine Moncef Amine. All rights reserved.
//

import Foundation
import Alamofire

class SessionHandler {
    
    var connectedEmployee: EmployeeLoginResponse?
    var connectedInvestor: Investor?
    
    static let shared = SessionHandler()
    private init() {}
    func loginAsEmployee(
        email: String, password: String,
        completion: @escaping ([String: Any]?, Error?) -> Void) {
        let url = URL(string: Constants.kdipaEmployeeBaseURL + KdipaAPI.login.path)!
        let params = ["email": email, "password": password, "token": "", "device_type": "IOS"]
        let headers = ["Content-Type": "application/json", "Accept": "application/json"]
        Alamofire.request(url, method: .post, parameters: params, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
            switch response.result {
            case .success(let value):
                self.connectedEmployee = EmployeeLoginResponse(dict: value as? [String: Any] ?? [:])
                completion(value as? [String: Any], nil)
            case .failure(let error):
                completion(nil, error)
            }
        }
    }
    
    func loginAsInvestor(
        email: String, password: String,
        completion: @escaping (Investor?, Error?) -> Void) {
        let url = URL(string: Constants.kdipaInvestorBaseURL + KdipaAPI.investorLogin.path)!
        let params = ["username": email, "password": password]
        let headers = ["Content-Type": "application/json", "Accept": "application/json"]
        Alamofire.request(url, method: .post, parameters: params, encoding: JSONEncoding.default, headers: headers).responseData { (response) in
            switch response.result {
            case .success(let value):
                do {
                    let investor = try JSONDecoder().decode(Investor.self, from: value)
                    completion(investor, nil)
                } catch {
                    completion(nil, error)
                }
            case .failure(let error):
                completion(nil, error)
            }
        }
    }
    
    func getDashBoard(for email: String, completion: @escaping ([String: Any]?, Error?) -> Void) {
        let url = URL(string: Constants.kdipaEmployeeBaseURL + KdipaAPI.dashboard.path)!
        let params = ["email": email]
        let headers = ["Content-Type": "application/json", "Accept": "application/json"]
        Alamofire.request(url, method: .post, parameters: params, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
            switch response.result {
            case .success(let value):
                let dashboard = value as? [String: Any]
                completion(dashboard, nil)
            case .failure(let error):
                completion(nil, error)
            }
        }
    }
}
