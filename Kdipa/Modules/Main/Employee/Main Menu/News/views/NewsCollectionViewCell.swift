//
//  NewsCollectionViewCell.swift
//  Kdipa
//
//  Created by Safoine Moncef Amine on 3/8/20.
//  Copyright © 2020 Safoine Moncef Amine. All rights reserved.
//

import UIKit

class NewsCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblContentDescription: UILabel!
        
    func configure(news: News) {
        self.lblTitle.text = news.title
        self.lblDate.text = news.date.formatDateToFormat(fromFormat: "yyyy-MM-dd'T'HH:mm:ss.SSSZ",
                                                         toformat: "EEE, MMM dd - hh:mm a")
        self.lblContentDescription.text = news.content
    }
}
