//
//  AppDelegate.swift
//  Kdipa
//
//  Created by Safoine Moncef Amine on 2/5/20.
//  Copyright © 2020 Safoine Moncef Amine. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var coordinator: MainCoordinator?
    let currentLanguage = "en"
    static let navController = UINavigationController()
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // create the main navigation controller to be used for our app
        
        
        // send that into our coordinator so that it can display view controllers
        coordinator = MainCoordinator(navigationController: AppDelegate.navController)
        
        // tell the coordinator to take over control
        coordinator?.start()
        
        // create a basic UIWindow and activate it
        window = UIWindow(frame: UIScreen.main.bounds)
        window?.rootViewController = AppDelegate.navController
        window?.makeKeyAndVisible()
        
        return true
    }
}
