//
//  Constant.swift
//  Kdipa
//
//  Created by Safoine Moncef Amine on 2/7/20.
//  Copyright © 2020 Safoine Moncef Amine. All rights reserved.
//

import Foundation

struct Constants {
    static var isLive: Bool = false
    static var kdipaEmployeeBaseURL: String {
        return "http://62.150.248.71:3001"
    }
    
    static let kdipaInvestorBaseURL = "http://62.150.248.68:8080"
    static let timeoutSeconds = 30.0
    static var isPunchInLast = false
    struct UserDefaultsKeys {
        static var email: String = "emailKey"
        static var password: String = "passwordKey"
        static var dashboard: String = "SavedDashboard"
    }
}
