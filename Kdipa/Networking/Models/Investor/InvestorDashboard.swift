//
//  InvestorDashboard.swift
//  Kdipa
//
//  Created by Safoine Moncef Amine on 4/1/20.
//  Copyright © 2020 Safoine Moncef Amine. All rights reserved.
//

import Foundation

// MARK: - InvestorDashboard
struct InvestorDashboard: Codable {
    let numberOfAppointments, numberOfApplications: Int?
    let appointmentService, appointmentAccountManager, appointmentMeetingDate, appointmentMeetingTime: String?
    let appointmentDetails, applicationCompany, applicationType, applicationStatus: String?
}
