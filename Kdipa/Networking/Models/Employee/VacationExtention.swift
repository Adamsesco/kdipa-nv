//
//  VacationExtention.swift
//  Kdipa
//
//  Created by Safoine Moncef Amine on 3/9/20.
//  Copyright © 2020 Safoine Moncef Amine. All rights reserved.
//

import Foundation

// MARK: - VacationExtention

struct VacationExtention: Codable {
    let newToDate, oldFromDate, oldToDate: String
    let vacationTypes, vacationTypesAr: [VacationType]
}

// MARK: - VacationExtentionPayload

struct VacationExtentionPayload: Codable {
    let email, vacationType, oldFromDate, oldToDate: String
    let newToDate, delegatedEmployee: String
}
