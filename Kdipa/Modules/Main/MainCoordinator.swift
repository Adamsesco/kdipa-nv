//
//  MainCoordinator.swift
//  Kdipa
//
//  Created by Safoine Moncef Amine on 2/12/20.
//  Copyright © 2020 Safoine Moncef Amine. All rights reserved.
//

import UIKit

class MainCoordinator: Coordinator {
    
    var childCoordinators = [Coordinator]()
    var navigationController: UINavigationController
    
    init(navigationController: UINavigationController) {
        self.navigationController = navigationController
    }
    
    func start() {
        let vc = IntroViewController.instantiate(from: .main)
        navigationController.pushViewController(vc, animated: false)
        vc.navigationController?.navigationBar.isHidden = true
    }
    
    func applyForUser() {
        let applyForUserVC = ApplyUserViewController.instantiate(from: .main)
        navigationController.pushViewController(applyForUserVC, animated: true)
        applyForUserVC.navigationController?.navigationBar.isHidden = false
    }
    
    func forgetPassword() {
        let forgetPasswordVC = ForgetPasswordViewController.instantiate(from: .main)
        navigationController.pushViewController(forgetPasswordVC, animated: true)
        forgetPasswordVC.navigationController?.navigationBar.isHidden = false
    }
    
    func dashboard() {
        let menuCoordinator = MenuCoordinator(navigationController: self.navigationController)
        menuCoordinator.start()
    }
    
    func investor() {
        let menuCoordinator = MenuCoordinator(navigationController: self.navigationController)
        menuCoordinator.start()
    }
}
