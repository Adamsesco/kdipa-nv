//
//  NewVacations.swift
//  Kdipa
//
//  Created by Safoine Moncef Amine on 3/8/20.
//  Copyright © 2020 Safoine Moncef Amine. All rights reserved.
//

import Foundation

// MARK: - NewVacations
struct NewVacations: Codable {
    let delegatedEmployees, delegatedEmployeesAr: [Employee]
    let departureDate, returnDate: String
    let vacationTypes, vacationTypesAr: [VacationType]

}

// MARK: - VacationType
    
struct VacationType: Codable {
    let vacationID: Int
    let vacationName: String

    enum CodingKeys: String, CodingKey {
        case vacationID = "VacationID"
        case vacationName = "VacationName"
    }
}
