//
//  ForgetPasswordService.swift
//  Kdipa
//
//  Created by Safoine Moncef Amine on 6/19/20.
//  Copyright © 2020 Safoine Moncef Amine. All rights reserved.
//

import Foundation
import Alamofire

struct ForgetPasswordService {
        
    func forgetPassword(email: String, completion: @escaping (Error?) -> Void) {
        let url = URL(string: Constants.kdipaInvestorBaseURL + KdipaAPI.forgetPassword.path)!
    let params = ["account": email]
        let headers = ["Content-Type": "application/json", "Accept": "application/json"]
        Alamofire.request(url, method: .post, parameters: params, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
            let status = response.response?.statusCode
            if status == 200 {
                completion(nil)
            } else {
                completion(NSError())
            }
        }
    }
}
