/* 
  Localizable.strings
  Kdipa

  Created by Safoine Moncef Amine on 2/16/20.
  Copyright © 2020 Safoine Moncef Amine. All rights reserved.
*/

// MARK: - Global

"global.language" = "english";

"global.language.en" = "english";
"global.language.ar" = "العربية";

// MARK: - Login

"login.title" = "Welcome";
"login.subtitle" = "Login to the system";
"login.forgetPassword"  = "Forgot Password?";
"login.login" = "Login";
"login.applyUserName" = "Apply for Username";

// MARK: - Employee Setting

"EmployeeSetting.title" = "Settings";
"EmployeeSetting.dropDownDesc" = "Change language";
"EmployeeSetting.btnSave" = "Save Changes";

// MARK: - KdipaTextField

"kdipaTextField.username" = "Username";
"kdipaTextField.password" = "Password";
"kdipaTextField.email" = "Email";
"kdipaTextField.firstName" = "First Name";
"kdipaTextField.lastName" = "Last Name";
"kdipaTextField.company" = "Company";
"kdipaTextField.phoneNumber" = "Phone Number";

// MARK: - Apply For UserName

"applyForUsername.title" = "Apply for username";
"applyForUsername.subtitle" = "All fields are required to be filled";
"applyForUsername.create" = "Create Request";

// MARK: - Side Menu

"sideMenu.home" = "HOME";
"sideMenu.transaction" = "History transactions";
"sideMenu.geolocation" = "Geolocation punch";
"sideMenu.newrequest" = "requests";
"sideMenu.notifications" = "notifications";
"sideMenu.news" = "news";
"sideMenu.setting" = "settings";
"sideMenu.logout" = "logout";
"sideMenu.investorDashboard" = "HOME";
"sideMenu.investorBooking" = "Booking";
"sideMenu.investorHowItWorks" = "How it works";
"sideMenu.investorSetting" = "Settings";

"requestsSideMenu.permission" = "Permission";
"requestsSideMenu.vacation" = "Vacation";
"requestsSideMenu.vacationAmendment" = "Vacation Amendment";
"requestsSideMenu.vacationExtension" = "Vacation Extension";
"requestsSideMenu.vacationReturn" = "Vacation Return";
"requestsSideMenu.emergencyLeave" = "Emergency Leave";
"requestsSideMenu.sickLeave" = "Sick Leave";
"requestsSideMenu.officialDuty" = "Official Duty";
"requestsSideMenu.requestStatus" = "Request Status";
"requestsSideMenu.absence" = "Absence";

// MARK: - Dashboard

"dashboard.title" = "Dashboard";
"dashboard.lastPinchIn" = "Last punch in";
"dashboard.lastPinchOut" = "Last punch out";
"dashboard.permissionsTaken" = "PERMISSIONS TAKEN";
"dashboard.permissionsRemaining" = "PERMISSIONS REMAINING";
"dashboard.vacations" = "VACATIONS";
"dashboard.attendedDays" = "ATTENDED DAYS";
"dashboard.absences" = "ABSENCES";
"dashboard.createNewPermission" = "CREATE NEW PERMISSION";

// MARK: - PunchIn View Controller

"punchIn.label.title" = "Punch In";
"punchIn.button.PunchIn.title" = "Punch in / out";
"punchIn.button.Ok.title" = "Ok";
"punchIn.button.Setting.title" = "Setting";
"punchIn.Alert.error.title" = "Error";
"punchIn.notificationAlert.sucess.title" = "Punch in";
"punchIn.notificationAlert.sucess.message" = "You have successfully Punch In at ";
"punchOut.notificationAlert.sucess.message" = "You have successfully Punch Out at ";
"punchIn.notificationAlert.failure.message" = "something went wrong";

// MARK: - Permission History

"permissions.createNewPermission" = "Permissions";
"permissions.totalPermissions" = "Total permissions ";
"permissions.createPermission" = "Create new permission";
"permissionHistory.permissions" = "Permissions";
"permissionHistory.permissionsRemaining " = "Permissions Remaining ";
"permissionHistory.permissionsTaken" = "Permissions taken";

"permissionCell.type" = "Permission type :";
"permissionCell.from" = "From :";
"permissionCell.to" = "To :";
"permissionCell.duration" = "Duration :";

// MARK: - Vacations

"Vacations.title" = "Vacations";
"Vacations.newVacation" = "New Vacation";

// MARK: - Absences

"Absence.title" = "Absences";

// MARK: - VacationAmendment

"VacationAmendment.title" = "Vacation Amendment";
"VacationAmendment.startFrom" = "Start from";
"VacationAmendment.to" = "TO";
"VacationAmendment.amdFrom" = "Start from";
"VacationAmendment.amdTo" = "TO";
"VacationAmendment.willBeAmended" = "Will be amended to";
"VacationAmendment.delegatedEmployee" = "Delegated Employee";
"VacationAmendment.submit" = "Submit";

// MARK: - VacationExtension

"VacationExtension.title" = "Vacation Extension";
"VacationExtension.startFrom" = "Start from";
"VacationExtension.to" = "To";
"VacationExtension.extTo" = "To";
"VacationExtension.willBeAmended" = "Will be extended to";
"VacationExtension.delegatedEmployee" = "Delegated Employee";
"VacationExtension.submit" = "Submit";

// MARK: - VacationReturn

"VacationReturn.title" = "Vacation Return";
"VacationReturn.returnDate" = "Return Date";
"VacationReturn.didNotDelay" = "I did not delay the date of my return";
"VacationReturn.wasLate" = "I was late for my return date";
"VacationReturn.forFollowingReason" = "For the following reasons :";
"VacationReturn.submit" = "Submit";

// MARK: - OfficialDuty

"OfficialDuty.title" = "Official Duty";
"OfficialDuty.from" = "Start from";
"OfficialDuty.to" = "To";
"OfficialDuty.employee" = "Employee";
"OfficialDuty.delegatedEmployee" = "Delegated employee";
"OfficialDuty.addmoreDelegates" = "Add more delegate:";
"OfficialDuty.submit" = "Submit";

// MARK: - NewVacation

"NewVacation.title" = "New Vacation";
"NewVacation.departureDate" = "From";
"NewVacation.returnDate" = "To";
"NewVacation.delegatedEmployee" = "Delegated Employee";
"NewVacation.submit" = "Submit";
"new.vacation.is.paid" = "Use advanced Payment";

// MARK: - DaysOfAttendance

"DaysOfAttendance.title" = "Days of attendance";
"DaysOfAttendance.attendance" = "Attendance";
"DaysOfAttendance.vacations" = "Vacations";
"DaysOfAttendance.absences" = "Absences";
"DaysOfAttendance.dayesOfpresence" = " days of presence, out of ";
"DaysOfAttendance.days" = "days";

// MARK: - NewPermission

"NewPermission.title" = "New Permission";
"NewPermission.dutyStart" = "Duty Start";
"NewPermission.dutyEnd" = "Duty End";
"NewPermission.duringDuty" = "During Duty";
"NewPermission.submit" = "SUBMIT";

// MARK: - News

"News.title" = "News";

// MARK: - Forget Password

"forgetpassword.updateSuccess" = "A verification email has been sent , please check your inbox";
"forgetpassword.error" = "something went wrong";
"forgetpassword.title" = "Forgot Password";
"forgetpassword.subTitle" = "Enter the email address associated with your account";
"forgetpassword.submit" = "SUBMIT";
