//
//  EmergencyLeave.swift
//  Kdipa
//
//  Created by Said Elmansour on 2020-02-26.
//  Copyright © 2020 Safoine Moncef Amine. All rights reserved.
//

import Foundation

struct EmergencyLeave: Codable {
    var email: String = UserDefaults.standard.string(forKey: Constants.UserDefaultsKeys.email) ?? ""
    var trxDate: String = ""
    var filename: Data?
}
