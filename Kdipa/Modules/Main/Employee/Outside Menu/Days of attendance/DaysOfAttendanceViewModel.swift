//
//  DaysOfAttendance.swift
//  Kdipa
//
//  Created by Safoine Moncef Amine on 3/7/20.
//  Copyright © 2020 Safoine Moncef Amine. All rights reserved.
//

import Foundation

class DaysOfAttendanceViewModel {
    
    let service = DemandesService()
    var years: [String] = []
    var attendanceDays: [DaysOfAttendance]? {
        didSet {
            self.years = self.attendanceDays?.map({ (attendanceDay) -> String in
                return "\(attendanceDay.year)"
            }) ?? []
            self.selectedYear = self.attendanceDays?[exist: 0]?.year.description ?? ""
        }
    }
    var loadDataState: Box<ApiCallState> = Box(.inactive)
    var selectedEmployee: Employee? = SessionHandler.shared.connectedEmployee?.employees?[exist: 0] ??
    Employee(
        dict: ["Email": UserDefaults.standard.string(forKey: Constants.UserDefaultsKeys.email) ?? "",
           "EmployeeName": SessionHandler.shared.connectedEmployee?.firstName ?? ""])
    
    var selectedYear: String = "" {
        didSet {
            self.selectedAttendedDay = self.attendanceDays?.first(where: { (attendance) -> Bool in
                "\(attendance.year)" == self.selectedYear
                })
        }
    }
    
    var selectedAttendedDay: DaysOfAttendance?
    
    var absenceProgress: Float {
        return Float(self.selectedAttendedDay?.absences ?? 0) / Float(self.selectedAttendedDay?.totalDays ?? 1)
    }
    
    var vacationProgress: Float {
        return Float(self.selectedAttendedDay?.vacations ?? 0) / Float(self.selectedAttendedDay?.totalDays ?? 1)
    }
    
    var attendanceProgress: Float {
        return Float(self.selectedAttendedDay?.attendance ?? 0) / Float(self.selectedAttendedDay?.totalDays ?? 1)
    }
    
    var attendancePercentage: String {
        let pourcentage = self.attendanceProgress * 100
        return String(format: "%.f", round(pourcentage))
    }
    
    func reloadData() {
        self.loadDataState.value = .running
        self.service.loadAttendancyDays(email: self.selectedEmployee?.email ?? "") { (response, error) in
            if error == nil {
                self.attendanceDays = response
                self.loadDataState.value = .success
            } else {
                self.loadDataState.value = .failed
            }
        }
    }
}
