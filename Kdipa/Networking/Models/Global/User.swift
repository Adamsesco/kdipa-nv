//
//  User.swift
//  Kdipa
//
//  Created by Safoine Moncef Amine on 2/10/20.
//  Copyright © 2020 Safoine Moncef Amine. All rights reserved.
//

import Foundation

struct User: Codable {
    let firstName: String
    let lastName: String
    let account: String
    let companyName: String
    let phone: String
}
