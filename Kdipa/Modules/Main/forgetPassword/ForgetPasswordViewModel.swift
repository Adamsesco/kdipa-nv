//
//  ForgetPasswordViewModel.swift
//  Kdipa
//
//  Created by Safoine Moncef Amine on 6/19/20.
//  Copyright © 2020 Safoine Moncef Amine. All rights reserved.
//

import Foundation

class ForgetPasswordViewModel {
    
    let service = ForgetPasswordService()
    var forgetPassowrdRequestCallState: Box<ApiCallState> = Box(.inactive)
    func resetPassword(email: String) {
        self.forgetPassowrdRequestCallState.value = .running
        self.service.forgetPassword(email: email) { (error) in
            if error != nil {
                self.forgetPassowrdRequestCallState.value = .failed
            } else {
                self.forgetPassowrdRequestCallState.value = .success
            }
        }
    }
}
