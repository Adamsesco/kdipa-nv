//
//  VacationAmendmentViewController.swift
//  Kdipa
//
//  Created by Said Elmansour on 2020-03-01.
//  Copyright © 2020 Safoine Moncef Amine. All rights reserved.
//

import UIKit
import FSCalendar
import DropDown

enum BtnDatesType {
    case startDate
    case endDate
    case amandStartDate
    case amandEndDate
}

class VacationAmendmentViewController: BaseViewController {
    
    @IBOutlet weak var calendarTrailingConstraint: NSLayoutConstraint!
    @IBOutlet weak var calendarCoàntainer: UIView!
    
    @IBOutlet weak var startDateLabel: UILabel!
    @IBOutlet weak var endDateLabel: UILabel!
    
    @IBOutlet weak var amandEndDateLabel: UILabel!
    @IBOutlet weak var amandStartDateLabel: UILabel!
    @IBOutlet weak var calendar: FSCalendar!
    @IBOutlet weak var typesDropDownContainer: RoundedView!
    @IBOutlet weak var delegatesDropDownContainer: RoundedView!
    @IBOutlet weak var vacationTypesdDropDownContainer: UIView!
    @IBOutlet weak var lblSelectedDelegate: KdipaLabel!
    @IBOutlet weak var lblSelectedVacationType: KdipaLabel!
    
    @IBOutlet weak var lblScreenTitle: KdipaLabel!
    
    @IBOutlet weak var lblStartFrom: UILabel!
    @IBOutlet weak var lbTo: UILabel!
    @IBOutlet weak var lblWillAmendedTo: UILabel!
    
    @IBOutlet weak var lblAmdStartFrom: UILabel!
    @IBOutlet weak var lblAmdTo: UILabel!
    @IBOutlet weak var lblDelegatedEmployee: UILabel!
    @IBOutlet weak var btnSubmit: UIButton!
    
    let vacationTypesDropDown = DropDown()
    let delegatesDropDown = DropDown()
    
    var coordinator: MenuCoordinator?
    
    // MARK: - Properties
    
    var btnType: BtnDatesType?
    var viewModel = VacationAmendmentViewModel()
    
    // MARK: - VC life cycle methodes
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupNavigationBarItems()
        self.bind()
        self.localize()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        viewModel.getvacationAmendment()
    }
    
    // MARK: - configuration
    
    private func bind() {
        // bind showing spinner with Api call
        guard let spinnerContainer = self.navigationController?.view ?? self.view else {
            return
        }
        
        self.viewModel.gettingVAmendmentCallState.bind { (state) in
            switch state {
            case .running:
                self.showSpinner(uiView: spinnerContainer)
            case .success:
                self.hideSpinner()
                self.reconfigure()
            case .failed:
                self.hideSpinner()
                self.showConfirmationMessage(view: self.view, message: "server error")
            default:
                self.hideSpinner()
            }
        }
        
        // bind dates fields
        self.viewModel.vacationAmendmentDates.oldFromDate.bind { (date) in
            self.startDateLabel.text = date?.formatTo(format: "EEE, MMM dd ")
        }
        self.viewModel.vacationAmendmentDates.oldToDate.bind { (date) in
            self.endDateLabel.text = date?.formatTo(format: "EEE, MMM dd ")
        }
        self.viewModel.vacationAmendmentDates.newFromDate.bind { (date) in
            self.amandStartDateLabel.text = date?.formatTo(format: "EEE, MMM dd ")
        }
        self.viewModel.vacationAmendmentDates.newToDate.bind { (date) in
            self.amandEndDateLabel.text = date?.formatTo(format: "EEE, MMM dd ")
        }
        
        // bind submittting new Vacation amendement API call
        
        self.viewModel.submittingVacationAmendmentCallState.bind { (state) in
            switch state {
            case .running:
                self.showSpinner(uiView: spinnerContainer)
            case .success:
                self.hideSpinner()
                self.coordinator?.dashboard()
            case .failed:
                self.hideSpinner()
                self.showConfirmationMessage(view: self.view, message: "server error")
            default:
                self.hideSpinner()
            }
        }
    }
    
    func localize() {
        self.lblScreenTitle.text = Localizable.VacationAmendment.title
        self.lblStartFrom.text = Localizable.VacationAmendment.startFrom
        self.lbTo.text = Localizable.VacationAmendment.to
        
        self.lblWillAmendedTo.text = Localizable.VacationAmendment.willBeAmended
        
        self.lblAmdTo.text = Localizable.VacationAmendment.amdTo
        self.lblAmdStartFrom.text = Localizable.VacationAmendment.startFrom
        
        self.lblDelegatedEmployee.text = Localizable.VacationAmendment.delegatedEmployee
        self.btnSubmit.setTitle(Localizable.VacationAmendment.submit, for: .normal)
    }
    private func reconfigure() {
        configureDropBox()
    }
    
    // MARK: - Local helpers
    
    private func showCalendar() {
        calendarTrailingConstraint.constant = 0
        UIView.animate(withDuration: 0.5) {
            self.view.layoutIfNeeded()
        }
    }
    
    private func hideCalendar() {
        calendarTrailingConstraint.constant = -414
        UIView.animate(withDuration: 0.5) {
            self.view.layoutIfNeeded()
        }
    }
    
    // MARK: - UI Actions
    
    @IBAction func onStart(_ sender: Any) {
        showCalendar()
        btnType = .startDate
    }
    
    @IBAction func onAmendStart(_ sender: Any) {
        showCalendar()
        btnType = .amandStartDate
        
    }
    
    @IBAction func onAmendTo(_ sender: Any) {
        showCalendar()
        btnType = .amandEndDate
        
    }
    
    @IBAction func onTo(_ sender: Any) {
        showCalendar()
        btnType = .endDate
        
    }
    
    @IBAction func onSubmit(_ sender: Any) {
        self.viewModel.submitVacationAmendment()
    }
    
    // MARK: - Local Helpers
    
    func configureDropBox() {
        
        // configure vacation types drop down
        
        guard let types = self.viewModel.vacationType else {
            return
        }
        let namesarray = types.map { $0.vacationName }
        vacationTypesDropDown.dataSource = namesarray 
        vacationTypesDropDown.anchorView = self.vacationTypesdDropDownContainer
        vacationTypesDropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            self.lblSelectedVacationType.text = item
            self.viewModel.payload.vacationType = item
            
        }
        let showDropDownTapGesture = UITapGestureRecognizer(target: self, action: #selector(self.showTypesDropDown))
        self.typesDropDownContainer.addGestureRecognizer(showDropDownTapGesture)
        
        // configure delegates drop down
        
        guard let delegates = SessionHandler.shared.connectedEmployee?.employees?.map({ (employee) -> String in
            return employee.employeeName
        }) else {
            return
        }
        delegatesDropDown.dataSource = delegates
        delegatesDropDown.anchorView = self.delegatesDropDownContainer
        delegatesDropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            self.lblSelectedDelegate.text = item
            self.viewModel.payload.delegatedEmployee = item
        }
        let showDelegatesDropDownTapGesture = UITapGestureRecognizer(target: self, action: #selector(self.showDelegatesDropDown))
        
        self.delegatesDropDownContainer.addGestureRecognizer(showDelegatesDropDownTapGesture)
    }
    
    @objc func showTypesDropDown() {
        self.vacationTypesDropDown.show()
    }
    
    @objc func showDelegatesDropDown() {
        self.delegatesDropDown.show()
    }
}

// MARK: - Calender delegate

extension VacationAmendmentViewController: FSCalendarDelegate {
    func calendar(_ calendar: FSCalendar, didSelect date: Date, at monthPosition: FSCalendarMonthPosition) {
        hideCalendar()
        switch btnType {
        case .startDate:
            self.viewModel.vacationAmendmentDates.oldFromDate.value = date
        case .endDate:
            self.viewModel.vacationAmendmentDates.oldToDate.value = date
        case .amandStartDate:
            self.viewModel.vacationAmendmentDates.newFromDate.value = date
        case .amandEndDate:
            self.viewModel.vacationAmendmentDates.newToDate.value = date
        default:
            break
        }
    }
}
