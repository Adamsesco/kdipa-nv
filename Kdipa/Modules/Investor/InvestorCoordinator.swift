//
//  InvestorCoordinator.swift
//  Kdipa
//
//  Created by Safoine Moncef Amine on 3/26/20.
//  Copyright © 2020 Safoine Moncef Amine. All rights reserved.
//

import UIKit

class InvestorCoordinator: Coordinator {
    
    var childCoordinators: [Coordinator] = []
    var navigationController: UINavigationController
    var parentCoordinator: Coordinator?
    
    init(navigationController: UINavigationController, parentCoordinator: Coordinator) {
        self.navigationController = navigationController
        self.parentCoordinator = parentCoordinator
    }
    
    func start() {
        self.booking()
    }
    
    func booking() {
        let vacationVC = BookingViewController.instantiate(from: .investor)
        navigationController.pushViewController(vacationVC, animated: true)
        vacationVC.navigationController?.navigationBar.isHidden = false
    }
    
}
