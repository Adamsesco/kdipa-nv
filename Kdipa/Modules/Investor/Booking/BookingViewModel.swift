//
//  BookingViewModel.swift
//  Kdipa
//
//  Created by Safoine Moncef Amine on 3/26/20.
//  Copyright © 2020 Safoine Moncef Amine. All rights reserved.
//

import Foundation

class BookingViewModel {
    
    private var service = BookingService()
    
    var bookingCallState: Box<ApiCallState> = Box(.inactive)
    var selectedDate: Date?
    var selectedSlotTime: String?
    var messageToShow = ""
    func bookNewBooking() {
        bookingCallState.value = .running
        let bookAppointement = BookingPayload(bookingServiceID: 1,
                                              accountManagerID: 1,
                                              meetingDate: selectedDate?.formatTo(format: "yyyy-MM-dd") ?? "",
                                              meetingTime: self.selectedSlotTime ?? "")
        self.service.bookNewAppointment(payload: bookAppointement) { (result, error) in
            if error != nil {
                self.messageToShow = (error as NSError?)?.domain ?? ""
                self.bookingCallState.value = .failed
            } else {
                self.messageToShow = "Appointment Booked"
                self.bookingCallState.value = .success
            }
        }
    }
    
    func getTimeByIndex(_ index: Int) -> String {
        switch index {
        case 1:
            return "08_AM"
        case 2:
            return "09_AM"
        case 3:
            return "10_AM"
        case 4:
            return "11_AM"
        case 5:
            return "12_AM"
        case 6:
            return "13_PM"
        case 7:
            return "15_PM"
        case 8:
            return "16_PM"
        case 9:
            return "17_PM"
        default:
            return ""
        }
    }
}


