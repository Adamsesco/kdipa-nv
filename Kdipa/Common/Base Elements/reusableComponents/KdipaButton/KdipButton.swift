//
//  Kdip.swift
//  Kdipa
//
//  Created by Safoine Moncef Amine on 2/8/20.
//  Copyright © 2020 Safoine Moncef Amine. All rights reserved.
//

import UIKit

enum KdipButtonType {
    case filled
    case empty
    case gradient
}

class KdipaButton: RoundedView {
    
    // MARK: - Outlets
    
    @IBOutlet weak var button: UIButton!
    @IBOutlet weak var lblTitle: KdipaLabel!
    @IBOutlet weak var imgIcon: UIImageView!
    @IBOutlet var contentView: UIView!
   
    // MARK: - Properties
   
    @IBInspectable
    var title: String? {
        didSet {
            self.lblTitle.text = title
        }
    }
    
    var type: KdipButtonType = .filled {
        didSet {
            self.configureButton()
        }
    }
    
    // MARK: - inits
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        sharedInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        sharedInit()
    }
    
    func sharedInit() {
        let nib = UINib(nibName: "KdipaButton", bundle: nil)
        nib.instantiate(withOwner: self, options: nil)
        contentView.frame = bounds
        addSubview(contentView)
    }
    
    // MARK: - Configure
    
    fileprivate func configureButton() {
        switch type {
        case .filled:
            self.configureAsFilledButton()
        case .empty:
            self.configureAsEmptyButton()
        case .gradient:
            self.configureAsGradient()
        }
    }
    
    fileprivate func configureAsFilledButton() {
        self.contentView.applyGradient(colours: [UIColor.kdipaDarkBlue, UIColor.kdipaLightBlue],
                           locations: [0, 1],
                           startPoint: CGPoint(x: 0, y: 0),
                           endPoint: CGPoint(x: 1, y: 0))
        self.lblTitle.font = UIFont.frutigerBold(size: 15)
        self.lblTitle.textColor = UIColor.white
        self.imgIcon.tintColor = UIColor.white
        self.cornerRadius = 3
    }
    
    fileprivate func configureAsEmptyButton() {
        self.backgroundColor = UIColor.white
        self.lblTitle.font = UIFont.frutigerRegular(size: 15)
        self.lblTitle.textColor = UIColor.kdipaDarkBlue
        self.borderColor = UIColor.kdipaDarkBlue
        self.borderWidth = 1
        self.cornerRadius = 3
        self.imgIcon.tintColor = UIColor.kdipaDarkBlue
    }
    
    fileprivate func configureAsGradient() {
        let gradientLayer = CAGradientLayer()
        gradientLayer.frame = self.bounds
        gradientLayer.colors = [UIColor.kdipaDarkBlue.cgColor, UIColor.kdipaLightBlue.cgColor]
        self.layer.insertSublayer(gradientLayer, at: 0)
        self.lblTitle.font = UIFont.frutigerBold(size: 15)
        self.lblTitle.textColor = UIColor.white
    }
}
