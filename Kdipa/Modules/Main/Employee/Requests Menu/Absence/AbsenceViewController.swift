//
//  ABsenceViewController.swift
//  Kdipa
//
//  Created by Safoine Moncef Amine on 3/1/20.
//  Copyright © 2020 Safoine Moncef Amine. All rights reserved.
//

import UIKit
import DropDown

class AbsenceViewController: BaseViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var currentName: KdipaLabel!
    @IBOutlet weak var currentYear: KdipaLabel!
    @IBOutlet weak var employeesDropDownContainer: RoundedView!
    @IBOutlet weak var yearsDropDownContainer: RoundedView!
    
    @IBOutlet weak var lblScreenTitle: KdipaLabel!
    
    var viewModel: AbsenceViewModel = AbsenceViewModel()
    var coordinator: MenuCoordinator?
    
    let employeesDropDown: DropDown = DropDown()
    let yearsDropDown: DropDown = DropDown()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupNavigationBarItems()
        configure()
        localize()
    }
    override func viewDidAppear(_ animated: Bool) {
        self.viewModel.loadAbsences()
    }
    func configure() {
        self.tableView.register(UINib.nib(named: VacationTableViewCell.reuseIdentifier),
                                forCellReuseIdentifier: VacationTableViewCell.reuseIdentifier)
        configureDropDowns()
        bind()
    }
    func localize() {
        self.lblScreenTitle.text = Localizable.Absence.title
    }
    func bind() {
        self.viewModel.loadAbsencesAPICallState.bind { (state) in
            switch state {
            case .running:
                self.showSpinner(uiView: self.coordinator?.navigationController.view ?? self.view)
            case .success:
                self.hideSpinner()
                self.reconfigureVacations()
            case .failed:
                self.hideSpinner()
                self.showConfirmationMessage(view: self.view, message: "something went wrong")
            default:
                self.hideSpinner()
            }
        }
    }
    
    func reconfigureVacations() {
        self.currentName.text = self.viewModel.selectedEmployee?.employeeName
        self.currentYear.text = self.viewModel.selectedYear
        self.tableView.reloadData()
        self.configureDropDowns()
    }
    
    func  configureDropDowns() {
        // Configure Employees drop down
        guard let employees = SessionHandler.shared.connectedEmployee?.employees else {
            return
        }
        let array = employees.map { $0.employeeName }
        employeesDropDown.dataSource = array
        employeesDropDown.anchorView = self.employeesDropDownContainer
        employeesDropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            self.viewModel.selectedEmployee = SessionHandler.shared.connectedEmployee?.employees?.first(where: { (employee) -> Bool in
                employee.employeeName == item
            })
            self.viewModel.loadAbsences()
        }
        let showDropDownTapGesture = UITapGestureRecognizer(target: self, action: #selector(self.showEmployeeDropDown))
        self.employeesDropDownContainer.addGestureRecognizer(showDropDownTapGesture)
        
        // Configure years drop down
        
        yearsDropDown.dataSource = viewModel.years
        yearsDropDown.anchorView = self.yearsDropDownContainer
        yearsDropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            self.viewModel.selectedYear = self.viewModel.years.first(where: { (year) -> Bool in
                year == item
            }) ?? ""
            self.reconfigureVacations()
        }
        let showYearsDropDownTapGesture = UITapGestureRecognizer(target: self, action: #selector(self.showYearsDropDown))
        self.yearsDropDownContainer.addGestureRecognizer(showYearsDropDownTapGesture)
    }
    
    @objc func showEmployeeDropDown() {
        self.employeesDropDown.show()
    }
    @objc func showYearsDropDown() {
        self.yearsDropDown.show()
    }
}

extension AbsenceViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.viewModel.absences.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: VacationTableViewCell.reuseIdentifier,
                                                       for: indexPath) as? VacationTableViewCell else { return VacationTableViewCell() }
        cell.configure(row: indexPath.row, absence: self.viewModel.absences[indexPath.row])
        return cell
    }
}
