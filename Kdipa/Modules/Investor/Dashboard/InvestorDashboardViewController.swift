//
//  InvestorDashboardViewController.swift
//  Kdipa
//
//  Created by Safoine Moncef Amine on 3/29/20.
//  Copyright © 2020 Safoine Moncef Amine. All rights reserved.
//

import UIKit
import Charts

class InvestorDashboardViewController: BaseViewController {

    @IBOutlet weak var pieChart: PieChartView!
    @IBOutlet weak var lblTotalRequests: UILabel!
    @IBOutlet weak var lblNumberOfAppoointment: UILabel!
    @IBOutlet weak var lblNumberOfApplications: UILabel!
    @IBOutlet weak var lblAppointmentService: UILabel!
    @IBOutlet weak var lblAppointmentDetails: UILabel!
    @IBOutlet weak var lblApplicationCompany: UILabel!
    @IBOutlet weak var lblProfile: UILabel!
    @IBOutlet weak var lblType: UILabel!
    @IBOutlet weak var lblAppointmentAccountManager: UILabel!
    
    @IBOutlet weak var lblAppointmentDate: UILabel!
    @IBOutlet weak var lblApplicationDate: UILabel!
    
    @IBOutlet weak var lblPourcentage: UILabel!
    
    var coordinator: MenuCoordinator?
    var viewModel = InvesstorDashboardViewModel()
    override func viewDidLoad() {
        super.viewDidLoad()
        super.setupNavigationBarItems()
        self.viewModel.loadDashboard()
        self.bind()
    }
    
    func bind() {
        self.viewModel.callAPI.bind { (state) in
            switch state {
            case .running:
                self.showSpinner(uiView: self.coordinator?.navigationController.view ?? self.view)
            case .success:
                self.configure()
                self.hideSpinner()
            case .failed:
                self.showConfirmationMessage(view: self.view, message: "server error")
                self.hideSpinner()
            default:
                self.hideSpinner()
            }
        }
    }
    
    func configure() {
        self.reconfigurePieChart()
        if let numberOfAppointement = self.viewModel.dashboard?.numberOfAppointments,
            let numberOfApplications =  self.viewModel.dashboard?.numberOfApplications {
            self.lblNumberOfApplications.text = String(numberOfApplications) + " Applications"
            self.lblNumberOfAppoointment.text = String(numberOfAppointement) + " Appointments"
            self.lblTotalRequests.text = String(numberOfAppointement + numberOfApplications)
            self.lblPourcentage.text = String( 100 * Double(numberOfAppointement) / Double(numberOfAppointement) + Double(numberOfApplications)) + "%"
            self.reconfigurePieChart()
        }
        
        self.lblAppointmentService.text = self.viewModel.dashboard?.appointmentService ?? "no data"
        self.lblAppointmentDetails.text = self.viewModel.dashboard?.appointmentDetails ?? "no data"
        self.lblAppointmentAccountManager.text = self.viewModel.dashboard?.appointmentAccountManager ?? "no data"
        self.lblApplicationCompany.text = self.viewModel.dashboard?.applicationCompany ?? "no data"
        self.lblProfile.text = self.viewModel.dashboard?.applicationType ?? "no data"
        self.lblType.text = self.viewModel.dashboard?.applicationStatus ?? "no data"
        
    }
    
    func reconfigurePieChart() {
        var pieEntries: [PieChartDataEntry] = []
        pieChart.legend.enabled = false
        pieChart.noDataText = "no data available"
        let numberOfAppointments: Int = self.viewModel.dashboard?.numberOfAppointments ?? 0
        
        let numberOfApplications: Int = self.viewModel.dashboard?.numberOfApplications ?? 0
        pieEntries.append(PieChartDataEntry(value: Double(numberOfAppointments)))
        pieEntries.append(PieChartDataEntry(value: Double(numberOfApplications)))
        let set: PieChartDataSet = PieChartDataSet(entries: pieEntries)
        set.colors = [UIColor.kdipaDarkBlue, UIColor.kdipaOrange]
        set.selectionShift = 7.0
        let data: PieChartData = PieChartData(dataSet: set)
        data.setDrawValues(false)
        self.pieChart.data = data
    }
}
