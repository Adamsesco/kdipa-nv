//
//  VacationReturnViewModel.swift
//  Kdipa
//
//  Created by Said Elmansour on 2020-03-01.
//  Copyright © 2020 Safoine Moncef Amine. All rights reserved.
//

import Foundation

struct VacationReturnDates {
    var departureDate: Box<Date?> = Box(nil)
    var returnDate: Box<Date?> = Box(nil)
}

class VacationReturnViewModel {
    
    // MARL: - Variable
    
    private let demandeService = VacationReturnService()
    
    var retrieveVacationsAPICallState: Box<ApiCallState> = Box(.inactive)
    var submitVacationReturnAPICallState: Box<ApiCallState> = Box(.inactive)
    
    var dates: VacationReturnDates = VacationReturnDates()
    
    var vacationReturn: VacationReturn? {
        didSet {
            dates.departureDate.value = self.vacationReturn?.departureDate.getDate()
            dates.returnDate.value = self.vacationReturn?.returnDate.getDate()
        }
    }
    
    var types: [String] {
        return self.vacationReturn?.vacationTypes.map({ (type) -> String in
            type.vacationName
        }) ?? []
    }
    
    var vacations: VacationResponse?
    
    var vacationsDescription: [String] {
        var results: [String] = []
        let vac = self.vacations?.year ?? ["": []]
        for (_, vacations) in vac {
            results.append(contentsOf: vacations.map {
                ((Localizable.lang == Language.english.rawValue ) ? $0.nameEn : $0.nameAr)
                    + " " + ($0.dateTo.getDate()?.formatTo(format: "dd/MM/yyyy") ?? "")})
        }
        return results
    }
    
    var selectedVacation: Vacation?
    var lateReturnReason: String = ""
    
    // MARK: - Helpers
    
    func loadVacationReturn() {
        let email = UserDefaults.standard.string(forKey: Constants.UserDefaultsKeys.email) ?? ""
        self.retrieveVacationsAPICallState.value = .running
        self.demandeService.loadVacations(email: email) { (response, error) in
            if response != nil && error == nil {
                self.vacations = response
                self.retrieveVacationsAPICallState.value = .success
            } else {
                self.retrieveVacationsAPICallState.value = .failed
            }
        }
 
    }
    
    func selectVacationAtIndex(index: Int) {
        var results: [Vacation] = []
        let vac = self.vacations?.year ?? ["": []]
        for (_, vacations) in vac {
            results.append(contentsOf: vacations)
        }
        self.selectedVacation = results[index]
    }
    
    func submitVacationReturn() {
        let vacationPayload = VacationReturnPayload(
            email: UserDefaults.standard.string(forKey: Constants.UserDefaultsKeys.email) ?? "",
            vacationID: "\(self.selectedVacation?.vacationID ?? 0)",
            returnDate: self.dates.returnDate.value?.formatTo(format: "dd/MM/yyyy") ?? "",
            lateReturnReason: lateReturnReason)
        self.submitVacationReturnAPICallState.value = .running
        self.demandeService.submitNewVacationsReturnn(payload: vacationPayload) { (error) in
            if error == nil {
                self.submitVacationReturnAPICallState.value = .success
            } else {
                self.submitVacationReturnAPICallState.value = .failed
            }
        }
    }
}
