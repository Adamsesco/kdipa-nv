//
//  PermissionsService.swift
//  Kdipa
//
//  Created by Safoine Moncef Amine on 2/23/20.
//  Copyright © 2020 Safoine Moncef Amine. All rights reserved.
//

import Foundation
import Alamofire

class PermissionsService {
    func postPermission(permission: Permission, completion: @escaping (Error?) -> Void) {
        let parameters = permission.dictionary as? [String: String] ?? [:]
        Alamofire.upload(
            multipartFormData: { multipartFormData in
                for (key, value) in parameters {
                    multipartFormData.append(value.data(using: .utf8)!, withName: key)
                }
                
        },
            to: Constants.kdipaEmployeeBaseURL + "/api/permission") { (encodingResult) in
                switch encodingResult {
                case .success(let upload, _, _):
                    upload.responseJSON { _ in
                        completion(nil)
                    }
                case .failure(let encodingError):
                    completion(encodingError)
                }
        }
    }
}
