//
//  DashboardViewController.swift
//  Kdipa
//
//  Created by Safoine Moncef Amine on 2/9/20.
//  Copyright © 2020 Safoine Moncef Amine. All rights reserved.
//

import UIKit
import DropDown

class DashboardViewController: BaseViewController {
    
    @IBOutlet weak var lblFullName: KdipaLabel!
    @IBOutlet weak var permissionsTakenItem: SampleItem!
    @IBOutlet weak var permissionRemainingItem: SampleItem!
    @IBOutlet weak var attendedDaysItem: ImageItem!
    @IBOutlet weak var vacationItem: ImageItem!
    @IBOutlet weak var abscenceItem: ImageItem!
    
    @IBOutlet weak var fingerPrintItem: UIImageView!
    @IBOutlet weak var lblLastPinchIn: UILabel!
    @IBOutlet weak var lblPinchOut: UILabel!
    @IBOutlet weak var btnNewPermetion: UIButton!
    @IBOutlet weak var dropDownContainer: RoundedView!
        
    @IBOutlet weak var lbScreenTitle: KdipaLabel!
    @IBOutlet weak var lblLastPunchIn: UILabel!
    @IBOutlet weak var lblLastPinchOut: UILabel!
    
    var viewModel: DashboardViewModel = DashboardViewModel()
    var coordinator: DashboardCoordinator?
    let employeesDropDown = DropDown()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.configure()
        self.viewModel.getDashboard()
    }
    
    func configure() {
        configureContent()
        setupUI()
        setupNavigationBarItems()
        configureDropBox()
        configureGestures()
        self.viewModel.apiCallState.bind { (state) in
            switch state {
            case .running:
                self.showSpinner(uiView: self.navigationController?.view ?? self.view)
            case .failed:
                self.hideSpinner()
                self.showConfirmationMessage(view: self.view, message: "Something went wrong from our side")
            default:
                self.hideSpinner()
            }
        }
        localize()
    }
    
    func localize() {
        self.lbScreenTitle.text = Localizable.Dashboard.title
        self.lblLastPinchOut.text = Localizable.Dashboard.lastPinchOut
        self.lblLastPunchIn.text = Localizable.Dashboard.lastPinchIn
        self.btnNewPermetion.setTitle(Localizable.Dashboard.createNewPermission, for: .normal)
    }
    
    func configureDropBox() {
        guard let employees = SessionHandler.shared.connectedEmployee?.employees else {
            return
        }
        let array = employees.map { $0.employeeName }
        employeesDropDown.dataSource = array
        employeesDropDown.anchorView = self.dropDownContainer
        employeesDropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            self.viewModel.selectedEmployee = SessionHandler.shared.connectedEmployee?.employees?.first(where: { (employee) -> Bool in
                employee.employeeName == item
            })
            self.viewModel.getDashboard()
        }
        let showDropDownTapGesture = UITapGestureRecognizer(target: self, action: #selector(self.showEmployeeDropDown))
        self.dropDownContainer.addGestureRecognizer(showDropDownTapGesture)
    }
    
    @objc func showEmployeeDropDown() {
        self.employeesDropDown.show()
    }
    
    func configureGestures() {
        let tapVacation = UITapGestureRecognizer(target: self, action: #selector(self.showVacations))
        self.vacationItem.addGestureRecognizer(tapVacation)
        
        let tapPermissionTaken = UITapGestureRecognizer(target: self, action: #selector(self.showPermissions))
        self.permissionsTakenItem.addGestureRecognizer(tapPermissionTaken)
        
        let tapPermissionRemaining = UITapGestureRecognizer(target: self, action: #selector(self.showPermissions))
        self.permissionRemainingItem.addGestureRecognizer(tapPermissionRemaining)
        
        let tapAbsences = UITapGestureRecognizer(target: self, action: #selector(self.showAbcence))
        self.abscenceItem.addGestureRecognizer(tapAbsences)
        
        let tapAttendedDays = UITapGestureRecognizer(target: self, action: #selector(self.showAttendedDays))
        self.attendedDaysItem.addGestureRecognizer(tapAttendedDays)
        
        let showPinchInScreenTapGesture = UITapGestureRecognizer(target: self, action: #selector(self.showPinchInScreen))
        self.fingerPrintItem.addGestureRecognizer(showPinchInScreenTapGesture)
    }
    
    func configureContent() {
        self.viewModel.dashboard.bind { (dashboard) in
            guard let loadedDashboard  = dashboard else {
                return
            }
            if let punshInDate = loadedDashboard.lastPunchIn.getDate() {
                if Calendar.current.isDateInToday(punshInDate) {
                    Constants.isPunchInLast = true
                } else {
                    Constants.isPunchInLast = false
                }
            }
            self.lblFullName.text = loadedDashboard.nameAr
            
            self.abscenceItem.configure(type: .absence, dashBoard: loadedDashboard)
            self.vacationItem.configure(type: .vacation, dashBoard: loadedDashboard)
            self.attendedDaysItem.configure(type: .attendeddays, dashBoard: loadedDashboard)
            
            let dateFormatterGet = DateFormatter()
            dateFormatterGet.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
            
            let dateFormatterPrint = DateFormatter()
            dateFormatterPrint.dateFormat = "EEE dd MMMMM hh:mm aaa"
            
            if let lastPinchInDate = loadedDashboard.lastPunchIn.getDate() {
                self.lblLastPinchIn.text = dateFormatterPrint.string(from: lastPinchInDate)
            } else {
                self.lblLastPinchIn.text = "no data available"
            }
            if let lastPinchOutDate = loadedDashboard.lastPunchOut.getDate() {
                self.lblPinchOut.text = dateFormatterPrint.string(from: lastPinchOutDate)
            } else {
                self.lblPinchOut.text = "no data available"
            }
            self.permissionsTakenItem.configure(type: .permissionTaken, dashboard: loadedDashboard)
            self.permissionRemainingItem.configure(type: .permisionRemaining, dashboard: loadedDashboard)
        }
    }
    
    func setupUI() {
        let gradientLayer = CAGradientLayer()
        gradientLayer.frame = btnNewPermetion.bounds
        gradientLayer.colors = [UIColor.kdipaDarkBlue.cgColor, UIColor.kdipaLightBlue.cgColor]
        btnNewPermetion.layer.insertSublayer(gradientLayer, at: 0)
    }

    @IBAction func dismiss(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btnNewPermissionClicked(_ sender: Any) {
        self.coordinator?.newPermission()
    }
}

extension DashboardViewController {
    
    @objc func showVacations() {
        (self.coordinator?.parentCoordinator)?.openVactions()
    }
    
    @objc func showPermissions() {
        (self.coordinator?.parentCoordinator)?.openPermission()
    }
    
    @objc func showAbcence() {
        (self.coordinator?.parentCoordinator)?.openAbsence()
    }
    
    @objc func showAttendedDays() {
        (self.coordinator?.parentCoordinator)?.openAttendedDays()
    }
    
    @objc func showPinchInScreen() {
        self.coordinator?.parentCoordinator.geolocation()
    }
}
