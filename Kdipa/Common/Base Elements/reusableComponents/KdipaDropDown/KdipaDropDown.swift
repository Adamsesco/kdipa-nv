//
//  KdipaDropDown.swift
//  Kdipa
//
//  Created by Safoine Moncef Amine on 3/10/20.
//  Copyright © 2020 Safoine Moncef Amine. All rights reserved.
//

import UIKit
import DropDown

protocol KdipaDropDownDelegate: class {
    func didSelectItem(sender: KdipaDropDown,index: Int, newValue: String, oldValue: String)
}

class KdipaDropDown: UIView {
    
    @IBOutlet var contentView: UIView!
    @IBOutlet weak var lblContent: KdipaLabel!
    
    let dropDown = DropDown()
    var delegate: KdipaDropDownDelegate?
    
    // MARK: - inits
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        sharedInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        sharedInit()
    }
    
    func sharedInit() {
        let nib = UINib(nibName: "KdipaDropDown", bundle: nil)
        nib.instantiate(withOwner: self, options: nil)
        contentView.frame = bounds
        addSubview(contentView)
        self.configureDropDown()
    }
    
    // MARK: - Helpers
    
    private func configureDropDown() {
        let showDropDownTapGesture = UITapGestureRecognizer(target: self, action: #selector(self.showDropDown))
        self.addGestureRecognizer(showDropDownTapGesture)
        dropDown.anchorView = self
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            self.delegate?.didSelectItem(sender: self, index: index, newValue: item, oldValue: self.lblContent.text ?? "")
            self.fill(description: item)
        }
    }
    
    func fill(description: String) {
        self.lblContent.text  = description
    }
    
    func reLoad(array: [String]) {
        dropDown.dataSource = array
        self.fill(description: array[exist: 0] ?? "")
    }
    
    @objc func showDropDown() {
        self.dropDown.show()
    }
}
