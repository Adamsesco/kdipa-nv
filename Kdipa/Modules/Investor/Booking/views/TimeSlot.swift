//
//  TimeSlot.swift
//  Kdipa
//
//  Created by Safoine Moncef Amine on 3/28/20.
//  Copyright © 2020 Safoine Moncef Amine. All rights reserved.
//

import UIKit

enum SlotState: Int {
    case selected = 1
    case unselected = 2
    case disabled = 3
}

class TimeSlot: UIView {

    @IBOutlet var superView: UIView!
    @IBOutlet weak var contentView: RoundedView!
    @IBOutlet weak var lblTime: UILabel!
    
    var state: SlotState = .unselected
    @IBInspectable var text: String?
    @IBInspectable var selectedState: Int = 2
    
    // MARK: - inits
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        sharedInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        sharedInit()
    }
    
    func sharedInit() {
        let nib = UINib(nibName: "TimeSlot", bundle: nil)
        nib.instantiate(withOwner: self, options: nil)
        superView.frame = bounds
        addSubview(superView)
        self.updateState(newState: SlotState(rawValue: selectedState) ?? SlotState.unselected)
    }
    
    func configure() {
        self.lblTime.text = self.text
    }
    // MARK: - Helpers
    
    func updateState(newState: SlotState) {
        switch newState {
        case .selected:
            self.contentView.backgroundColor = UIColor.kdipaDarkBlue
            self.lblTime.textColor = UIColor.kdipaLightGray
        case .unselected:
             self.contentView.backgroundColor = UIColor.kdipaLightGray
             self.lblTime.textColor = UIColor.kdipaDarkBlue
        case .disabled:
            self.contentView.backgroundColor = UIColor.lightGray
            self.lblTime.textColor = UIColor.black
        }
    }
    
}
