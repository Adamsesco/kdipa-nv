//
//  PermissionViewModel.swift
//  Kdipa
//
//  Created by Safoine Moncef Amine on 3/3/20.
//  Copyright © 2020 Safoine Moncef Amine. All rights reserved.
//

import Foundation

class PermissionViewModel {
    
    var userFullName: String? {
        return self.selectedEmployee?.employeeName
    }
    
    var permissionsTakenPercentage: String {
        let taken = self.loadedPermissionHistory.value?.permissionsTaken ?? 0
        let remainingValue = self.loadedPermissionHistory.value?.permissionsRemained ?? 0
        let pourcentage = (Double(taken) / Double(taken + remainingValue)) * 100
        return String(format: "%.f", round(pourcentage))
    }
    
    var permissionsRemainingPercentage: String {
        let taken = self.loadedPermissionHistory.value?.permissionsTaken ?? 0
        let remainingValue = self.loadedPermissionHistory.value?.permissionsRemained ?? 0
        let pourcentage = (Double(remainingValue) / Double(taken + remainingValue)) * 100
        return String(format: "%.f", round(pourcentage))
    }
    
    var selectedEmployee: Employee? = SessionHandler.shared.connectedEmployee?.employees?[exist: 0] ??
    Employee(
        dict: ["Email": UserDefaults.standard.string(forKey: Constants.UserDefaultsKeys.email) ?? "",
           "EmployeeName": SessionHandler.shared.connectedEmployee?.firstName ?? ""])
    var selectedyear: String = "2019"
    
    var loadedPermissionHistory: Box<PermissionHistoryResponse?> = Box(nil)
    var loadpermissionsAPICallState: Box<ApiCallState> = Box(.inactive)
    let service = DemandesService()
    
    func loadPermissions() {
        self.loadpermissionsAPICallState.value = .running
        service.loadPermissions(email: self.selectedEmployee?.email ?? "") { (permissionResponse, error) in
            if error == nil {
                self.loadpermissionsAPICallState.value = .success
                self.loadedPermissionHistory.value = permissionResponse
            } else {
                self.loadpermissionsAPICallState.value = .failed
            }
        }
    }
}
