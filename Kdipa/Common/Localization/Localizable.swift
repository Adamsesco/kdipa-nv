//
//  Localizable.swift
//  Kdipa
//
//  Created by Safoine Moncef Amine on 2/9/20.
//  Copyright © 2020 Safoine Moncef Amine. All rights reserved.
//

import Foundation

struct Localizable {
    private init() {
        
    }
}

extension Localizable {
    
    static var lang: String { return "global.language".localized }
    static var arabic: String { return "global.language.ar".localized }
    static var english: String { return "global.language.en".localized }
    // MARK: - Login
    
    struct Login {
        static var title: String { return "login.title".localized }
        static var subtitle: String { return "login.subtitle".localized }
        static var forgetPassword: String { return "login.forgetPassword".localized }
        static var login: String { return "login.login".localized }
        static var applyUserName: String { return "login.applyUserName".localized }
    }
    
    struct EmployeeSetting {
        static var title: String { return "EmployeeSetting.title".localized }
        static var dropDownDesc: String { return "EmployeeSetting.dropDownDesc".localized }
        static var btnSave: String { return "EmployeeSetting.btnSave".localized }
    }
    
    // MARK: - KDipa Text Field
    
    struct KdipaTextField {
        static var username: String { return "kdipaTextField.username".localized }
        static var password: String { return "kdipaTextField.password".localized }
        static var email: String { return "kdipaTextField.email".localized }
        static var firstName: String { return "kdipaTextField.firstName".localized }
        static var lastName: String { return "kdipaTextField.lastName".localized }
        static var company: String { return "kdipaTextField.company".localized }
        static var phoneNumber: String { return "kdipaTextField.phoneNumber".localized }
    }
    
    // MARK: - Apply For UserName
    
    struct ApplyForUsername {
        static var title: String { return "applyForUsername.title".localized }
        static var subtitle: String { return "applyForUsername.subtitle".localized }
        static var create: String { return "applyForUsername.create".localized }
    }
    
    // MARK: - Dashboard
    
    struct Dashboard {
        static var title: String { return "dashboard.title".localized }
        static var lastPinchIn: String { return "dashboard.lastPinchIn".localized }
        static var lastPinchOut: String { return "dashboard.lastPinchOut".localized }
        static var permissionsTaken: String { return "dashboard.permissionsTaken".localized }
        static var permissionsRemaining: String { return "dashboard.permissionsRemaining".localized }
        static var vacations: String { return "dashboard.vacations".localized }
        static var attendedDays: String { return "dashboard.attendedDays".localized }
        static var absences: String { return "dashboard.absences".localized }
        static var createNewPermission: String { return "dashboard.createNewPermission".localized }
    }
    
    // MARK: - Permissions
    
    struct Permissions {
        static var title: String { return "permissions.createNewPermission".localized }
        static var totalPermissions: String { return "permissions.totalPermissions".localized }
        static var createPermission: String { return "permissions.createPermission".localized }
    }
    
    // MARK: - Vacations
    
    struct Vacations {
        static var title: String { return "Vacations.title".localized }
        static var newVacation: String { return "Vacations.newVacation".localized }
    }
    
    // MARK: - Absence
    
    struct Absence {
        static var title: String { return "Absence.title".localized }
    }
    
    // MARK: - Vacation Amendment
    
    struct VacationAmendment {
        static var title: String { return "VacationAmendment.title".localized }
        static var startFrom: String { return "VacationAmendment.startFrom".localized }
        static var to: String { return "VacationAmendment.to".localized }
        static var amdFrom: String { return "VacationAmendment.amdFrom".localized }
        static var amdTo: String { return "VacationAmendment.amdTo".localized }
        static var willBeAmended: String { return "VacationAmendment.willBeAmended".localized }
        static var delegatedEmployee: String { return "VacationAmendment.delegatedEmployee".localized }
        static var submit: String { return "VacationAmendment.submit".localized }
    }
    
    // MARK: - Vacation Amendment
    
    struct VacationExtension {
        static var title: String { return "VacationExtension.title".localized }
        static var startFrom: String { return "VacationExtension.startFrom".localized }
        static var to: String { return "VacationExtension.to".localized }
        static var extFrom: String { return "VacationExtension.extFrom".localized }
        static var extTo: String { return "VacationExtension.extTo".localized }
        static var willBeExtended: String { return "VacationExtension.willBeAmended".localized }
        static var delegatedEmployee: String { return "VacationExtension.delegatedEmployee".localized }
        static var submit: String { return "VacationExtension.submit".localized }
    }
    
    // MARK: - Vacation Return
    
    struct VacationReturn {
        static var title: String { return "VacationReturn.title".localized }
        static var returnDate: String { return "VacationReturn.returnDate".localized }
        static var didNotDelay: String { return "VacationReturn.didNotDelay".localized }
        static var wasLate: String { return "VacationReturn.wasLate".localized }
        static var forFollowingReason: String { return "VacationReturn.forFollowingReason".localized }
        static var submit: String { return "VacationReturn.submit".localized }
    }
    
    // MARK: - Official Duty
    
    struct OfficialDuty {
        static var title: String { return "OfficialDuty.title".localized }
        static var from: String { return "OfficialDuty.from".localized }
        static var to: String { return "OfficialDuty.to".localized }
        static var employee: String { return "OfficialDuty.employee".localized }
        static var delegatedEmployee: String { return "OfficialDuty.delegatedEmployee".localized }
        static var addmoreDelegates: String { return "OfficialDuty.addmoreDelegates".localized }
        static var submit: String { return "OfficialDuty.submit".localized }
    }
    
    // MARK: News
    
    struct News {
        static var title: String { return "News.title".localized }
    }
    
    // MARK: - New Vacation
    
    struct NewVacation {
        static var title: String { return "NewVacation.title".localized }
        static var departureDate: String { return "NewVacation.departureDate".localized }
        static var returnDate: String { return "NewVacation.returnDate".localized }
        static var delegatedEmployee: String { return "NewVacation.delegatedEmployee".localized }
        static var submit: String { return "NewVacation.submit".localized }
    }
    
    // MARK: - Days of attendance
    
    struct DaysOfAttendance {
        static var title: String { return "DaysOfAttendance.title".localized }
        static var attendance: String { return "DaysOfAttendance.attendance".localized }
        static var vacations: String { return "DaysOfAttendance.vacations".localized }
        static var absences: String { return "DaysOfAttendance.absences".localized }
        static var dayesOfpresence: String { return "DaysOfAttendance.dayesOfpresence".localized }
        static var days: String { return "DaysOfAttendance.days".localized }
    }
    
    // MARK: - New Permission
    
    struct NewPermission {
        static var title: String { return "NewPermission.title".localized }
        static var dutyStart: String { return "NewPermission.dutyStart".localized }
        static var dutyEnd: String { return "NewPermission.dutyEnd".localized }
        static var duringDuty: String { return "NewPermission.duringDuty".localized }
        static var submit: String { return "NewPermission.submit".localized }
    }
    // MARK: - Authentication
    
    struct LoadingView {
        
        // MARK: - Private Init
        
        private init() {}
        
        // MARK: Common
        
        static var loadingTitle: String { return "loader.loadingTitle".localized }
        static var successTitle: String { return "loader.successTitle".localized }
        static var loginTitle: String { return "loader.loginTitle".localized }
    }
    
    struct SideMenu {
        static var homeTitle: String { return "sideMenu.home".localized }
        static var transactionTitle: String { return "sideMenu.transaction".localized }
        static var geolocationTitle: String { return "sideMenu.geolocation".localized }
        static var newsTitle: String { return "sideMenu.news".localized }
        static var requestsTitle: String { return "sideMenu.newrequest".localized }
        static var notificationsTitle: String { return "sideMenu.notifications".localized }
        static var settingTitle: String { return "sideMenu.setting".localized }
        static var logoutTitle: String { return "sideMenu.logout".localized }
        
        static var investorDashboard: String { return "sideMenu.investorDashboard".localized }
        static var investorBooking: String { return "sideMenu.investorBooking".localized }
        static var investorHowItWorks: String { return "sideMenu.investorHowItWorks".localized }
        static var investorSetting: String { return "sideMenu.investorSetting".localized }
    }
    
    struct RequestSideMenuTitles {
        
        static var permission: String { return "requestsSideMenu.permission".localized }
        static var vacation: String {
            return "requestsSideMenu.vacation".localized
        }
        static var vacationAmendment: String { return "requestsSideMenu.vacationAmendment".localized }
        static var vacationExtension: String { return "requestsSideMenu.vacationExtension".localized }
        static var vacationReturn: String { return "requestsSideMenu.vacationReturn".localized }
        static var emergencyLeave: String { return "requestsSideMenu.emergencyLeave".localized }
        static var sickLeave: String { return "requestsSideMenu.sickLeave".localized }
        static var officialDuty: String { return "requestsSideMenu.officialDuty".localized }
        static var requestStatus: String { return "requestsSideMenu.requestStatus".localized }
        static var absence: String { return "requestsSideMenu.absence".localized }
    }
    
    struct PermissionHistory {
        static var Title: String {return "permissionHistory.Permissions".localized}
        static var permissionsRemaining: String {return "permissionHistory.permissionsRemaining ".localized}
        static var permissionsTaken: String {return "permissionHistory.permissionsTaken".localized}
    }
    
    struct PunchIn {
        static var punchInTitle: String { return "punchIn.label.title".localized }
        static var punchInButtonTitle: String { return "punchIn.button.PunchIn.title".localized }
        static var errorTitle: String { return "punchIn.Alert.error.title".localized }
        static var okButton: String { return "punchIn.button.Ok.title".localized }
        static var settingButton: String { return "punchIn.button.Setting.title".localized }
        static var punchInNotificationSuccess: String { return "punchIn.notificationAlert.sucess.message".localized }
        static var punchOutNotificationSuccess: String { return "punchOut.notificationAlert.sucess.message".localized }
        static var notificationFailureAlertMessage: String { return "punchIn.notificationAlert.failure.message".localized }
        static var notificationsuccessAlertTitle: String { return "punchIn.notificationAlert.sucess.title".localized }
    }
    
    struct PermissionCell {
        static var type: String {
            return "permissionCell.type".localized
        }
        static var from: String {
            return "permissionCell.from".localized
        }
        static var toDate: String {
            return "permissionCell.to".localized
        }
        static var duration: String {
            return "permissionCell.duration".localized
        }
    }
    
    // MARK: - Forget Password
    
    struct ForgetPassword {
        static var error: String {
            return "forgetpassword.error"
        }
        static var updateSuccess: String {
            return "forgetpassword.updateSuccess"
        }
        static var submit: String {
            return "forgetpassword.submit"
        }
        static var title: String {
            return "forgetpassword.title"
        }
        
        static var subTitle: String {
            return "forgetpassword.subTitle"
        }
    }
}
