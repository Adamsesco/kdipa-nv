//
//  KdipaTextField.swift
//  Kdipa
//
//  Created by Safoine Moncef Amine on 2/7/20.
//  Copyright © 2020 Safoine Moncef Amine. All rights reserved.
//

import UIKit

enum KdipaTextFieldType {
    case username
    case password
    case email
    case standard
}

class KdipaTextField: UIView {
    
    // MARK: Constants
    
    struct Constant {
        static let userNameIconName = "username"
        static let passwordIconName = "password"
        static let emailIconName = "email"
    }
    
    // MARK: - Outlets
    
    @IBOutlet var contentView: UIView!
    @IBOutlet weak var imgIcon: UIImageView!
    @IBOutlet weak var vwSeperatorLine: UIView!
    @IBOutlet weak var textfield: UITextField!
    
    // MARK: - properties
    
    @IBInspectable
    var placeholder: String? {
        set {
            self.textfield.placeholder = newValue
        }
        get {
            return self.textfield.placeholder
        }
    }
    
    var type: KdipaTextFieldType = .standard {
        didSet {
            configureTextField()
        }
    }
    
    var text: String? {
        set {
            self.textfield.text = newValue
        }
        get {
            return self.textfield?.text
        }
    }
    
    // MARK: - inits
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        sharedInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        sharedInit()
    }
    
    func sharedInit() {
        let nib = UINib(nibName: "KdipaTextField", bundle: nil)
        nib.instantiate(withOwner: self, options: nil)
        contentView.frame = bounds
        addSubview(contentView)
    }
    
    // MARK: - Configure
    
    fileprivate func configureTextField() {
        self.textfield.returnKeyType = .done
        switch type {
        case .username:
            self.imgIcon.image = UIImage(named: "username")
            self.textfield.placeholder = Localizable.KdipaTextField.username
        case .password:
            self.imgIcon.image = UIImage(named: "password")
            self.textfield.isSecureTextEntry = true
            self.textfield.placeholder = Localizable.KdipaTextField.password
        case .email:
            self.imgIcon.image = UIImage(named: "email")
            self.textfield.placeholder = Localizable.KdipaTextField.email
        default:
            self.imgIcon.isHidden = true
            self.vwSeperatorLine.isHidden = true
        }
        
        self.textfield.font = UIFont.frutigerRegular(size: 15)
    }
}
