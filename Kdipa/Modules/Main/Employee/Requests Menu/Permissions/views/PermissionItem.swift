//
//  PermissionItem.swift
//  Kdipa
//
//  Created by Safoine Moncef Amine on 3/3/20.
//  Copyright © 2020 Safoine Moncef Amine. All rights reserved.
//

import UIKit

enum PermissionItemType {
    case remaining
    case taken
}

class PermissionItem: UIView {
    
    // MARK: - Outlets
    
    @IBOutlet var contentView: UIView!
    @IBOutlet weak var backgroundVw: UIView!
    @IBOutlet weak var lbltitle: UILabel!
    @IBOutlet weak var lblValue: UILabel!
    
    // MARK: Variable
    
    var isSelected: Bool = false {
        didSet {
            if isSelected == false {
                self.backgroundVw.backgroundColor = UIColor.KdipaItemBackgroundGray
            } else {
                switch type {
                case .remaining:
                    self.backgroundVw.backgroundColor = isSelected ? UIColor.kdipaOrange : UIColor.KdipaItemBackgroundGray
                case .taken:
                    self.backgroundVw.backgroundColor = isSelected ? UIColor.kdipaDarkBlue : UIColor.KdipaItemBackgroundGray
                }
            }
            
        }
    }
    
    var type: PermissionItemType = .taken
    
    // MARK: - inits
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        sharedInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        sharedInit()
    }
    
    func sharedInit() {
        let nib = UINib(nibName: "PermissionItem", bundle: nil)
        nib.instantiate(withOwner: self, options: nil)
        contentView.frame = bounds
        addSubview(contentView)
    }
    
    // MARK: - Configure
    
    func configure(value: Int, type: PermissionItemType, isSelected: Bool) {
        self.lblValue.text = String(value)
        self.type = type
        self.isSelected = isSelected
        switch type {
        case .remaining:
            self.lbltitle.text = Localizable.PermissionHistory.permissionsRemaining
        case .taken:
            self.lbltitle.text = Localizable.PermissionHistory.permissionsTaken
        }
    }
}
