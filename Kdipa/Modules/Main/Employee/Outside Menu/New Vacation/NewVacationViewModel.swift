//
//  NewVacationViewModel.swift
//  Kdipa
//
//  Created by Safoine Moncef Amine on 3/8/20.
//  Copyright © 2020 Safoine Moncef Amine. All rights reserved.
//

import Foundation

class NewVacationViewModel {
    
    let service = NewVacationService()
    var loadNewVacationsCallState: Box<ApiCallState> = Box(.inactive)
    var submittNewVacationsCallState: Box<ApiCallState> = Box(.inactive)
    var selectedEmployee: Box<Employee?> = Box(nil)
    var selectedVacationType: Box<String> = Box("")
    var selectedFromDate: String = ""
    var selectedToDate: String = ""
    var isAdvancedPayment: Bool = true
    var newVacations: NewVacations? {
        didSet {
            self.selectedEmployee.value = self.newVacations?.delegatedEmployees[0]
            self.selectedVacationType.value = self.newVacations?.vacationTypes[0].vacationName ?? ""
            
        }
    }
    
    var delegates: [Employee] {
        return self.newVacations?.delegatedEmployees ?? []
    }
    
    func submitNewVacation() {
        self.submittNewVacationsCallState.value = .running
        let submittedVacation = SubmittedVacation(
            email: UserDefaults.standard.string(forKey: Constants.UserDefaultsKeys.email) ?? "",
            vacationType: self.selectedVacationType.value,
            fromDate: self.selectedFromDate,
            toDate: self.selectedToDate,
            delegatedEmployee: self.selectedEmployee.value?.employeeName ?? "", advancePayment: self.isAdvancedPayment)
        self.service.submitNewVacation(submittedVacation: submittedVacation) { (error) in
            if error == nil {
                self.submittNewVacationsCallState.value = .success
            } else {
                self.submittNewVacationsCallState.value = .failed
            }
        }
    }
    
    func loadNewVacations() {
        self.loadNewVacationsCallState.value = .running
        service.loadNewVacations(email: UserDefaults.standard.string(forKey: Constants.UserDefaultsKeys.email) ?? "") { (newVacations, error) in
            if error == nil {
                self.newVacations = newVacations
                self.loadNewVacationsCallState.value = .success
            } else {
                self.loadNewVacationsCallState.value = .failed
            }
        }
    }
}
