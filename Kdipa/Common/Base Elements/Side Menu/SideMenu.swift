//
//  SideMenu.swift
//  Kdipa
//
//  Created by Safoine Moncef Amine on 2/15/20.
//  Copyright © 2020 Safoine Moncef Amine. All rights reserved.
//

import UIKit

class SideMenu: UIView {
    
    // MARK: - Outlets 
    
    @IBOutlet weak var constraintRequestsTrailing: NSLayoutConstraint!
    @IBOutlet weak var vwBlurrBackGround: UIView!
    @IBOutlet var contentView: UIView!
    @IBOutlet weak var requestsSideMenu: RequestsSideMenu!
    @IBOutlet weak var firstItem: SideMenuItem!
    @IBOutlet weak var secondItem: SideMenuItem!
    @IBOutlet weak var thirdItem: SideMenuItem!
    @IBOutlet weak var fourthItem: SideMenuItem!
    @IBOutlet weak var fifth: SideMenuItem!
    @IBOutlet weak var sixth: SideMenuItem!
    @IBOutlet weak var seventh: SideMenuItem!
    @IBOutlet weak var logoutItem: SideMenuItem!
    @IBOutlet weak var itemsStack: UIStackView!
    
    @IBOutlet weak var lblComplainsAndConditions: UILabel!
    @IBOutlet weak var lblPrivacyPolicy: UILabel!
    @IBOutlet weak var lblTermesAndConditions: UILabel!
    
    // MARK: - Properties
    
    var coordinator: MenuCoordinator?
    static let shared: SideMenu = SideMenu()
    
    // MARK: - inits
    
    private override init(frame: CGRect) {
        super.init(frame: frame)
        sharedInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    private func sharedInit() {
        let nib = UINib(nibName: "SideMenu", bundle: nil)
        nib.instantiate(withOwner: self, options: nil)
        contentView.frame = bounds
        addSubview(contentView)
        if SessionHandler.shared.connectedEmployee != nil {
            configureSideItemsForEmployees()
        } else {
            configureSideItemsForInvestor()
        }
        
        configureItemsDelegate()
        self.requestsSideMenu.btnHideRequests.addTarget(self, action: #selector(self.hideRequestsMenu), for: .touchUpInside)
    }
    
    // MARK: Configure
    
    private func configureSideItemsForEmployees() {
        self.firstItem.type = .home
        self.secondItem.type = .transaction
        self.thirdItem.type = .geolocation
        self.fourthItem.type = .requests
        self.fifth.type = .notification
        self.sixth.type = .news
        self.seventh.type = .setting
        self.logoutItem.type = .logout
        
        self.fifth.isHidden = false
        self.sixth.isHidden = false
        self.seventh.isHidden = false
    }
    
    private func configureSideItemsForInvestor() {
        self.firstItem.type = .investorDashboard
        self.secondItem.type = .investorBooking
        self.thirdItem.type = .investorHowItWorks
        self.fourthItem.type = .investorSetting
        
        self.logoutItem.type = .logout
        
        self.fifth.isHidden = true
        self.sixth.isHidden = true
        self.seventh.isHidden = true
    }
    
    private func configureItemsDelegate() {
        for view in self.itemsStack.subviews {
            (view as? SideMenuItem)?.delegate = self
        }
        for view in self.requestsSideMenu.itemsStack.subviews {
            (view as? SideMenuItem)?.delegate = self
        }
        self.logoutItem.delegate = self
    }
    
    private func showRequestsMenu() {
        UIView.animate(withDuration: 0.3) {
            self.constraintRequestsTrailing.constant = self.requestsSideMenu.frame.width
            self.layoutIfNeeded()
        }
    }
    
    @objc private func hideRequestsMenu() {
        UIView.animate(withDuration: 0.3) {
            self.constraintRequestsTrailing.constant = 0
            self.layoutIfNeeded()
        }
    }
    
    func toggleRequestMenu() {
        if self.constraintRequestsTrailing.constant == 0 {
            self.showRequestsMenu()
        } else {
            self.hideRequestsMenu()
        }
    }
    func setasSelectedItem(item: SideMenuItem) {
        for view in self.itemsStack.subviews {
            (view as? SideMenuItem)?.isSelected = false
        }
        for view in self.requestsSideMenu.itemsStack.subviews {
            (view as? SideMenuItem)?.isSelected = false
        }
        item.isSelected = true
    }
}

extension SideMenu: SideMenuDelegate {
    func didSelectItem(sender: SideMenuItem) {
        self.setasSelectedItem(item: sender)
        switch sender.type {
        case .requests:
            self.toggleRequestMenu()
        case .home:
            self.coordinator?.dashboard()
        case .geolocation:
            self.coordinator?.geolocation()
        case .news:
            self.coordinator?.openNews()
        case .logout:
            self.coordinator?.finish()
        case .sample:
            self.handleRequestItemClick(requestItem: sender)
        case .transaction:
            break
        case .notification:
            break
        case .setting:
            self.coordinator?.employeeSetting()
        case .investorDashboard:
            self.coordinator?.openInvestorDasboard()
        case .investorBooking:
            self.coordinator?.openInvestorBooking()
        case .investorHowItWorks:
            self.coordinator?.openInvestorHowItWorks()
        case .investorSetting:
            self.coordinator?.openInvestorSetting()
        }
    }
    
    func handleRequestItemClick(requestItem: SideMenuItem) {
        print(requestItem.tag)
        switch requestItem.tag {
        case RequestsItemType.emergencyLeave.rawValue:
            self.coordinator?.openEmergencyLeave()
        case RequestsItemType.vacationAmendment.rawValue:
            self.coordinator?.openVacationAmendment()
        case RequestsItemType.vacationReturn.rawValue:
            self.coordinator?.openVacationReturn()
        case RequestsItemType.vacationExtention.rawValue:
            self.coordinator?.openVacationExtension()
        case RequestsItemType.vacations.rawValue:
            self.coordinator?.openVactions()
        case RequestsItemType.absences.rawValue:
            self.coordinator?.openAbsence()
        case RequestsItemType.permissions.rawValue:
            self.coordinator?.openPermission()
        case RequestsItemType.officialDuty.rawValue:
            self.coordinator?.openOfficialDuty()
        case RequestsItemType.requestStatus.rawValue:
            self.coordinator?.openRequestStatus()
        default:
            break
        }
    }
}
