//
//  RequestStatusResponse.swift
//  Kdipa
//
//  Created by Safoine Moncef Amine on 5/24/20.
//  Copyright © 2020 Safoine Moncef Amine. All rights reserved.
//

import Foundation

// MARK: - RequestStatusResponse
struct RequestStatusResponse: Codable {
    let name, nameAr: String
    var year: Year
}

// MARK: - Year
struct Year: Codable {
    var the2020: [The2020] // this shouldn't be like this but I was forced to do that because of the server response and we don't have a choice in front side

    enum CodingKeys: String, CodingKey {
        case the2020 = "2020"
    }
}

// MARK: - The2020
struct The2020: Codable {
    let dateFrom, dateTo, nameAr, nameEn: String
    let period, vacationID: Int

    enum CodingKeys: String, CodingKey {
        case dateFrom = "DateFrom"
        case dateTo = "DateTo"
        case nameAr = "Name-Ar"
        case nameEn = "Name-En"
        case period = "Period"
        case vacationID = "VacationId"
    }
}
