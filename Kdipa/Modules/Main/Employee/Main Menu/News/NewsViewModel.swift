//
//  NewsViewModel.swift
//  Kdipa
//
//  Created by Safoine Moncef Amine on 3/8/20.
//  Copyright © 2020 Safoine Moncef Amine. All rights reserved.
//

import Foundation

class NewsViewModel {
    let service: NewsService = NewsService()
    var news: [News] = []
    var loadNewsCallState: Box<ApiCallState> = Box(.inactive)
    
    func loadNews() {
        self.loadNewsCallState.value = .running
        service.loadNews { (response, error) in
            if error == nil {
                self.loadNewsCallState.value = .success
                self.news = response ?? []
            } else {
                self.loadNewsCallState.value = .failed
            }
        }
    }
}
