//
//  MenuCoordinator.swift
//  Kdipa
//
//  Created by Safoine Moncef Amine on 2/17/20.
//  Copyright © 2020 Safoine Moncef Amine. All rights reserved.
//

import UIKit

class MenuCoordinator: Coordinator {
    
    var childCoordinators: [Coordinator] = []
    var navigationController: UINavigationController
    var currentVC: BaseViewController?
    
    init(navigationController: UINavigationController) {
        self.navigationController = navigationController
    }
    
    func start() {
        BaseViewController.sideMenu.coordinator = self
        if SessionHandler.shared.connectedEmployee != nil {
            self.dashboard()
        } else { 
            self.openInvestorDasboard()
        }
    }
    
    func dashboard() {
        let dashboardCoordinator = DashboardCoordinator(navigationController: self.navigationController, parentCoordinator: self)
        dashboardCoordinator.start()
    }
    
    func newPermission() {
        let newPermissionVc = NewPermissionViewController.instantiate(from: .outSideMenu)
        self.currentVC = newPermissionVc
        self.navigationController.setViewControllers([newPermissionVc], animated: true)
    }
    
    func geolocation() {
        let punchInVC = PunchInViewController.instantiate(from: .mainMenu)
        punchInVC.coordinator = self
        self.currentVC = punchInVC
        navigationController.pushViewController(punchInVC, animated: true)
        punchInVC.navigationController?.navigationBar.isHidden = false
    }
    
    func openEmergencyLeave() {
        let emergencyLeaveVC = EmergencyLeaveViewController.instantiate(from: .mainMenu)
        self.currentVC = emergencyLeaveVC
        navigationController.pushViewController(emergencyLeaveVC, animated: true)
        emergencyLeaveVC.navigationController?.navigationBar.isHidden = false
    }
    
    func openVactions() {
        let vacationVC = VacationsViewController.instantiate(from: .requestsMenu)
        vacationVC.coordinator = self
        self.currentVC = vacationVC
        navigationController.pushViewController(vacationVC, animated: true)
        vacationVC.navigationController?.navigationBar.isHidden = false
    }
    
    func openAbsence() {
        let absenceVC = AbsenceViewController.instantiate(from: .requestsMenu)
        absenceVC.coordinator = self
        self.currentVC = absenceVC
        navigationController.pushViewController(absenceVC, animated: true)
        absenceVC.navigationController?.navigationBar.isHidden = false
    }
    
    func openPermission() {
        let permisionsVC = PermissionsViewController.instantiate(from: .requestsMenu)
        self.currentVC = permisionsVC
        permisionsVC.coordinator = self
        navigationController.pushViewController(permisionsVC, animated: true)
        permisionsVC.navigationController?.navigationBar.isHidden = false
    }
    
    func openAttendedDays() {
        let attendedDays = DaysOfAttendancyViewController.instantiate(from: .outSideMenu)
        self.currentVC = attendedDays
        attendedDays.coordinator = self
        navigationController.pushViewController(attendedDays, animated: true)
        attendedDays.navigationController?.navigationBar.isHidden = false
    }
    
    func openNewVacation() {
        let newVacationVC = NewVacationViewController.instantiate(from: .outSideMenu)
        self.currentVC = newVacationVC
        newVacationVC.coordinator = self
        navigationController.pushViewController(newVacationVC, animated: true)
        newVacationVC.navigationController?.navigationBar.isHidden = false
    }
    
    func openNews() {
        let newsVC = NewsViewController.instantiate(from: .mainMenu)
        self.currentVC = newsVC
        newsVC.coordinator = self
        navigationController.pushViewController(newsVC, animated: true)
        newsVC.navigationController?.navigationBar.isHidden = false
    }
    
    func openVacationAmendment() {
        let vacationAmendmentLeaveVC = VacationAmendmentViewController.instantiate(from: .requestsMenu)
        self.currentVC = vacationAmendmentLeaveVC
        vacationAmendmentLeaveVC.coordinator = self
        navigationController.pushViewController(vacationAmendmentLeaveVC, animated: true)
        vacationAmendmentLeaveVC.navigationController?.navigationBar.isHidden = false
    }
    
    func openVacationExtension() {
        let vacationExtensionLeaveVC = VacationExtensionViewController.instantiate(from: .requestsMenu)
        self.currentVC = vacationExtensionLeaveVC
        vacationExtensionLeaveVC.coordinator = self
        navigationController.pushViewController(vacationExtensionLeaveVC, animated: true)
        vacationExtensionLeaveVC.navigationController?.navigationBar.isHidden = false
    }
    
    func openVacationReturn() {
        let vacationReturnVC = VacationReturnViewController.instantiate(from: .requestsMenu)
        self.currentVC = vacationReturnVC
        vacationReturnVC.coordinator = self
        navigationController.pushViewController(vacationReturnVC, animated: true)
        vacationReturnVC.navigationController?.navigationBar.isHidden = false
    }
    
    func openOfficialDuty() {
        let officialDutyVC = OfficialDutyViewController.instantiate(from: .requestsMenu)
        self.currentVC = officialDutyVC
        officialDutyVC.coordinator = self
        navigationController.pushViewController(officialDutyVC, animated: true)
        officialDutyVC.navigationController?.navigationBar.isHidden = false
    }
    
    func employeeSetting() {
        let settingVc = EmployeeSettingViewController.instantiate(from: .mainMenu)
        self.currentVC = settingVc
        settingVc.coordinator = self
        navigationController.pushViewController(settingVc, animated: true)
        settingVc.navigationController?.navigationBar.isHidden = false
    }
    
    func openInvestorDasboard() {
        let investorDasboardVC = InvestorDashboardViewController.instantiate(from: .investor)
        self.currentVC = investorDasboardVC
        investorDasboardVC.coordinator = self
        navigationController.pushViewController(investorDasboardVC, animated: true)
        investorDasboardVC.navigationController?.navigationBar.isHidden = false
    }
    
    func openInvestorBooking() {
        let bookingVC = BookingViewController.instantiate(from: .investor)
        self.currentVC = bookingVC
        bookingVC.coordinator = self
        navigationController.pushViewController(bookingVC, animated: true)
        bookingVC.navigationController?.navigationBar.isHidden = false
    }
    func openInvestorHowItWorks() {
        let hiwVC = HowItWorksViewController.instantiate(from: .investor)
        self.currentVC = hiwVC
        hiwVC.coordinator = self
        navigationController.pushViewController(hiwVC, animated: true)
        hiwVC.navigationController?.navigationBar.isHidden = false
    }
    
    func openInvestorSetting() {
        let settingVC = SettingViewController.instantiate(from: .investor)
        self.currentVC = settingVC
        settingVC.coordinator = self
        navigationController.pushViewController(settingVC, animated: true)
        settingVC.navigationController?.navigationBar.isHidden = false
    }
    
    func openRequestStatus() {
        let requestStatusVC = RequestStatusViewController.instantiate(from: .requestsMenu)
        self.currentVC = requestStatusVC
        requestStatusVC.coordinator = self
        navigationController.pushViewController(requestStatusVC, animated: true)
        requestStatusVC.navigationController?.navigationBar.isHidden = false
    }
    
    func finish() {
        currentVC?.hideSideMenu()
        SessionHandler.shared.connectedEmployee = nil
        SessionHandler.shared.connectedInvestor = nil
        UserDefaults.standard.set("", forKey: Constants.UserDefaultsKeys.email)
        UserDefaults.standard.set("", forKey: Constants.UserDefaultsKeys.password)
        let mainCoordinator = MainCoordinator(navigationController: self.navigationController)
        mainCoordinator.start()
    }
}
