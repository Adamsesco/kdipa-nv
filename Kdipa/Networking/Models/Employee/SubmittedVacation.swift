//
//  SubmittedVacation.swift
//  Kdipa
//
//  Created by Safoine Moncef Amine on 3/8/20.
//  Copyright © 2020 Safoine Moncef Amine. All rights reserved.
//

import Foundation

struct SubmittedVacation: Codable {
    let email, vacationType, fromDate, toDate: String
    let delegatedEmployee: String
    let advancePayment: Bool
}
