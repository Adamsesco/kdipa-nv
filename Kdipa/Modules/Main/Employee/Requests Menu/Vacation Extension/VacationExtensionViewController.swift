//
//  VacationExtensionViewController.swift
//  Kdipa
//
//  Created by Said Elmansour on 2020-03-01.
//  Copyright © 2020 Safoine Moncef Amine. All rights reserved.
//

import UIKit
import FSCalendar
import DropDown

class VacationExtensionViewController: BaseViewController {
    
    @IBOutlet weak var calendarTrailingConstraint: NSLayoutConstraint!
    @IBOutlet weak var calendarCoàntainer: UIView!
    @IBOutlet weak var startDateLabel: UILabel!
    @IBOutlet weak var endDateLabel: UILabel!
    @IBOutlet weak var amandEndDateLabel: UILabel!
    @IBOutlet weak var calendar: FSCalendar!
    @IBOutlet weak var typesDropDownContainer: RoundedView!
    @IBOutlet weak var lblSelectedType: KdipaLabel!
    @IBOutlet weak var delegatesDropDownContainer: RoundedView!
    @IBOutlet weak var lblSelectedDelegate: KdipaLabel!
    
    
    @IBOutlet weak var lblScreenTitle: KdipaLabel!
    @IBOutlet weak var lblStartFrom: UILabel!
    @IBOutlet weak var lblTo: UILabel!
    @IBOutlet weak var lblExtTo: UILabel!
    @IBOutlet weak var lblWillBeExtendedTo: UILabel!
    @IBOutlet weak var lblDelegatedEmployee: UILabel!
    
    @IBOutlet weak var btnSubmit: UIButton!
    // MARK: - Properties
    
    var viewModel = VacationExtensionViewModel()
    var coordinator: MenuCoordinator?
    
    var btnType: BtnDatesType?
    
    let delegatesDropDown = DropDown()
    let typesDropDown = DropDown()
    
    // MARK: - VC life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupNavigationBarItems()
        self.bind()
        self.configure()
        self.localize()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.viewModel.getvacationExtension()
    }
    
    // MARK: - Configure
    
    func configure() {
        configureDropDowns()
    }
    func localize() {
        self.lblScreenTitle.text = Localizable.VacationExtension.title
        self.lblStartFrom.text = Localizable.VacationExtension.startFrom
        self.lblTo.text = Localizable.VacationExtension.to
        self.lblWillBeExtendedTo.text = Localizable.VacationExtension.willBeExtended
        self.lblExtTo.text = Localizable.VacationExtension.extTo
        self.lblDelegatedEmployee.text = Localizable.VacationExtension.delegatedEmployee
        self.btnSubmit.setTitle(Localizable.VacationExtension.submit, for: .normal)
        
    }
    
    private func bind() {
        // bind showing spinner with Api call
        guard let spinnerContainer = self.navigationController?.view ?? self.view else {
            return
        }
        
        self.viewModel.gettingVExtensionCallState.bind { (state) in
            switch state {
            case .running:
                self.showSpinner(uiView: spinnerContainer)
            case .success:
                self.hideSpinner()
                self.configure()
            case .failed:
                self.hideSpinner()
                self.showConfirmationMessage(view: self.view, message: "server error")
            default:
                self.hideSpinner()
            }
        }
        
        // bind submittting new Vacation amendement API call
         self.viewModel.submittingVacationExtensionCallState.bind { (state) in
             switch state {
             case .running:
                 self.showSpinner(uiView: spinnerContainer)
             case .success:
                 self.hideSpinner()
                 self.coordinator?.dashboard()
             case .failed:
                 self.hideSpinner()
                 self.showConfirmationMessage(view: self.view, message: "server error")
             default:
                 self.hideSpinner()
             }
         }
        
        // bind dates fields
        self.viewModel.vacationExtensionDates
            .oldFromDate.bind { (date) in
            self.startDateLabel.text = date?.formatTo(format: "EEE, MMM dd ")
        }
        self.viewModel.vacationExtensionDates.oldToDate.bind { (date) in
            self.endDateLabel.text = date?.formatTo(format: "EEE, MMM dd ")
        }
        self.viewModel.vacationExtensionDates.newToDate.bind { (date) in
            self.amandEndDateLabel.text = date?.formatTo(format: "EEE, MMM dd ")
        }
    }
    
    private func configureDropDowns() {
        
        // configure types drop down
        
        let types = self.viewModel.vacationType
        let namesarray = types.map { $0.vacationName }
        self.typesDropDown.dataSource = namesarray
        self.typesDropDown.anchorView = self.typesDropDownContainer
        self.typesDropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            self.lblSelectedType.text = item
            self.viewModel.selectedtype = item
        }
        let showDropDownTapGesture = UITapGestureRecognizer(target: self, action: #selector(self.showTypesDropDown))
        self.typesDropDownContainer.addGestureRecognizer(showDropDownTapGesture)
        
        // configure delegates drop down
        
        guard let delegates = SessionHandler.shared.connectedEmployee?.employees?.map({ (employee) -> String in
            return employee.employeeName
        }) else {
            return
        }
        delegatesDropDown.dataSource = delegates
        delegatesDropDown.anchorView = self.delegatesDropDownContainer
        delegatesDropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            self.lblSelectedDelegate.text = item
            self.viewModel.selectedDelegate = item
        }
        let showDelegatesDropDownTapGesture = UITapGestureRecognizer(target: self, action: #selector(self.showDelegatesDropDown))
        
        self.delegatesDropDownContainer.addGestureRecognizer(showDelegatesDropDownTapGesture)
    }
    
    @objc func showTypesDropDown() {
        self.typesDropDown.show()
    }
    
    @objc func showDelegatesDropDown() {
        self.delegatesDropDown.show()
    }
    
    // MARK: - Local helpers

    private func showCalendar() {
        calendarTrailingConstraint.constant = 0
        UIView.animate(withDuration: 0.5) {
            self.view.layoutIfNeeded()
        }
    }

    private func hideCalendar() {
        calendarTrailingConstraint.constant = -414
        UIView.animate(withDuration: 0.5) {
            self.view.layoutIfNeeded()
        }
    }
    
    // MARK: - Actions

    @IBAction func onStart(_ sender: Any) {
        showCalendar()
        btnType = .startDate
    }

    @IBAction func onAmendTo(_ sender: Any) {
        showCalendar()
        btnType = .amandEndDate

    }

    @IBAction func onTo(_ sender: Any) {
        showCalendar()
        btnType = .endDate
    }

    @IBAction func onSubmit(_ sender: Any) {
        self.viewModel.submitVacationAmendment()
    }

}

// MARK: - Calender delegate

extension VacationExtensionViewController: FSCalendarDelegate {
    func calendar(_ calendar: FSCalendar, didSelect date: Date, at monthPosition: FSCalendarMonthPosition) {
        hideCalendar()
        switch btnType {
        case .startDate:
            self.viewModel.vacationExtensionDates.oldFromDate.value = date
        case .endDate:
            self.viewModel.vacationExtensionDates.oldToDate.value = date
        case .amandEndDate:
            self.viewModel.vacationExtensionDates.newToDate.value = date
        default:
            break
        }
    }
}
