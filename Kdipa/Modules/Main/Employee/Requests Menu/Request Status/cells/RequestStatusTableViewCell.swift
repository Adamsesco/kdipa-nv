//
//  RequestStatusTableViewCell.swift
//  Kdipa
//
//  Created by Safoine Moncef Amine on 5/24/20.
//  Copyright © 2020 Safoine Moncef Amine. All rights reserved.
//

import UIKit

protocol RequestsDelegate {
    func cancelRequest(vacationID: String)
}

class RequestStatusTableViewCell: UITableViewCell {
    @IBOutlet weak var lblReqIDTitle: UILabel!
    @IBOutlet weak var lblReqIDValue: UILabel!
    @IBOutlet weak var lblReqTypeTitle: UILabel!
    @IBOutlet weak var lblReqTypeValue: UILabel!
    @IBOutlet weak var lblReqState: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    
    private var request: The2020?
    
    static let reuseIdentifier = "RequestStatusTableViewCell"
    var delegate: RequestsDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func configure(request: The2020) {
        self.request = request
        self.lblReqIDValue.text = String(request.vacationID)
        self.lblReqTypeValue.text = request.nameEn
        self.lblDate.text = request.dateFrom.getDate()?.formatTo(format: "E,MMM d h:mma")
    }
    
    @IBAction func cancelReqeust(_ sender: Any) {
        if let id = self.request?.vacationID {
            self.delegate?.cancelRequest(vacationID: String(id))
        }
    }
    
}
