//
//  PermissionCollectionViewCell.swift
//  Kdipa
//
//  Created by Safoine Moncef Amine on 3/4/20.
//  Copyright © 2020 Safoine Moncef Amine. All rights reserved.
//

import UIKit

class PermissionCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var lblPermissionDate: UILabel!
    @IBOutlet weak var lblTypeTitle: UILabel!
    @IBOutlet weak var lblFromTitle: UILabel!
    @IBOutlet weak var lblToTitle: UILabel!
    @IBOutlet weak var lblDurationTitle: UILabel!
    
    @IBOutlet weak var lblType: UILabel!
    @IBOutlet weak var lblFrom: UILabel!
    @IBOutlet weak var lblTo: UILabel!
    @IBOutlet weak var lblDuration: UILabel!
    
    static let reuseIdentifier = "PermissionCollectionViewCell"
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.lblTypeTitle.text = Localizable.PermissionCell.type
        self.lblFromTitle.text = Localizable.PermissionCell.from
        self.lblToTitle.text = Localizable.PermissionCell.toDate
        self.lblDurationTitle.text = Localizable.PermissionCell.duration
    }
    
    func configure(permission: PermissionResponse) {
        self.lblPermissionDate.text = permission.trxDate.formatDateToFormat(
            fromFormat: "yyyy-MM-dd'T'HH:mm:ss.SSSZ",
            toformat: "EEEE dd MMMM yyyy")
        self.lblType.text = permission.nameEn
        self.lblFrom.text = permission.timeFrom.formatDateToFormat(fromFormat: "HH:mm:ss", toformat: "hh:mm a")
        self.lblTo.text = permission.timeTo.formatDateToFormat(fromFormat: "HH:mm:ss", toformat: "hh:mm a")
        self.lblDuration.text = permission.period
    }
}
