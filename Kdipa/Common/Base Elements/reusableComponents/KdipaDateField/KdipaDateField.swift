//
//  KdipaDateField.swift
//  Kdipa
//
//  Created by Safoine Moncef Amine on 2/18/20.
//  Copyright © 2020 Safoine Moncef Amine. All rights reserved.
//

import UIKit
import MaterialComponents.MaterialRipple

class KdipaDateField: UIView {
    
    @IBOutlet var contentView: UIView!
    @IBOutlet weak var lblDate: UILabel!
    
    // MARK: - inits
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        sharedInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        sharedInit()
    }
    
    func sharedInit() {
        let nib = UINib(nibName: "KdipaDateField", bundle: nil)
        nib.instantiate(withOwner: self, options: nil)
        contentView.frame = bounds
        addSubview(contentView)
    }
    
    func fillTextField(_ text: String ) {
        self.lblDate.text = text
        self.updateStyle()
    }
    
    func updateStyle() {
        self.lblDate.font = UIFont.frutigerBold(size: 13)
        self.lblDate.textColor = UIColor.kdipaGrayBorder
    }
}
