//
//  PunchInService.swift
//  Kdipa
//
//  Created by Said Elmansour on 2020-02-29.
//  Copyright © 2020 Safoine Moncef Amine. All rights reserved.
//

import Foundation

import Alamofire

class PunchInService {
    
    private let punchInEndPoint = Constants.kdipaEmployeeBaseURL + "/api/geoAttend"
    private let punchInCheckEndPoint = Constants.kdipaEmployeeBaseURL + "/api/geoAttendCheck"
    
    func postPunchIn(punchIn: PunchIn, completion: @escaping ([String: String]?, Error?) -> Void) {
        let parameters = punchIn.dictionary as? [String: String] ?? [:]
          let headers = ["Content-Type": "application/json", "Accept": "application/json"]
          Alamofire.request(punchInEndPoint,
                            method: .post,
                            parameters: parameters,
                            encoding: JSONEncoding.default,
                            headers: headers).responseJSON { (response) in
              switch response.result {
              case .success(let value):
                  let dashboard = value as? [String: String]
                  if (dashboard["Time"] as? String)?.isEmpty ?? true {
                    completion(nil, NSError())
                  }
                  completion(dashboard, nil)
              case .failure(let error):
                  completion(nil, error)
              }
          }
    }
    
    func geoAttendCheck(punchIn: PunchIn, completion: @escaping ([String: String]?, Error?) -> Void) {
        let parameters = punchIn.dictionary as? [String: String] ?? [:]
        let headers = ["Content-Type": "application/json", "Accept": "application/json"]
        Alamofire.request(punchInCheckEndPoint,
                          method: .post,
                          parameters: parameters,
                          encoding: JSONEncoding.default,
                          headers: headers).responseJSON { (response) in
            switch response.result {
            case .success(let value):
                let dashboard = value as? [String: String]
                completion(dashboard, nil)
            case .failure(let error):
                completion(nil, error)
            }
        }
    }
}

