//
//  InvestorDashboardService.swift
//  Kdipa
//
//  Created by Safoine Moncef Amine on 3/31/20.
//  Copyright © 2020 Safoine Moncef Amine. All rights reserved.
//

import Foundation
import Alamofire

struct InvestorDashboardService {
    
    func loadInvestorDashboard(completion: @escaping (InvestorDashboard?, Error?) -> Void) {
        let url = URL(string: Constants.kdipaInvestorBaseURL + KdipaAPI.investorDashboard.path)!
        let headers = ["Content-Type": "application/json",
                       "API-KEY": "API-KEY-MOBILE-123",
                       "X-Auth-Token": SessionHandler.shared.connectedInvestor?.token ?? ""]
        Alamofire.request(url, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: headers).responseData { (response) in
            switch response.result {
            case .success(let value):
                do {
                let dashboard = try JSONDecoder().decode(InvestorDashboard.self, from: value)
                completion(dashboard, nil)
                } catch {
                completion(nil, error)
                }
                
            case .failure(let error):
                completion(nil, error)
            }
        }
    }
}
