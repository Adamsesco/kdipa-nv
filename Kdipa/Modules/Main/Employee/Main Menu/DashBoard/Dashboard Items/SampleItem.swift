//
//  SampleItem.swift
//  Kdipa
//
//  Created by Safoine Moncef Amine on 2/12/20.
//  Copyright © 2020 Safoine Moncef Amine. All rights reserved.
//

import UIKit
import MaterialComponents.MaterialProgressView

enum SampleItemType {
    case permissionTaken
    case permisionRemaining
}
class SampleItem: UIView {
    
    @IBOutlet var contentView: UIView!
    @IBOutlet weak var value: UILabel!
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var progressView: UIView!
    
    // MARK: - inits
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        sharedInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        sharedInit()
    }
    
    func sharedInit() {
        let nib = UINib(nibName: "SampleItem", bundle: nil)
        nib.instantiate(withOwner: self, options: nil)
        contentView.frame = bounds
        addSubview(contentView)
    }
    
    // MARK: - Configure
    func configure(value: Int, title: String, progress: Float) {
        self.value.text = "\(value)"
        self.title.text = title
        
        let progressView = MDCProgressView()
        progressView.progress = progress
        let progressViewHeight = CGFloat(4)
        progressView.frame = CGRect(x: 0,
                                    y: self.progressView.bounds.height - progressViewHeight,
                                    width: self.progressView.bounds.width,
                                    height: progressViewHeight)
        progressView.progressTintColor = UIColor.white
        progressView.setHidden(false, animated: true)
        self.progressView.addSubview(progressView)
    }
    
    func configure(type: SampleItemType, dashboard: DashboardResponse) {
        switch type {
        case .permisionRemaining:
            self.title.text = Localizable.Dashboard.permissionsRemaining
            self.value.text = dashboard.permissionsRemaining.isEmpty ? "00:00" : dashboard.permissionsRemaining
        case .permissionTaken:
            self.title.text = Localizable.Dashboard.permissionsTaken
            self.value.text = dashboard.permissionsTaken.isEmpty ? "00:00" : dashboard.permissionsTaken
        }
    }
}
