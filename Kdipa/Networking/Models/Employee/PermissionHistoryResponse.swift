//
//  PermissionResponse.swift
//  Kdipa
//
//  Created by Safoine Moncef Amine on 2/23/20.
//  Copyright © 2020 Safoine Moncef Amine. All rights reserved.
//

import Foundation

struct PermissionHistoryResponse: Codable {
    let name, nameAr: String
    let permissionsRemained, permissionsTaken: Int
    let permissionsHistory: [PermissionResponse]
}

// MARK: - PermissionResponse
struct PermissionResponse: Codable {
    let nameAr, nameEn, period, timeFrom: String
    let timeTo, trxDate: String

    enum CodingKeys: String, CodingKey {
        case nameAr = "Name-Ar"
        case nameEn = "Name-En"
        case period = "Period"
        case timeFrom = "TimeFrom"
        case timeTo = "TimeTo"
        case trxDate = "TrxDate"
    }
}
