//
//  DateItem.swift
//  Kdipa
//
//  Created by Safoine Moncef Amine on 3/8/20.
//  Copyright © 2020 Safoine Moncef Amine. All rights reserved.
//

import UIKit

protocol DateItemDelegate: class {
    func dateFieldCleared(sender: DateItem)
    func dateFieldFilled(sender: DateItem)
}

class DateItem: UIView {
    
    @IBOutlet var contentView: UIView!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var clearFieldVW: RoundedView!
    
    weak var delegate: DateItemDelegate?
    
    // MARK: - inits
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        sharedInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        sharedInit()
    }
    
    func sharedInit() {
        let nib = UINib(nibName: "DateItem", bundle: nil)
        nib.instantiate(withOwner: self, options: nil)
        contentView.frame = bounds
        addSubview(contentView)
        
        let clearDateGesture = UITapGestureRecognizer(target: self, action: #selector(self.clearDate))
        self.contentView.addGestureRecognizer(clearDateGesture)
        
    }
    
    @objc private func clearDate() {
        self.lblDate.text = ""
        self.delegate?.dateFieldCleared(sender: self)
    }
    
    func fillDateWith(date: Date) {
        self.lblDate.text = date.formatTo(format: "EEE,MMM dd")
        self.delegate?.dateFieldFilled(sender: self)
    }
    
}
