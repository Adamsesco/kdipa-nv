//
//  RequestStatusViewController.swift
//  Kdipa
//
//  Created by Safoine Moncef Amine on 5/24/20.
//  Copyright © 2020 Safoine Moncef Amine. All rights reserved.
//

import UIKit

class RequestStatusViewController: BaseViewController {
    
    var coordinator: MenuCoordinator?
    var viewModel: RequestStatusViewModel = RequestStatusViewModel()
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupNavigationBarItems()
        self.bind()
        self.viewModel.loadRequests()
    }
    
    private func bind() {
        self.tableView.register(UINib.nib(named: RequestStatusTableViewCell.reuseIdentifier),
                                forCellReuseIdentifier: RequestStatusTableViewCell.reuseIdentifier)
        self.tableView.dataSource = self
        self.viewModel.loadRequestsCallStatus.bind { (state) in
            switch state {
            case .running:
                self.showSpinner(uiView: self.coordinator?.navigationController.view ?? self.view)
            case .success:
                self.hideSpinner()
                self.tableView.reloadData()
            case .failed:
                self.hideSpinner()
                self.showConfirmationMessage(view: self.view, message: "something went wrong")
            default:
                self.hideSpinner()
            }
        }
        
        self.viewModel.cancelRequestCallState.bind { (state) in
            switch state {
            case .running:
                self.showSpinner(uiView: self.coordinator?.navigationController.view ?? self.view)
            case .success:
                self.hideSpinner()
                self.tableView.reloadData()
            case .failed:
                self.hideSpinner()
                self.showConfirmationMessage(view: self.view, message: "something went wrong")
            default:
                self.hideSpinner()
            }
        }
    }
}

extension RequestStatusViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let req = self.viewModel.requestsResponse?.year.the2020
        return req?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(
            withIdentifier: RequestStatusTableViewCell.reuseIdentifier,
            for: indexPath) as? RequestStatusTableViewCell else { return RequestStatusTableViewCell() }
        if let req = self.viewModel.requestsResponse?.year.the2020[indexPath.row] {
            cell.configure(request: req)
        }
        cell.delegate = self
        return cell
    }
}

extension RequestStatusViewController: RequestsDelegate {
    func cancelRequest(vacationID: String) {
        self.viewModel.cancelRequest(vacationID: vacationID)
    }
}
