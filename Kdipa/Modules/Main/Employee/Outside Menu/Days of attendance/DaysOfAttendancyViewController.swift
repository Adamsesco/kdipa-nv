//
//  DaysOfAttendancyViewController.swift
//  Kdipa
//
//  Created by Safoine Moncef Amine on 3/7/20.
//  Copyright © 2020 Safoine Moncef Amine. All rights reserved.
//

import UIKit
import DropDown
import Charts

class DaysOfAttendancyViewController: BaseViewController {

    var viewModel: DaysOfAttendanceViewModel = DaysOfAttendanceViewModel()
    
    @IBOutlet weak var lblChartPercentage: UILabel!
    @IBOutlet weak var employeeDropDownContainer: RoundedView!
    @IBOutlet weak var yearsDropDownContainer: RoundedView!
    @IBOutlet weak var currentName: KdipaLabel!
    @IBOutlet weak var currentYear: KdipaLabel!
    @IBOutlet weak var pieChart: PieChartView!
    
    @IBOutlet weak var attendanceItem: AttendanceDayItem!
    @IBOutlet weak var vacationItem: AttendanceDayItem!
    @IBOutlet weak var absenceItem: AttendanceDayItem!
    @IBOutlet weak var lblChartDescription: UILabel!
    
    @IBOutlet weak var lblScreenTitle: KdipaLabel!
    
    let employeesDropDown: DropDown = DropDown()
    let yearsDropDown: DropDown = DropDown()
    
    var coordinator: MenuCoordinator?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupNavigationBarItems()
        self.configure()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        viewModel.reloadData()
    }
    func configure() {
        self.bind()
        self.configureDropDowns()
        self.lblScreenTitle.text = Localizable.DaysOfAttendance.title
    }
    
    func reconfigureData() {
        self.currentName.text = self.viewModel.selectedEmployee?.employeeName
        self.currentYear.text = self.viewModel.selectedYear
        self.lblChartPercentage.text = "\(self.viewModel.attendancePercentage)%"
        self.lblChartDescription.text = "\(self.viewModel.selectedAttendedDay?.attendance ?? 0) " +
            Localizable.DaysOfAttendance.dayesOfpresence
            + "\(self.viewModel.selectedAttendedDay?.totalDays ?? 0) "
            + Localizable.DaysOfAttendance.days
        
        self.attendanceItem.configure(title: Localizable.DaysOfAttendance.attendance,
                                      value: self.viewModel.selectedAttendedDay?.attendance ?? 0,
                                      progress: self.viewModel.attendanceProgress)
        self.vacationItem.configure(title: Localizable.DaysOfAttendance.vacations,
                                    value: self.viewModel.selectedAttendedDay?.vacations ?? 0,
                                    progress: self.viewModel.vacationProgress)
        self.absenceItem.configure(title: Localizable.DaysOfAttendance.absences,
                                   value: self.viewModel.selectedAttendedDay?.absences ?? 0,
                                   progress: self.viewModel.absenceProgress)
        
        self.configureDropDowns()
        reConfigureChart()
    }
    
    func bind() {
        self.viewModel.loadDataState.bind { (state) in
            switch state {
            case .running:
                self.showSpinner(uiView: self.coordinator?.navigationController.view ?? self.view)
            case .success:
                self.hideSpinner()
                self.reconfigureData()
            case .failed:
                self.hideSpinner()
                self.showConfirmationMessage(view: self.view, message: "something went wrong")
            default:
                self.hideSpinner()
            }
        }
    }
    
    func  configureDropDowns() {
        // Configure Employees drop down
        guard let employees = SessionHandler.shared.connectedEmployee?.employees else {
            return
        }
        let array = employees.map { $0.employeeName }
        employeesDropDown.dataSource = array
        employeesDropDown.anchorView = self.employeeDropDownContainer
        employeesDropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            self.viewModel.selectedEmployee = SessionHandler.shared.connectedEmployee?.employees?.first(where: { (employee) -> Bool in
                employee.employeeName == item
            })
            self.viewModel.reloadData()
        }
        let showDropDownTapGesture = UITapGestureRecognizer(target: self, action: #selector(self.showEmployeeDropDown))
        self.employeeDropDownContainer.addGestureRecognizer(showDropDownTapGesture)
        
        // Configure years drop down
        
        yearsDropDown.dataSource = viewModel.years
        yearsDropDown.anchorView = self.yearsDropDownContainer
        yearsDropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            self.viewModel.selectedYear = self.viewModel.years.first(where: { (year) -> Bool in
                year == item
            }) ?? ""
            self.reconfigureData()
        }
        let showYearsDropDownTapGesture = UITapGestureRecognizer(target: self, action: #selector(self.showYearsDropDown))
        self.yearsDropDownContainer.addGestureRecognizer(showYearsDropDownTapGesture)
    }
    
    @objc func showEmployeeDropDown() {
        self.employeesDropDown.show()
    }
    
    @objc func showYearsDropDown() {
        self.yearsDropDown.show()
    }
    
    func reConfigureChart() {
        var pieEntries: [PieChartDataEntry] = []
        pieChart.legend.enabled = false
        pieChart.noDataText = "no data available"
        let totalDays: Int = self.viewModel.attendanceDays?.first(where: { (attendanceDay) -> Bool in
            "\(attendanceDay.year)" == self.viewModel.selectedYear
        })?.totalDays ?? 0
        
        let attendedDays: Int = self.viewModel.attendanceDays?.first(where: { (attendanceDay) -> Bool in
            "\(attendanceDay.year)" == self.viewModel.selectedYear
        })?.attendance ?? 0
        pieEntries.append(PieChartDataEntry(value: Double(totalDays - attendedDays)))
        pieEntries.append(PieChartDataEntry(value: Double(attendedDays)))
        let set: PieChartDataSet = PieChartDataSet(entries: pieEntries)
        set.colors = [UIColor.clear, UIColor.kdipaOrange]
        set.selectionShift = 7.0
        let data: PieChartData = PieChartData(dataSet: set)
        data.setDrawValues(false)
        self.pieChart.data = data
    }
}
