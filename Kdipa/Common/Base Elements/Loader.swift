//
//  Loader.swift
//  Kdipa
//
//  Created by Safoine Moncef Amine on 2/9/20.
//  Copyright © 2020 Safoine Moncef Amine. All rights reserved.
//

import UIKit

protocol LoaderProtocol {
    func show(onView view: UIView?, withTitle title: String)
    func hide(onView view: UIView?, withTitle title: String)
}

class Loader: LoaderProtocol {
    
    // MARK: - Properties
    
    static let shared = Loader()
    
    // MARK: - Init
    
    private init() {}
    
    /// SHow Loader
    ///
    /// - Parameters:
    ///   - onView: indicate the superView
    ///   - title: Title to show under spinner (empty if you don't want to show any title)
    func show(onView view: UIView? = nil, withTitle title: String) {
    }
    
    /// Hide Loader
    ///
    /// - Parameters:
    ///   - onView: indicate the superView
    ///   - title: Title to show under the check mark before hiding (empty title mean no check mark just hide)
    func hide(onView view: UIView? = nil, withTitle title: String) {
    }
}
