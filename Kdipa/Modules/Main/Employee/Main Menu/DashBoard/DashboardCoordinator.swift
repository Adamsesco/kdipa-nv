//
//  DashboardCoordinator.swift
//  Kdipa
//
//  Created by Safoine Moncef Amine on 2/17/20.
//  Copyright © 2020 Safoine Moncef Amine. All rights reserved.
//

import UIKit

class DashboardCoordinator: Coordinator {
    
    var childCoordinators: [Coordinator] = []
    var navigationController: UINavigationController
    var parentCoordinator: MenuCoordinator
    
    init(navigationController: UINavigationController, parentCoordinator: MenuCoordinator) {
        self.navigationController = navigationController
        self.parentCoordinator = parentCoordinator
    }
    
    func start() {
        let dashboardVC = DashboardViewController.instantiate(from: .mainMenu)
        dashboardVC.coordinator = self
        self.parentCoordinator.currentVC = dashboardVC
        self.navigationController.setViewControllers([dashboardVC], animated: true)
    }
    
    func newPermission() {
        (parentCoordinator as? MenuCoordinator)?.newPermission()
    }
}
