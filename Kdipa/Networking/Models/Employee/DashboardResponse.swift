//
//  DashboardResponse.swift
//  Kdipa
//
//  Created by Safoine Moncef Amine on 2/13/20.
//  Copyright © 2020 Safoine Moncef Amine. All rights reserved.
//

import Foundation

struct DashboardResponse: Codable {
    
    let absences: Int
    let attendedDays: Int
    let lastPunchIn: String
    let lastPunchOut: String
    let name: String
    let nameAr: String
    let permissionsCountRemaining: Int
    let permissionsRemaining: String
    let permissionsTaken: String
    let vacations: Int
    
    init(dict: [String: Any]) {
        self.absences = dict["absences"] as? Int  ?? 0
        self.attendedDays = dict["attendedDays"] as?  Int ?? 0
        self.lastPunchIn = dict["lastPunchIn"] as?  String ?? ""
        self.lastPunchOut = dict["lastPunchOut"] as?  String ?? ""
        self.name = dict["name"] as? String ?? ""
        self.nameAr = dict["nameAr"] as? String ?? ""
        self.permissionsCountRemaining = dict["permissionsCountRemaining"] as? Int ?? 0
        self.permissionsRemaining = dict["permissionsRemaining"] as? String ?? ""
        self.permissionsTaken = dict["permissionsTaken"] as? String ?? ""
        self.vacations = dict["vacations"] as? Int ?? 0
    }
}
