//
//  EmergencyLeaveService.swift
//  Kdipa
//
//  Created by Said Elmansour on 2020-02-26.
//  Copyright © 2020 Safoine Moncef Amine. All rights reserved.
//

import Foundation
import Alamofire

private let emergencyLeaveEndPoint = Constants.kdipaEmployeeBaseURL + "/api/emergencyLeave"

class EmergencyLeaveService {
    
    func postEmergencyLeave(emergencyLeave: EmergencyLeave, completion: @escaping (Error?) -> Void) {
        let parameters = emergencyLeave.dictionary as? [String: String] ?? [:]
        Alamofire.upload(multipartFormData: { multipartFormData in
            for (key, value) in parameters {
                multipartFormData.append(value.data(using: .utf8)!, withName: key)
            }
            
        }, to: emergencyLeaveEndPoint, encodingCompletion: { (encodingResult) in
            switch encodingResult {
            case .success(let upload, _, _):
                upload.responseJSON { _ in
                    completion(nil)
                }
            case .failure(let encodingError):
                completion(encodingError)
            }
        })
    }
}
