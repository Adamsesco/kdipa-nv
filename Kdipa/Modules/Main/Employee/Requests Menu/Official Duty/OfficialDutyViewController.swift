//
//  OfficialDutyViewController.swift
//  Kdipa
//
//  Created by Safoine Moncef Amine on 3/10/20.
//  Copyright © 2020 Safoine Moncef Amine. All rights reserved.
//

import UIKit
import FSCalendar
import DropDown

class OfficialDutyViewController: BaseViewController {
    
    // MARK: - Outlets
    
    @IBOutlet weak var dutyTypesDropDown: KdipaDropDown!
    @IBOutlet weak var employeeDropDown: KdipaDropDown!
    @IBOutlet weak var delegatesDropDown: KdipaDropDown!
    @IBOutlet weak var attendanceExemptionTableView: UITableView!
    @IBOutlet weak var lblFromDate: UILabel!
    @IBOutlet weak var lblToDate: UILabel!
    @IBOutlet weak var btnSubmit: UIButton!
    
    @IBOutlet weak var calendar: FSCalendar!
    @IBOutlet weak var constraintCalenderTrailingShow: NSLayoutConstraint!
    @IBOutlet weak var constraintCalendarTrailingHide: NSLayoutConstraint!
    @IBOutlet weak var delegateStack: UIStackView!
    @IBOutlet weak var btnRemoveDropDown: UIButton!
    
    @IBOutlet weak var lblScreenTitle: KdipaLabel!
    @IBOutlet weak var lblStartFromDesc: UILabel!
    @IBOutlet weak var lblToDesc: UILabel!
    @IBOutlet weak var lblEmployeeDesc: UILabel!
    @IBOutlet weak var lblDelegatedEmployeeDesc: UILabel!
    @IBOutlet weak var btnAddMoreDelegates: UIButton!
    
    // MARK: - Variables
    
    var coordinator: MenuCoordinator?
    private let viewModel: OfficialDutyViewModel = OfficialDutyViewModel()
    private var btnType: BtnDatesType?
    
    // MARK: VC life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupNavigationBarItems()
        self.configure()
        self.localize()
        self.bind()
        self.viewModel.loadOfficialDuty()
    }
    
    // MARK: - Configuration
    
    func configure() {
        self.attendanceExemptionTableView.register(
        UINib.nib(named: OfficialDutySelectionTableViewCell.reuseIdentifier),
        forCellReuseIdentifier: OfficialDutySelectionTableViewCell.reuseIdentifier)
        self.attendanceExemptionTableView.estimatedRowHeight = 45.0
        self.attendanceExemptionTableView.rowHeight = UITableView.automaticDimension
    }
    
    func bind() {
        guard let spinnerContainer = self.navigationController?.view ?? self.view else {
            return
        }
        
        self.viewModel.loadOfficialDutyAPICallState.bind { (state) in
            switch state {
            case .running:
                self.showSpinner(uiView: spinnerContainer)
            case .success:
                self.reloadDate()
                self.hideSpinner()
            case .failed:
                self.showConfirmationMessage(view: self.view, message: "server error")
            default:
                self.hideSpinner()
            }
        }
        
        self.viewModel.dates.fromDate.bind { (date) in
            self.lblFromDate.text = date?.formatTo(format: "EEE, MMM dd")
        }
        
        self.viewModel.dates.toDate.bind { (date) in
            self.lblToDate.text  = date?.formatTo(format: "EEE, MMM dd")
        }
        
        self.viewModel.submitOfficialDutyAPICallState.bind { (state) in
            switch state {
            case .running:
                self.showSpinner(uiView: spinnerContainer)
            case .success:
                self.hideSpinner()
                self.coordinator?.dashboard()
            case .failed:
                self.showConfirmationMessage(view: self.view, message: "server error")
            default:
                self.hideSpinner()
            }
        }
    }
    
    func reloadDate() {
        self.dutyTypesDropDown.reLoad(array: self.viewModel.dutyTypes)
        self.employeeDropDown.reLoad(array: self.viewModel.employees)
        self.delegatesDropDown.reLoad(array: self.viewModel.delegatedEmployees)
        self.attendanceExemptionTableView.reloadData()
        let indexPath = IndexPath(item: 0, section: 0)
        self.selectCell(index: indexPath)
    }
    
    func localize() {
        self.lblScreenTitle.text = Localizable.OfficialDuty.title
        self.lblStartFromDesc.text = Localizable.OfficialDuty.from
        self.lblToDesc.text = Localizable.OfficialDuty.to
        self.lblEmployeeDesc.text = Localizable.OfficialDuty.employee
        self.lblDelegatedEmployeeDesc.text = Localizable.OfficialDuty.delegatedEmployee
        self.btnSubmit.setTitle(Localizable.OfficialDuty.submit, for: .normal)
        self.btnAddMoreDelegates.setTitle(Localizable.OfficialDuty.addmoreDelegates, for: .normal)
    }
    
    // MARK: - UI Actions
    
    @IBAction func onFromDate(_ sender: Any) {
        self.showCalendar()
        btnType = .startDate
    }
    
    @IBAction func onToDate(_ sender: Any) {
        self.showCalendar()
        btnType = .endDate
    }
    
    @IBAction func onSubmit(_ sender: Any) {
        self.viewModel.submitNewOfficialDuty()
    }
    
    @IBAction func onAddDelegate(_ sender: Any) {
        self.btnRemoveDropDown.isHidden = false
        let containerVW = UIView()
        let delegateDropDown = KdipaDropDown()
        delegateDropDown.reLoad(array: self.viewModel.delegatedEmployees)
        containerVW.heightAnchor.constraint(equalToConstant: 45).isActive = true
        delegateStack.translatesAutoresizingMaskIntoConstraints = false
        containerVW.translatesAutoresizingMaskIntoConstraints = false
        delegateDropDown.translatesAutoresizingMaskIntoConstraints = false
        containerVW.addSubview(delegateDropDown)
        delegateDropDown.leadingAnchor.constraint(equalTo: containerVW.leadingAnchor, constant: 20).isActive = true
        delegateDropDown.trailingAnchor.constraint(equalTo: containerVW.trailingAnchor, constant: -20).isActive = true
        delegateDropDown.topAnchor.constraint(equalTo: containerVW.topAnchor, constant: 0).isActive = true
        delegateDropDown.bottomAnchor.constraint(equalTo: containerVW.bottomAnchor, constant: 0).isActive = true
        self.delegateStack.addArrangedSubview(containerVW)
    }
    
    @IBAction func onRemoveDropDown(_ sender: Any) {
        if let dropView = delegateStack.arrangedSubviews.last,
            delegateStack.arrangedSubviews.count > 1 {
            dropView.removeFromSuperview()
        }
        
        if delegateStack.arrangedSubviews.count <= 1 {
            self.btnRemoveDropDown.isHidden = true
        }
    }
    
    // MARK: - Heloper
    
    func showCalendar() {
        self.view.bringSubviewToFront(self.calendar)
        self.constraintCalendarTrailingHide.isActive = false
        self.constraintCalenderTrailingShow.isActive = true
        
        UIView.animate(withDuration: 0.3) {
            self.view.layoutIfNeeded()
        }
    }
    
    func hideCalendar() {
        self.constraintCalenderTrailingShow.isActive = false
        self.constraintCalendarTrailingHide.isActive = true
        
        UIView.animate(withDuration: 0.3) {
            self.view.layoutIfNeeded()
        }
    }
    
}

// MARK: - FSCalendarDelegate

extension OfficialDutyViewController: FSCalendarDelegate {
    func calendar(_ calendar: FSCalendar, didSelect date: Date, at monthPosition: FSCalendarMonthPosition) {
        self.hideCalendar()
        switch btnType {
        case .startDate:
            self.viewModel.dates.fromDate.value = date
        default:
            self.viewModel.dates.toDate.value = date
        }
    }
}

// MARK: - UITableViewDataSource

extension OfficialDutyViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.viewModel.attendanceExemption.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(
            withIdentifier: OfficialDutySelectionTableViewCell.reuseIdentifier,
            for: indexPath) as? OfficialDutySelectionTableViewCell else { return OfficialDutySelectionTableViewCell() }
        cell.configure(description: self.viewModel.attendanceExemption[indexPath.section], selected: false)
        cell.selectionStyle = .none
        return cell
    }
    
    func selectCell(index: IndexPath) {
        for cell in self.attendanceExemptionTableView.visibleCells {
            (cell as? OfficialDutySelectionTableViewCell)?.setAsUnselected()

        }
        if let cell = (self.attendanceExemptionTableView.cellForRow(at: index) as? OfficialDutySelectionTableViewCell) {
            cell.setAsSelected()
            self.viewModel.selectedAttendanceExemption = cell.content
        }
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 10
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let view = UIView()
        view.backgroundColor = UIColor.clear
        return view
    }
}

// MARK: - UITableViewDelegate

extension OfficialDutyViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.selectCell(index: indexPath)
    }
}

// MARK: - KdipaDropDownDelegate

extension OfficialDutyViewController: KdipaDropDownDelegate {
    
    func didSelectItem(sender: KdipaDropDown, index: Int, newValue: String, oldValue: String) {

        if sender == self.dutyTypesDropDown {
            self.viewModel.selectedtype = newValue
        } else if sender == self.employeeDropDown {
            self.viewModel.selectedEmployee = newValue
        } else {
            self.viewModel.selectedDelegates.removeAll { (value) -> Bool in
                value == oldValue
            }
            self.viewModel.selectedDelegates.append(newValue)
        }
    }
}
