//
//  EmployeeSettingViewController.swift
//  Kdipa
//
//  Created by Safoine Moncef Amine on 3/29/20.
//  Copyright © 2020 Safoine Moncef Amine. All rights reserved.
//

import UIKit

class EmployeeSettingViewController: BaseViewController {
    
    @IBOutlet weak var txtFieldFirstName: KdipaTextField!
    @IBOutlet weak var txtFieldLastName: KdipaTextField!
    @IBOutlet weak var txtFieldEmail: KdipaTextField!
    
    @IBOutlet weak var dropDownLanguage: KdipaDropDown!
    @IBOutlet weak var lblScreenTitle: KdipaLabel!
    @IBOutlet weak var lblDropDownDesc: UILabel!
    @IBOutlet weak var btnUpdateLang: UIButton!
    
    var coordinator: MenuCoordinator?
    
    var selectedLanguage: Language = Language.language
    
    override func viewDidLoad() {
        super.viewDidLoad()
        super.setupNavigationBarItems()
        self.configure()
        self.loadData()
        localize()
    }
    
    func configure() {
        self.txtFieldFirstName.type = .standard
        self.txtFieldLastName.type = .standard
        self.txtFieldEmail.type = .standard
        self.dropDownLanguage.reLoad(array: [Localizable.english, Localizable.arabic])
        self.dropDownLanguage.fill(description: Localizable.lang)
        self.dropDownLanguage.delegate = self
    }
    
    func localize() {
        self.lblScreenTitle.text = Localizable.EmployeeSetting.title
        self.lblDropDownDesc.text = Localizable.EmployeeSetting.dropDownDesc
        self.btnUpdateLang.setTitle(Localizable.EmployeeSetting.btnSave
            , for: .normal)
    }
    
    func loadData() {
        self.txtFieldFirstName.placeholder = SessionHandler.shared.connectedEmployee?.firstName
        self.txtFieldLastName.placeholder = SessionHandler.shared.connectedEmployee?.lastName
        self.txtFieldEmail.placeholder = UserDefaults.standard.string(forKey: Constants.UserDefaultsKeys.email)
    }
    
    @IBAction func updateLanguage(_ sender: Any) {
        let alert = UIAlertController(
            title: "are you sure ?".localized,
            message: "this will close the app",
            preferredStyle: .alert
        )
        
        alert.addAction(
            UIAlertAction(title: "Yes", style: .default, handler: { (_) in
                Language.language = self.selectedLanguage
                exit(0)
            })
        )
        
        alert.addAction(
            UIAlertAction(
                title: "Cancel".localized,
                style: UIAlertAction.Style.cancel,
                handler: nil
            )
        )
        present(alert, animated: true, completion: nil)
    }
}

extension EmployeeSettingViewController: KdipaDropDownDelegate {
    func didSelectItem(sender: KdipaDropDown, index: Int, newValue: String, oldValue: String) {
        if newValue == Localizable.english {
            self.selectedLanguage = .english
        } else {
            self.selectedLanguage = .arabic
        }
    }
}
