//
//  RemoteAPI.swift
//  IBMRemote
//
//  Created by Omar Aksmi on 9/16/19.
//  Copyright © 2019 IBM. All rights reserved.
//

import Foundation

enum KdipaAPI: APIProtocol {
    case login
    case investorLogin
    case applyForUser
    case dashboard
    case vacations
    case absence
    case permisions
    case attendancyDays
    case newVacations
    case submittVacation
    case historyNews
    case submitNewVacationAmendment
    case vacationExtention
    case postVacationExtention
    case vacationReturn
    case postVacationReturn
    case officialDuty
    case booking
    case investorDashboard
    case requestStatus
    case cancelRequest
    case forgetPassword
}

extension KdipaAPI {
    
    var employeeBaseURL: URL {
        return URL(string: Constants.kdipaEmployeeBaseURL)!
    }
    var investorBaseURL: URL {
        return URL(string: Constants.kdipaInvestorBaseURL)!
    }
    
    var path: String {
        switch self {
        case .login:
            return "/api/login"
        case .applyForUser:
            return "/api/applyForUsername"
        case .dashboard:
            return "/api/dashboard"
        case .absence:
            return "/api/historyAbsent"
        case .vacations:
            return "/api/historyVacation"
        case .permisions:
            return "/api/historyPermission"
        case .attendancyDays:
            return "/api/getDaysOfAttendance"
        case .newVacations:
            return "/api/newVacation"
        case .submittVacation:
            return "/api/postNewVacation"
        case .historyNews:
            return "/api/historyNews"
        case .submitNewVacationAmendment:
            return "/api/vacationAmendment"
        case .vacationExtention:
            return "/api/vacationExtension"
        case .postVacationExtention:
            return "/api/postVacationExtension"
        case .vacationReturn:
            return "/api/vacationReturn"
        case .postVacationReturn:
            return ""
        case .officialDuty:
            return "/api/newDuty"
        case .investorLogin:
            return "/api/auth/signin"
        case .booking:
            return "/api/bookingForm"
        case .investorDashboard:
            return "/api/investor-dashboard"
        case .requestStatus:
            return "/api/newVacationCancel"
        case .cancelRequest:
            return "/api/postVacationCancel"
        case .forgetPassword:
            return "/api/auth/forgotPassword"
        }
    }
    
}
