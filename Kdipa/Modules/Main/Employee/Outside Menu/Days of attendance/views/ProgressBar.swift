//
//  ProgressBar.swift
//  Kdipa
//
//  Created by Safoine Moncef Amine on 3/8/20.
//  Copyright © 2020 Safoine Moncef Amine. All rights reserved.
//

import UIKit
import MaterialComponents.MaterialProgressView

class ProgressBar: UIView {
    
    func configure(progress: Float) {
        let progressView = MDCProgressView()
        progressView.progress = progress
        let progressViewHeight = CGFloat(4)
        progressView.frame = CGRect(x: 0,
                                    y: self.bounds.height - progressViewHeight,
                                    width: self.bounds.width,
                                    height: progressViewHeight)
        progressView.progressTintColor = UIColor.kdipaDarkBlue
        progressView.setHidden(false, animated: true)
        self.addSubview(progressView)
    }
    
}
