//
//  DaysOfAttendance.swift
//  Kdipa
//
//  Created by Safoine Moncef Amine on 3/7/20.
//  Copyright © 2020 Safoine Moncef Amine. All rights reserved.
//

import Foundation

struct DaysOfAttendance: Codable {
    let year, totalDays, attendance, absences: Int
    let vacations: Int
    let permissions: String
}
