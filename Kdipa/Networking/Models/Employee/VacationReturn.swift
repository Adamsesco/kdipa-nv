//
//  VacationReturn.swift
//  Kdipa
//
//  Created by Safoine Moncef Amine on 3/10/20.
//  Copyright © 2020 Safoine Moncef Amine. All rights reserved.
//

import Foundation

// MARK: - VacationReturn

struct VacationReturn: Codable {
    let departureDate, returnDate: String
    let vacationTypes, vacationTypesAr: [VacationType]
}

// MARK: - VacationReturnPayload

struct VacationReturnPayload: Codable {
    let email, vacationID, returnDate, lateReturnReason: String
    enum CodingKeys: String, CodingKey {
        case email
        case vacationID = "vacationId"
        case returnDate, lateReturnReason
    }
}
