//
//  NewVacationViewController.swift
//  Kdipa
//
//  Created by Safoine Moncef Amine on 3/8/20.
//  Copyright © 2020 Safoine Moncef Amine. All rights reserved.
//

import UIKit
import FSCalendar
import DropDown

class NewVacationViewController: BaseViewController {
    
    var coordinator: MenuCoordinator?
    
    @IBOutlet weak var departedDate: DateItem!
    @IBOutlet weak var returnDate: DateItem!
    @IBOutlet weak var calenderVW: FSCalendar!
    @IBOutlet weak var constraintCalenderLeading: NSLayoutConstraint!
    @IBOutlet weak var constraintCalenderTrailing: NSLayoutConstraint!
    
    @IBOutlet weak var lblDelegateName: KdipaLabel!
    @IBOutlet weak var delegateDropDownContainer: RoundedView!
    
    @IBOutlet weak var lblVacationType: KdipaLabel!
    @IBOutlet weak var vacationTypesDropDownContainer: RoundedView!
    
    @IBOutlet weak var lblScreenTitle: KdipaLabel!
    @IBOutlet weak var lblDepartureDate: UILabel!
    @IBOutlet weak var lblReturnDate: UILabel!
    @IBOutlet weak var lblDelegatedEmployee: UILabel!
    @IBOutlet weak var btnSsubmit: UIButton!
    @IBOutlet weak var paidVacationLbl: KdipaLabel!
    @IBOutlet weak var isPaidVacationSwitch: UISwitch!
    
    let employeesDropDown: DropDown = DropDown()
    let vacationTypesDropDown: DropDown = DropDown()
    var selectedDateItem: DateItem?
    
    var viewModel: NewVacationViewModel = NewVacationViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupNavigationBarItems()
        self.bind()
        self.configure()
        self.localize()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.viewModel.loadNewVacations()
    }
    
    func configure() {
        self.departedDate.delegate = self
        self.returnDate.delegate = self
        self.calenderVW.delegate = self
        self.configureDropDowns()
    }
    
    func localize() {
        self.lblScreenTitle.text = Localizable.NewVacation.title
        self.lblDepartureDate.text = Localizable.NewVacation.departureDate
        self.lblReturnDate.text = Localizable.NewVacation.returnDate
        self.lblDelegatedEmployee.text = Localizable.NewVacation.delegatedEmployee
        self.btnSsubmit.setTitle(Localizable.NewVacation.submit, for: .normal)
        self.paidVacationLbl.text = "new.vacation.is.paid".localized
    }
    
    func bind() {
        self.viewModel.loadNewVacationsCallState.bind { (state) in
            switch state {
            case .running:
                self.showSpinner(uiView: self.coordinator?.navigationController.view ?? self.view)
            case .success:
                self.hideSpinner()
                self.configureDropDowns()
            case .failed:
                self.hideSpinner()
                self.showConfirmationMessage(view: self.view, message: "something went wrong")
            default:
                self.hideSpinner()
            }
        }
        
        self.viewModel.selectedVacationType.bind { (type) in
            self.lblVacationType.text = type
        }
        
        self.viewModel.selectedEmployee.bind { (employee) in
            self.lblDelegateName.text = employee?.employeeName
        }
        
        self.viewModel.submittNewVacationsCallState.bind { (state) in
            switch state {
            case .running:
                self.showSpinner(uiView: self.coordinator?.navigationController.view ?? self.view)
            case .success:
                self.hideSpinner()
                self.coordinator?.dashboard()
            case .failed:
                self.hideSpinner()
                self.showConfirmationMessage(view: self.view, message: "something went wrong")
            default:
                self.hideSpinner()
            }
        }
    }
    func configureDropDowns() {
        self.configureDelegatesDropDown()
        self.configureVacationsTypesDropDown()
    }
    
    private func configureDelegatesDropDown() {
        let array = self.viewModel.delegates.map { $0.employeeName }
        employeesDropDown.dataSource = array
        employeesDropDown.anchorView = self.delegateDropDownContainer
        employeesDropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            self.viewModel.selectedEmployee.value = self.viewModel.delegates.first(where: { (employee) -> Bool in
                employee.employeeName == item
            })
        }
        let showDropDownTapGesture = UITapGestureRecognizer(target: self, action: #selector(self.showDelegatesDropDown))
        self.delegateDropDownContainer.addGestureRecognizer(showDropDownTapGesture)
    }
    
    private func configureVacationsTypesDropDown() {

        let array = self.viewModel.newVacations?.vacationTypes.map({ (vacationType) -> String in
            return vacationType.vacationName
        }) ?? []
        vacationTypesDropDown.dataSource = array
        vacationTypesDropDown.anchorView = self.vacationTypesDropDownContainer
        vacationTypesDropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            self.viewModel.selectedVacationType.value = item
        }
        let showDropDownTapGesture = UITapGestureRecognizer(target: self, action: #selector(self.showVacationTypesDropDown))
        self.vacationTypesDropDownContainer.addGestureRecognizer(showDropDownTapGesture)
    }
    
    @objc func showDelegatesDropDown() {
        self.employeesDropDown.show()
    }
    
    @objc func showVacationTypesDropDown() {
        self.vacationTypesDropDown.show()
    }
    
    func hideCalender() {
        self.constraintCalenderLeading.isActive = false
        self.constraintCalenderTrailing.isActive = true
        UIView.animate(withDuration: 0.3) {
            self.view.layoutIfNeeded()
        }
    }
    
    func showCalender() {
        self.constraintCalenderTrailing.isActive = false
        self.constraintCalenderLeading.isActive = true
        UIView.animate(withDuration: 0.3) {
            self.view.layoutIfNeeded()
        }
    }
    
    // MARK: - UI Actions
    
    @IBAction func btnSubmitNewVacationClicked(_ sender: Any) {
        self.viewModel.isAdvancedPayment  = self.isPaidVacationSwitch.isOn
        self.viewModel.submitNewVacation()
    }
}

extension NewVacationViewController: DateItemDelegate {
    func dateFieldCleared(sender: DateItem) {
        self.showCalender()
        self.selectedDateItem = sender
    }
    
    func dateFieldFilled(sender: DateItem) {
        self.hideCalender()
    }
}

extension NewVacationViewController: FSCalendarDelegate {
    func calendar(_ calendar: FSCalendar, didSelect date: Date, at monthPosition: FSCalendarMonthPosition) {
        self.selectedDateItem?.fillDateWith(date: date)
        if selectedDateItem == self.returnDate {
            self.viewModel.selectedToDate = date.formatTo(format: "yyyy/mm/dd")
        } else {
            self.viewModel.selectedFromDate = date.formatTo(format: "yyyy/mm/dd")
        }
    }
}
