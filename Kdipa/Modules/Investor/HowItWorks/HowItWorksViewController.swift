//
//  HowItWorksViewController.swift
//  Kdipa
//
//  Created by Safoine Moncef Amine on 3/31/20.
//  Copyright © 2020 Safoine Moncef Amine. All rights reserved.
//

import UIKit
import WebKit

class HowItWorksViewController: BaseViewController, WKNavigationDelegate {

    var webView: WKWebView!
    var coordinator: MenuCoordinator?
    
    override func loadView() {
        webView = WKWebView()
        webView.navigationDelegate = self
        view = webView
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        super.setupNavigationBarItems()
        let url = URL(string: "https://www.kdipa.gov.kw/en/faq-2/")!
        webView.load(URLRequest(url: url))
        webView.allowsBackForwardNavigationGestures = true
    }
}
