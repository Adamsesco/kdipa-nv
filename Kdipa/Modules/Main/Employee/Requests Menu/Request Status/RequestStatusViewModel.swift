//
//  RequestStatusViewModel.swift
//  Kdipa
//
//  Created by Safoine Moncef Amine on 5/24/20.
//  Copyright © 2020 Safoine Moncef Amine. All rights reserved.
//

import Foundation

class RequestStatusViewModel {
    
    let service = RequestStatusService()
    var loadRequestsCallStatus: Box<ApiCallState> = Box(.inactive)
    var cancelRequestCallState: Box<ApiCallState> = Box(.inactive)
    var requestsResponse: RequestStatusResponse?
    
    func loadRequests() {
        loadRequestsCallStatus.value = .running
        service.loadRequestsStatus(email: UserDefaults.standard.string(forKey: Constants.UserDefaultsKeys.email)!) { [unowned self] (response, error) in
            if error != nil {
                self.loadRequestsCallStatus.value = .failed
            } else {
                self.requestsResponse = response
                self.loadRequestsCallStatus.value = .success
            }
        }
    }
    
    func cancelRequest(vacationID: String) {
        cancelRequestCallState.value = .running
        self.service.cancel(email: UserDefaults.standard.string(forKey: Constants.UserDefaultsKeys.email)!, vacationID: vacationID) { (error) in
            if error == nil {
                self.requestsResponse?.year.the2020.removeAll(where: { (req) -> Bool in
                    return String(req.vacationID) == vacationID
                })
                self.cancelRequestCallState.value = .success
            } else {
                self.cancelRequestCallState.value = .failed
            }
        }
    }
}
