//
//  VacationAmendment.swift
//  Kdipa
//
//  Created by Said Elmansour on 2020-03-01.
//  Copyright © 2020 Safoine Moncef Amine. All rights reserved.
//

import Foundation

// MARK: - VacationAmendment
struct VacationAmendment: Codable {
    let newFromDate, newToDate, oldFromDate, oldToDate: String
    let vacationTypes, vacationTypesAr: [VacationType]
}

struct VacationAmendmentPayload: Codable {
    var email: String = UserDefaults.standard.string(forKey: Constants.UserDefaultsKeys.email) ?? ""
    var vacationType: String = ""
    var oldFromDate: String = ""
    var newFromDate: String = ""
    var newToDate: String = ""
    var oldToDate: String = ""
    var delegatedEmployee: String = ""
}
