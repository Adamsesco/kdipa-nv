//
//  SignInViewController.swift
//  Kdipa
//
//  Created by Safoine Moncef Amine on 2/8/20.
//  Copyright © 2020 Safoine Moncef Amine. All rights reserved.
//

import UIKit

class SignInViewController: BaseViewController {
        
    @IBOutlet weak var txtFieldUsername: KdipaTextField!
    @IBOutlet weak var txtFieldPassword: KdipaTextField!
    @IBOutlet weak var btnLogin: UIView!
    @IBOutlet weak var btnApply: UIView!
    @IBOutlet weak var applyBtn: UIButton!
    
    @IBOutlet weak var lblForgetPassword: KdipaLabel!
    @IBOutlet weak var lblWelcome: KdipaLabel!
    @IBOutlet weak var lblSubTitle: KdipaLabel!
    
    @IBOutlet weak var loginBtn: UIButton!
    
    @IBOutlet weak var forgetPasswordVw: UIView!
    
    var viewModel: SignInViewModel = SignInViewModel()
    var coordinator: MainCoordinator?

    override func viewDidLoad() {
        super.viewDidLoad()
        self.coordinator = MainCoordinator(navigationController: AppDelegate.navController)
        configure()
        localize()
        self.txtFieldUsername.text = UserDefaults.standard.string(forKey: Constants.UserDefaultsKeys.email)
        self.txtFieldPassword.text = UserDefaults.standard.string(forKey: Constants.UserDefaultsKeys.password)
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        let tapOnViewGesture = UITapGestureRecognizer(target: self, action: #selector(self.hideKeyboard))
        self.view.addGestureRecognizer(tapOnViewGesture)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
    }
    
    // MARK: - Configure
    
    func configure() {
        self.txtFieldUsername.textfield.delegate = self
        self.txtFieldPassword.textfield.delegate = self
        self.txtFieldUsername.type = .username
        self.txtFieldPassword.type = .password
        self.loginBtn.backgroundColor = UIColor.kdipaOrange
        self.loginBtn.setTitle("login.login".localized, for: .normal)
        self.loginBtn.titleLabel?.textColor = UIColor.white
        let loginTapGesture = UITapGestureRecognizer(target: self, action: #selector(login))
        self.btnLogin.addGestureRecognizer(loginTapGesture)
        
        let applyTapGesture = UITapGestureRecognizer(target: self, action: #selector(applyForUserName))
        self.btnApply.addGestureRecognizer(applyTapGesture)
        
        let forgetPasswordTapGesture = UITapGestureRecognizer(target: self, action: #selector(self.resetPassword))
        self.forgetPasswordVw.addGestureRecognizer(forgetPasswordTapGesture)
        
        self.viewModel.loginState.bind { (state) in
            switch state {
            case .isRuning:
                self.showLoader()
            case .loginSuccess:
                self.handleLoginSuccess()
            case .loginFailure:
                self.handleLoginFailure()
            default:
                self.hideSpinner()
            }
        }
    }
    
    func localize() {
        self.lblWelcome.text = Localizable.Login.title
        self.lblSubTitle.text = Localizable.Login.subtitle
        self.lblForgetPassword.text = Localizable.Login.forgetPassword
        self.applyBtn.setTitle(Localizable.Login.applyUserName, for: .normal)
    }
    
    // MARK: - Actions
    
    @IBAction func btnLoginTap(_ sender: Any) {
        self.viewModel.login(email: self.txtFieldUsername.text ?? "", password: self.txtFieldPassword.text ?? "")
    }
    @IBAction func btnApplyClicked(_ sender: Any) {
        coordinator?.applyForUser()
    }
    
    @objc func login() {
        self.viewModel.login(email: self.txtFieldUsername.text ?? "", password: self.txtFieldPassword.text ?? "")
    }
    
    @objc func applyForUserName() {
        coordinator?.applyForUser()
    }
    
    @objc func resetPassword() {
        coordinator?.forgetPassword()
    }
    
    @objc private func hideKeyboard() {
        self.txtFieldUsername.textfield.resignFirstResponder()
        self.txtFieldPassword.textfield.resignFirstResponder()
    }
    
    // MARK: - Handlers
    
    func showLoader() {
        self.showSpinner(uiView: self.view)
    }
    
    func handleLoginSuccess() {
        DispatchQueue.main.async {
            self.hideSpinner()
            if self.viewModel.isInvestor {
                self.coordinator?.investor()
            } else {
                self.coordinator?.dashboard()
            }
        }
    }
    
    func handleLoginFailure() {
        DispatchQueue.main.async {
            self.hideSpinner()
            let alert = UIAlertController(title: "your email or password are wrong", message: "please try again", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
            self.present(alert, animated: true)
        }
    }
}

extension SignInViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
}
