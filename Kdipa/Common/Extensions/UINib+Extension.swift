//
//  UINib+Extension.swift
//  Kdipa
//
//  Created by Said Elmansour on 2020-02-24.
//  Copyright © 2020 Safoine Moncef Amine. All rights reserved.
//

import Foundation
import UIKit

extension UINib {
    
    /// Load Nib with name
    ///
    /// - Parameter nibName: Nib name
    /// - Returns: UINib
    static func nib(named nibName: String) -> UINib {
        return UINib(nibName: nibName, bundle: nil)
    }
}
