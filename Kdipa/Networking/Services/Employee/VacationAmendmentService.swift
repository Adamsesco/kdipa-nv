//
//  vacationAmendmentService.swift
//  Kdipa
//
//  Created by Said Elmansour on 2020-03-01.
//  Copyright © 2020 Safoine Moncef Amine. All rights reserved.
//

import Foundation
import Alamofire
import UIKit

class VacationAmendmentService {
    
    let getVacationAmendmentEndPoint = Constants.kdipaEmployeeBaseURL + "/api/vacationAmendment"
    let postVacationAmendmentEndPoint = Constants.kdipaEmployeeBaseURL + "/api/postvacationAmendment"
    
    func getvacationAmendment(completion: @escaping (VacationAmendment?, Error?) -> Void) {

        let parameters = ["email": UserDefaults.standard.string(forKey: Constants.UserDefaultsKeys.email) ?? ""]
        let headers = ["Content-Type": "application/json", "Accept": "application/json"]

        Alamofire.request(getVacationAmendmentEndPoint,
                          method: .post,
                          parameters: parameters,
                          encoding: JSONEncoding.default,
                          headers: headers).responseData { (response) in
            switch response.result {
            case .success(let data):
                do {
                    let allVacationAmendments = try JSONDecoder().decode(VacationAmendment.self, from: data)
                    completion(allVacationAmendments, nil)
                } catch {
                    completion(nil, error)
                }
            case .failure(let error):
                completion(nil, error)
            }
        }
    }
    
    func postVacationAmendment(payload: VacationAmendmentPayload, completion: @escaping (Error?) -> Void) {
        let url = URL(string: Constants.kdipaEmployeeBaseURL + KdipaAPI.submittVacation.path)!
        let params = payload.dictionary
        let headers = ["Content-Type": "application/json", "Accept": "application/json"]
        Alamofire.request(url, method: .post, parameters: params, encoding: JSONEncoding.default, headers: headers).responseData { (response) in
            switch response.result {
            case .success:
                completion(nil)
            case .failure(let error):
                completion(error)
            }
        }
    }
}
