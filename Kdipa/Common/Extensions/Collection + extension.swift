//
//  Collection + extension.swift
//  Kdipa
//
//  Created by Safoine Moncef Amine on 3/5/20.
//  Copyright © 2020 Safoine Moncef Amine. All rights reserved.
//

import Foundation

extension Collection where Indices.Iterator.Element == Index {
    subscript (exist index: Index) -> Iterator.Element? {
        return indices.contains(index) ? self[index] : nil
    }
}
