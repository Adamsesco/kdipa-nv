//
//  VacationAmendmentViewModel.swift
//  Kdipa
//
//  Created by Said Elmansour on 2020-03-01.
//  Copyright © 2020 Safoine Moncef Amine. All rights reserved.
//

import Foundation
struct VacationAmendmentDates {
    var oldFromDate: Box<Date?> = Box(nil)
    var oldToDate: Box<Date?> = Box(nil)
    var newFromDate: Box<Date?> = Box(nil)
    var newToDate: Box<Date?> = Box(nil)
}

class VacationAmendmentViewModel {

    var vacationAmendment: VacationAmendment? {
        didSet {
            self.vacationAmendmentDates.oldFromDate.value = self.vacationAmendment?.oldFromDate.getDate()
            self.vacationAmendmentDates.oldToDate.value = self.vacationAmendment?.oldToDate.getDate()
            self.vacationAmendmentDates.newFromDate.value = self.vacationAmendment?.newFromDate.getDate()
            self.vacationAmendmentDates.newToDate.value = self.vacationAmendment?.newToDate.getDate()
        }
    }
    
    var payload: VacationAmendmentPayload = VacationAmendmentPayload()
    var vacationAmendmentDates: VacationAmendmentDates = VacationAmendmentDates()
    var vacationType: [VacationType]?
    var submittingVacationAmendmentCallState: Box<ApiCallState> = Box(.inactive)
    var gettingVAmendmentCallState: Box<ApiCallState> = Box(.inactive)
    
    private var services = VacationAmendmentService()

    func getvacationAmendment() {
        self.gettingVAmendmentCallState.value = .running
        services.getvacationAmendment { (vacation, error) in
            if vacation != nil {
                self.vacationAmendment = vacation
                self.payload.oldFromDate = vacation?.oldFromDate ?? ""
                self.payload.oldToDate = vacation?.oldToDate ?? ""
                self.vacationType = vacation?.vacationTypes
                self.gettingVAmendmentCallState.value = .success
            }
            if error != nil {
                self.gettingVAmendmentCallState.value = .failed
            }
        }
    }
    
    func submitVacationAmendment() {
        let newVacAmendment = VacationAmendmentPayload(
            email: UserDefaults.standard.string(forKey: Constants.UserDefaultsKeys.email) ?? "",
            vacationType: self.payload.vacationType,
            oldFromDate: self.vacationAmendmentDates.oldFromDate.value?.formatTo(format: "yyyy/mm/dd") ?? "",
            newFromDate: self.vacationAmendmentDates.newFromDate.value?.formatTo(format: "yyyy/mm/dd") ?? "",
            newToDate: self.vacationAmendmentDates.newToDate.value?.formatTo(format: "yyyy/mm/dd") ?? "",
            oldToDate: self.vacationAmendmentDates.oldToDate.value?.formatTo(format: "yyyy/mm/dd") ?? "",
            delegatedEmployee: self.payload.delegatedEmployee)
        
        self.submittingVacationAmendmentCallState.value = .running
        services.postVacationAmendment(payload: newVacAmendment) { (error) in
            if error == nil {
                self.submittingVacationAmendmentCallState.value = .success
            } else {
                self.submittingVacationAmendmentCallState.value = .failed
            }
        }
    }
    
    private func formatDateForDateField(fromDate: Date?) -> String {
        guard let date = fromDate else {
            return ""
        }
        let dateFormatterPrint = DateFormatter()
        dateFormatterPrint.dateFormat = "EEE, MMM dd "
        return dateFormatterPrint.string(from: date)
    }
    
    private func formatDateForSubmiting(fromDate: Date?) -> String {
        guard let date = fromDate else {
            return ""
        }
        let dateFormatterPrint = DateFormatter()
        dateFormatterPrint.dateFormat = "YYYY/MM/dd"
        return dateFormatterPrint.string(from: date)
    }
    
}
