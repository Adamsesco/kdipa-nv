//
//  IntroViewController.swift
//  Kdipa
//
//  Created by Safoine Moncef Amine on 6/3/20.
//  Copyright © 2020 Safoine Moncef Amine. All rights reserved.
//

import UIKit

class IntroViewController: UIPageViewController, Storyboarded {
    
    var pageControl = UIPageControl()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.dataSource = self
        self.delegate   = self
        
        if let firstVC = pages.first
        {
            setViewControllers([firstVC], direction: .forward, animated: true, completion: nil)
        }
        self.configurePageControl()
    }
    
    func configurePageControl() {
        // The total number of pages that are available is based on how many available colors we have.
        pageControl = UIPageControl(frame: CGRect(x: 0,y: UIScreen.main.bounds.maxY - 50,width: UIScreen.main.bounds.width,height: 50))
        self.pageControl.numberOfPages = pages.count
        self.pageControl.currentPage = 0
        self.pageControl.tintColor = UIColor.black
        self.pageControl.pageIndicatorTintColor = UIColor.white
        self.pageControl.currentPageIndicatorTintColor = UIColor.kdipaOrange
        self.view.addSubview(pageControl)
    }
    
    fileprivate lazy var pages: [UIViewController] = {
        return [
            self.getViewController(withIdentifier: "firstIntroVC"),
            self.getViewController(withIdentifier: "secondIntroVC"),
            self.getViewController(withIdentifier: "thirdIntroVC"),
            self.getViewController(withIdentifier: "fourthIntroVC")
        ]
    }()
    
    fileprivate func getViewController(withIdentifier identifier: String) -> UIViewController
    {
        return UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: identifier)
    }
}


extension IntroViewController: UIPageViewControllerDataSource, UIPageViewControllerDelegate {
 
    func pageViewController(_ pageViewController: UIPageViewController,
                            viewControllerBefore viewController: UIViewController) -> UIViewController? {
            guard let viewControllerIndex = pages.index(of: viewController) else { return nil }
            
            let previousIndex = viewControllerIndex - 1
            
            guard previousIndex >= 0          else { return pages.last }
            
            guard pages.count > previousIndex else { return nil        }
            
            return pages[previousIndex]
    }
    
    func pageViewController(_ pageViewController: UIPageViewController,
                            viewControllerAfter viewController: UIViewController) -> UIViewController? {
            guard let viewControllerIndex = pages.index(of: viewController) else { return nil }
            
            let nextIndex = viewControllerIndex + 1
            
            guard nextIndex < pages.count else { return pages.first }
            
            guard pages.count > nextIndex else { return nil         }
            
            return pages[nextIndex]
    }
    
    // MARK: Delegate functions
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        let pageContentViewController = pageViewController.viewControllers![0]
        self.pageControl.currentPage = pages.index(of: pageContentViewController)!
    }
}
