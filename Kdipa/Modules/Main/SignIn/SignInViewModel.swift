//
//  SignInViewModel.swift
//  Kdipa
//
//  Created by Safoine Moncef Amine on 2/9/20.
//  Copyright © 2020 Safoine Moncef Amine. All rights reserved.
//

import Foundation
enum LoginStates {
    case notActive
    case isRuning
    case loginSuccess
    case loginFailure
}

class SignInViewModel {
    
    let user: Box<EmployeeLoginResponse?> = Box(nil)
    var loginState: Box<LoginStates> = Box(.notActive)
    var isInvestor: Bool = false
    var dashboard: DashboardResponse?
    private let service = SessionHandler.shared
    
    func login(email: String, password: String) {
        self.loginState.value = .isRuning
        if email.contains("@burgan-systems.com") || email.contains("@kdipa.gov.kw"){
            self.isInvestor = false
            self.loginAsEmployee(email: email, password: password)
        } else {
            self.isInvestor = true
            self.loginAsInvestor(email: email, password: password)
        }
    }
    
    private func loginAsEmployee(email: String, password: String) {
        if email.contains("@burgan-systems.com") {
            Constants.isLive = false
        } else {
            Constants.isLive = true
        }
        service.loginAsEmployee(email: email, password: password) { _, error  in
            if error == nil {
                self.user.value = SessionHandler.shared.connectedEmployee
                if self.user.value?.result == "false" {
                    self.loginState.value = .loginFailure
                    return
                }
                UserDefaults.standard.set(email, forKey: Constants.UserDefaultsKeys.email)
                UserDefaults.standard.set(password, forKey: Constants.UserDefaultsKeys.password)
                self.loginState.value = .loginSuccess
            } else {
                self.loginState.value = .loginFailure
            }
        }
    }
    
    private func loginAsInvestor(email: String, password: String) {
        service.loginAsInvestor(email: email, password: password) { (response, error) in
            if error == nil {
                self.service.connectedInvestor = response
                self.loginState.value = .loginSuccess
                UserDefaults.standard.set(email, forKey: Constants.UserDefaultsKeys.email)
                UserDefaults.standard.set(password, forKey: Constants.UserDefaultsKeys.password)
            } else {
                self.loginState.value = .loginFailure
            }
        }
    }
}
