//
//  OfficalDuty.swift
//  Kdipa
//
//  Created by Safoine Moncef Amine on 3/11/20.
//  Copyright © 2020 Safoine Moncef Amine. All rights reserved.
//

import Foundation
import Alamofire

struct OfficalDutyService {
    
    func loadOfficialDuty(email: String, completion: @escaping (OfficialDuty?, Error?) -> Void) {
                let url = URL(string: Constants.kdipaEmployeeBaseURL + KdipaAPI.officialDuty.path)!
        let params = ["email": email]
        let headers = ["Content-Type": "application/json", "Accept": "application/json"]
        Alamofire.request(url, method: .post, parameters: params, encoding: JSONEncoding.default, headers: headers).responseData { (response) in
            switch response.result {
            case .success(let value):
                do {
                    let vacationExtention = try JSONDecoder().decode(OfficialDuty.self, from: value)
                    completion(vacationExtention, nil)
                } catch {
                    completion(nil, error)
                }
            case .failure(let error):
                completion(nil, error)
            }
        }
    }
    
     func submitNewVacationsReturnn(payload: OfficialDutyPayload,
                                    completion: @escaping (Error?) -> Void) {
          let url = URL(string: Constants.kdipaEmployeeBaseURL + KdipaAPI.postVacationReturn.path)!
          let params = payload.dictionary
          let headers = ["Content-Type": "application/json", "Accept": "application/json"]
          Alamofire.request(url, method: .post, parameters: params, encoding: JSONEncoding.default, headers: headers).responseData { (response) in
              switch response.result {
              case .success:
                  completion(nil)
              case .failure(let error):
                  completion(error)
              }
          }
      }
}
