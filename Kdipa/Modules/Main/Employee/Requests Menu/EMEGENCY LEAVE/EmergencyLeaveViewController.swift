//
//  EmergencyLeaveViewController.swift
//  Kdipa
//
//  Created by Said Elmansour on 2020-02-23.
//  Copyright © 2020 Safoine Moncef Amine. All rights reserved.
//

import UIKit
import FSCalendar

class EmergencyLeaveViewController: BaseViewController {

    // MARK: - Views

    @IBOutlet weak var titleLabel: KdipaLabel!
    @IBOutlet weak var calendar: FSCalendar!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var addAttachmentButton: UIButton!
    @IBOutlet weak var submitButton: UIButton!

    // MARK: - Properties
    
    var imagePicker: ImagePicker!
    var viewModel = EmergencyLeaveViewModel()

    override func viewDidLoad() {
        super.viewDidLoad()

        setupCollectionView()
        configure()
        self.setupNavigationBarItems()

        self.imagePicker = ImagePicker(presentationController: self, delegate: self)

    }

    // MARK: - Helpers
    private func setupCollectionView() {
        // Register Cell
        collectionView.register(UINib.nib(named: EmergencyLeaveCell.reuseIdentifier), forCellWithReuseIdentifier: EmergencyLeaveCell.reuseIdentifier)
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        layout.itemSize = CGSize(width: 172, height: 230)
        layout.minimumLineSpacing = 5
        layout.minimumInteritemSpacing = 1
        collectionView?.setCollectionViewLayout(layout, animated: false)
        collectionView.dataSource = self
        collectionView.delegate = self
    }
    
    private func configure() {
        self.viewModel.submittingPermissionCallState.bind { (state) in
            switch state {
            case .running:
                self.showSpinner(uiView: self.navigationController?.view ?? self.view)
            case .success:
                self.hideSpinner()
                self.showConfirmationMessage(view: self.view, message: "Emergency Leave successfully sent")
            case .failed:
                self.hideSpinner()
                self.showConfirmationMessage(view: self.view, message: "something went wrong")
            default:
                self.hideSpinner()
            }
        }
    }
    
    // MAKE: - Actions

    @IBAction func onAddAttachement(_ sender: Any) {
        if let view = sender as? UIView {
            self.imagePicker.present(from: view)
        }
    }
    
    @IBAction func onSubmit(_ sender: Any) {
        self.viewModel.submitEmergencyLeave()
    }
}

// MARK: - Collection View Data Source

extension EmergencyLeaveViewController: UICollectionViewDataSource {

    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.viewModel.emergencyLeaveDocuments.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        guard let cell = collectionView.dequeueReusableCell(
            withReuseIdentifier: EmergencyLeaveCell.reuseIdentifier,
            for: indexPath) as? EmergencyLeaveCell else { return EmergencyLeaveCell() }
        
        cell.confugire(index: indexPath.row, image: self.viewModel.emergencyLeaveDocuments[indexPath.row])
        cell.delegate = self
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) ->
       CGSize {
        return CGSize(width: 172, height: 230)
    
    }
}

// MARK: - Collection View Delegate

extension EmergencyLeaveViewController: UICollectionViewDelegate {
    
}

// MARK: - Calender delegate

extension EmergencyLeaveViewController: FSCalendarDelegate {
    func calendar(_ calendar: FSCalendar, didSelect date: Date, at monthPosition: FSCalendarMonthPosition) {
        self.viewModel.emergencyLeave.trxDate = date.description

    }
}

// MARK: - ImagePickerDelegate

extension EmergencyLeaveViewController: ImagePickerDelegate {

    func didSelect(image: UIImage?) {
        self.viewModel.emergencyLeave.filename = image?.pngData()
        self.viewModel.emergencyLeaveDocuments.append(image ?? UIImage())
        self.collectionView.reloadData()
    }
}

// MARK: - EmergencyLeaveCellDelegate

extension EmergencyLeaveViewController: EmergencyLeaveCellDelegate {
    func didSelect(index: Int) {
        self.viewModel.emergencyLeaveDocuments.remove(at: index)
        self.collectionView.reloadData()
    }
}
