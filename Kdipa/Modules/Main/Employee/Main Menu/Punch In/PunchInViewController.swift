//
//  PunchInViewController.swift
//  Kdipa
//
//  Created by Said Elmansour on 2020-02-28.
//  Copyright © 2020 Safoine Moncef Amine. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation

class PunchInViewController: BaseViewController {

    // MARK: - Views

    @IBOutlet weak var titleLabel: KdipaLabel!
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var punchInButton: UIButton!
    @IBOutlet weak var btnPunchIn: UIButton!
    
    // MARK: - Properties
    
    let biometricManager = BiometricManager()
    let viewModel = PunchInViewModel()
    var coordinator: MenuCoordinator?
    
    // MARK: - Life Cycle

    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavigationBarItems()
        mapView.showsUserLocation = true
        userLocationAuthorizationStatus()
        configure()
        setupUI()
        setupLocalizer()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        UserLocationManager.shared.startUpdatingLocation()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if let currentLocation = UserLocationManager.shared.currentLocation {
            self.viewModel.punchIn.latitude = "\(currentLocation.latitude)"
            self.viewModel.punchIn.logitude = "\(currentLocation.longitude)"
        }
        self.viewModel.checkAttendancePunch()
    }

    // MARK: - Setups
    
    private func setupUI() {
        punchInButton.layer.cornerRadius = 5
    }
    
    private func setupLocalizer() {
        self.punchInButton.setTitle(Localizable.PunchIn.punchInButtonTitle, for: .normal)
        self.titleLabel.text = Localizable.PunchIn.punchInTitle
    }
    
    // MARK: - Local Helprs
    
    private func touchIDLoginAction() {
        
        biometricManager.authenticateUser { [weak self] message in
            if let message = message {
                let alertView = UIAlertController(title: Localizable.PunchIn.errorTitle,
                    message: message,
                    preferredStyle: .alert)
                let okAction = UIAlertAction(title: Localizable.PunchIn.okButton, style: .default)
                let appSettingAction = UIAlertAction(title: Localizable.PunchIn.settingButton, style: .default) { (_) in
                    UIApplication.openAppSettings()
                }
                alertView.addAction(okAction)
                alertView.addAction(appSettingAction)
                self?.present(alertView, animated: true)
            } else {
                self?.viewModel.punchIn.latitude = "\(UserLocationManager.shared.currentLocation.latitude)"
                self?.viewModel.punchIn.logitude = "\(UserLocationManager.shared.currentLocation.longitude)"
                DispatchQueue.main.async {
                    self?.viewModel.submitAttendancePunch()
                }
            }
        }
    }
    
    private func configure() {
        self.viewModel.submittingPunchInCallState.bind { (state) in
            switch state {
            case .running:
                self.showSpinner(uiView: self.navigationController?.view ?? self.view)
            case .success:
                self.hideSpinner()
                let delayInSeconds = 2.0
                DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + delayInSeconds) {
                    self.coordinator?.dashboard()
                }
                var message = ""
                if Constants.isPunchInLast {
                    message = Localizable.PunchIn.punchOutNotificationSuccess
                } else {
                    message = Localizable.PunchIn.punchInNotificationSuccess
                }
                Constants.isPunchInLast = !Constants.isPunchInLast
                if let time = self.viewModel.punchInResponse["Time"] as? String {
                    let timeDesc = time.getDate()?.formatTo(format: "hh:mm:ss")
                    message.append("\(timeDesc ?? "")")
                }
                
                self.showConfirmationMessage(view: self.navigationController?.view ?? self.view, message: message)
            case .failed:
                self.hideSpinner()
                self.showConfirmationMessage(
                    view: self.navigationController?.view ?? self.view,
                    message: Localizable.PunchIn.notificationFailureAlertMessage)
            default:
                self.hideSpinner()
            }
        }
        
        self.viewModel.checkPunchInCallState.bind { (state) in
            switch state {
            case .success:
                self.btnPunchIn.isUserInteractionEnabled = true
                self.btnPunchIn.backgroundColor = UIColor.kdipaDarkBlue
                self.hideSpinner()
            case .failed:
                self.btnPunchIn.isUserInteractionEnabled = false
                self.btnPunchIn.backgroundColor = UIColor.lightGray
                self.hideSpinner()
            case .inactive:
                self.hideSpinner()
            case .running:
                self.showSpinner(uiView: self.navigationController?.view ?? self.view)
            }
        }
    }

    private func userLocationAuthorizationStatus() {
        UserLocationManager.shared.locationStatus.bind { (status) in
            switch status {
            case .authorizedAlways:
                self.viewModel.checkAttendancePunch()
                self.zoomToUserLocation()
            case .authorizedWhenInUse:
                self.viewModel.checkAttendancePunch()
                self.zoomToUserLocation()
            case .restricted:
                UIApplication.openAppSettings()
            case .notDetermined:
                UserLocationManager.shared.locationManager.requestAlwaysAuthorization()
            case .denied:
                UserLocationManager.shared.locationManager.requestAlwaysAuthorization()
            }
        }
    }
    
    private func zoomToUserLocation() {
        //Zoom to user location
        if let userLocation = UserLocationManager.shared.currentLocation {
            let viewRegion = MKCoordinateRegion(center: userLocation, latitudinalMeters: 500, longitudinalMeters: 500)
               mapView.setRegion(viewRegion, animated: false)
            UserLocationManager.shared.stopUpdatingLocation()
           }
    }

    // MARK: - Actions

    @IBAction func onPunchIn(_ sender: Any) {
        touchIDLoginAction()
    }

}
