//
//  PostedReturnDate.swift
//  Kdipa
//
//  Created by Safoine Moncef Amine on 3/23/20.
//  Copyright © 2020 Safoine Moncef Amine. All rights reserved.
//

import Foundation

// MARK: - PostedReturnDate

struct PostedReturnDate: Codable {
    let email, vacationID, returnDate, lateReturnReason: String

    enum CodingKeys: String, CodingKey {
        case email
        case vacationID = "vacationId"
        case returnDate, lateReturnReason
    }
}
